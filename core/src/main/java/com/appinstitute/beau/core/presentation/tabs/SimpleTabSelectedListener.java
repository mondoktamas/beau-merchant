package com.appinstitute.beau.core.presentation.tabs;

import android.support.design.widget.TabLayout;

public class SimpleTabSelectedListener implements TabLayout.OnTabSelectedListener {

    private boolean mShouldOpenTabByPosition = true;

    public boolean isShouldOpenTabByPosition() {
        return mShouldOpenTabByPosition;
    }

    @Override
    public void onTabSelected(final TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(final TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(final TabLayout.Tab tab) {

    }

    public void setShouldOpenTabByPosition(final boolean shouldOpenTabByPosition) {
        mShouldOpenTabByPosition = shouldOpenTabByPosition;
    }
}
