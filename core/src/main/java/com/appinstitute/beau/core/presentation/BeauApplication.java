package com.appinstitute.beau.core.presentation;

import android.app.Application;

import com.appinstitute.beau.core.common.FontsOverride;

public class BeauApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        setupFonts();
    }

    public void setupFonts() {
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/Tryst-Regular.otf");
    }
}
