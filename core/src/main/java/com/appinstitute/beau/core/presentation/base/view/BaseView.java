package com.appinstitute.beau.core.presentation.base.view;

import com.appinstitute.beau.core.presentation.base.presenter.Presenter;

public interface BaseView<P extends Presenter> extends View<P> {
    void showMessage(final String message);
    void showProgress(final boolean visible);
}
