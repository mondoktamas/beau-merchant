package com.appinstitute.beau.core.common;

import android.os.Looper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class RxUtils {

    public static Scheduler getBackgroundScheduler() {
        //main thread
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            return Schedulers.io();
        } else {
            //we are already in bg thread
            return Schedulers.immediate();
        }
    }

    static final Observable.Transformer asyncTransformer = new Observable.Transformer() {
        @Override
        public Object call(Object o) {
            return ((Observable)o).subscribeOn(getBackgroundScheduler())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    };

    static final Observable.Transformer syncTransformer = new Observable.Transformer() {
        @Override
        public Object call(Object o) {
            return ((Observable)o).subscribeOn(Schedulers.immediate())
                    .observeOn(Schedulers.immediate());
        }
    };

    static final Observable.Transformer mainThreadTransformer = new Observable.Transformer() {
        @Override
        public Object call(Object o) {
            return ((Observable)o).subscribeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    };

    @SuppressWarnings("unchecked")
    public static <T> Observable.Transformer<T, T> applyAsyncSchedulers() {
        return (Observable.Transformer<T, T>) asyncTransformer;
    }

    @SuppressWarnings("unchecked")
    public static <T> Observable.Transformer<T, T> applySyncSchedulers() {
        return (Observable.Transformer<T, T>) syncTransformer;
    }

    public static <T> Observable.Transformer<T, T> applyMainThreadScheduler() {
        return (Observable.Transformer<T, T>) mainThreadTransformer;
    }

    private static <T> Func1<Iterable<T>, List<T>> convertIterableToList() {
        return new Func1<Iterable<T>, List<T>>() {
            @Override
            public List<T> call(final Iterable<T> iterable) {
                if(iterable instanceof List) {
                    return (List<T>) iterable;
                }
                ArrayList<T> list = new ArrayList<>();
                if(iterable != null) {
                    for(T t: iterable) {
                        list.add(t);
                    }
                }
                return list;
            }
        };
    }

    public static <T> Single.Transformer<Iterable<T>, List<T>> iterableToListSingle() {
        return new Single.Transformer<Iterable<T>, List<T>>() {
            @Override
            public Single<List<T>> call(final Single<Iterable<T>> iterableSingle) {
                return iterableSingle.map(RxUtils.<T>convertIterableToList());
            }
        };
    }
}
