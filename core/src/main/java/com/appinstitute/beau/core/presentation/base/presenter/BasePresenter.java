package com.appinstitute.beau.core.presentation.base.presenter;

import android.support.annotation.NonNull;

import com.appinstitute.beau.core.presentation.base.view.View;

import java.lang.ref.WeakReference;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<V extends View> implements Presenter<V> {

    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    WeakReference<V> baseView;

    @Override
    public void attachView(final V view) {
        baseView = new WeakReference<V>(view);
    }

    @Override
    public V getView() {
        return baseView.get();
    }

    @Override
    public boolean isViewAttached() {
        return baseView != null && baseView.get() != null;
    }

    @Override
    public void unsubscribeOnDestroy(@NonNull final Subscription subscription) {
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void detachView() {
        mCompositeSubscription.clear();
        if (baseView != null)
            baseView.clear();
        baseView = null;
    }
}
