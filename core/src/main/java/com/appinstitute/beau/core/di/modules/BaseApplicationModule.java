package com.appinstitute.beau.core.di.modules;

import android.app.Application;
import android.content.Context;

import com.appinstitute.beau.core.di.annotations.ApplicationContext;

import dagger.Module;
import dagger.Provides;

@Module
public class BaseApplicationModule {

}
