package com.appinstitute.beau.core.domain.base;

import com.appinstitute.beau.core.data.network.exception.RetrofitException;

import rx.Subscriber;

public class DefaultSubscriber<T> extends Subscriber<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(final Throwable e) {
        if (!(e instanceof RetrofitException)) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNext(T t) {

    }
}
