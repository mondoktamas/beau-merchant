package com.appinstitute.beau.core.presentation.base.view.form;

import java.util.regex.Pattern;

public final class Validator {

    private final static String EMAIL_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+";
    private final static String PHONE_PATTERN = "^\\+?\\d{1,4}?[-.\\s]?\\(?\\d{1,3}?\\)?[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,9}$";
    private final static String USER_ID_PATTERN = "([a-zA-Z]{1,})(?=.*[\\d]{0,})[a-zA-Z0-9]{3,65}$";
    private final static String PASSWORD_PATTERN = "((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-9]).{5,})\\S";

    public static boolean isEmpty(final String field) {
        return field == null || field.equals("");
    }

    public static boolean isCorrectEmail(final String email) {
        return Pattern.compile(EMAIL_PATTERN).matcher(email).matches();
    }

    public static boolean isCorrectUserId(final String userId) {
        return Pattern.compile(USER_ID_PATTERN).matcher(userId).matches();
    }

    public static boolean isCorrectPassword(final String password) {
        return Pattern.compile(PASSWORD_PATTERN).matcher(password).matches();
    }

    public static boolean isCorrectPhone(final String phone) {
        return Pattern.compile(PHONE_PATTERN).matcher(phone).matches();
    }

    public static boolean isEquals(final String password, final String confirmPassword) {
        return password.equals(confirmPassword);
    }
}

