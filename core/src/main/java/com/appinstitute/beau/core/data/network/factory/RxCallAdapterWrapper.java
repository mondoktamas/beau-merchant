package com.appinstitute.beau.core.data.network.factory;

import com.appinstitute.beau.core.data.network.exception.RetrofitException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.functions.Func1;

/**
 * The class customize {@link Retrofit} calls, he returns an {@link Observable}
 * that allows you to work with RxJava.
 */

public class RxCallAdapterWrapper implements CallAdapter<Observable<?>> {
    private final Retrofit retrofit;
    private final CallAdapter<?> wrapped;

    public RxCallAdapterWrapper(Retrofit retrofit, CallAdapter<?> wrapped) {
        this.retrofit = retrofit;
        this.wrapped = wrapped;
    }

    /**
     * @see Factory#responseType()
     */
    @Override
    public Type responseType() {
        return wrapped.responseType();
    }

    /**
     * @see Factory#adapt(Call) ()
     */
    @SuppressWarnings("unchecked")
    @Override
    public <R> Observable<?> adapt(Call<R> call) {
        return ((Observable) wrapped.adapt(call)).onErrorResumeNext(new Func1<Throwable, Observable>() {
            @Override
            public Observable call(Throwable throwable) {
                return Observable.error(asRetrofitException(throwable));
            }
        });
    }

    /**
     * Returns the converted {@link Throwable} to {@link RetrofitException}
     *
     * @param throwable The exception which we want to convert to the {@link RetrofitException}
     */
    public RetrofitException asRetrofitException(Throwable throwable) {
        // We had non-200 http error
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            Response response = httpException.response();

            return RetrofitException.httpError(response.raw().request().url().toString(), response, retrofit);
        }
        // A network error happened
        if (throwable instanceof TimeoutException || throwable instanceof ConnectException ||
                throwable instanceof SocketTimeoutException || throwable instanceof UnknownHostException) {
            return RetrofitException.networkError(new IOException(throwable.getMessage(), throwable));
        }

        // We don't know what happened. We need to simply convert to an unknown error
        return RetrofitException.unexpectedError(throwable);
    }
}
