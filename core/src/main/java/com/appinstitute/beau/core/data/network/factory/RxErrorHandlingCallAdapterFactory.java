package com.appinstitute.beau.core.data.network.factory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;

/**
 * A {@link CallAdapter.Factory call adapter} which uses RxJava 2 for creating observables.
 * <p>
 * Adding this class to {@link Retrofit} allows you to return an {@link Observable} from service
 * methods.
 * Now the class returns an {@link RxCallAdapterWrapper}
 */
public class RxErrorHandlingCallAdapterFactory extends CallAdapter.Factory {
    private final RxJavaCallAdapterFactory original;

    private RxErrorHandlingCallAdapterFactory() {
        original = RxJavaCallAdapterFactory.create();
    }

    /**
     * Returns an {@link RxErrorHandlingCallAdapterFactory} instance
     */
    public static CallAdapter.Factory create() {
        return new RxErrorHandlingCallAdapterFactory();
    }

    /**
     * Returns an {@link RxCallAdapterWrapper} instance
     */
    @Override
    public CallAdapter<?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        return new RxCallAdapterWrapper(retrofit, original.get(returnType, annotations, retrofit));
    }
}
