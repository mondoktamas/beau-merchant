package com.appinstitute.beau.core.data.network.config;

import java.util.List;

import okhttp3.Interceptor;

public interface NetworkConfiguration {

    int getConnectionTimeout();

    String getBaseUrl();

    List<Interceptor> getInterceptors();
}