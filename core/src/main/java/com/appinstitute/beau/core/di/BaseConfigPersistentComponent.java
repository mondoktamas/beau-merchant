package com.appinstitute.beau.core.di;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.di.modules.BaseActivityModule;

import dagger.Component;

@ConfigPersistent
@Component(dependencies = BaseApplicationComponent.class)
public interface BaseConfigPersistentComponent {


}
