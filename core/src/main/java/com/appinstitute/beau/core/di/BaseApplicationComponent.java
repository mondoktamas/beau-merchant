package com.appinstitute.beau.core.di;

import com.appinstitute.beau.core.di.modules.BaseApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {BaseApplicationModule.class})
public interface BaseApplicationComponent {

}
