package com.appinstitute.beau.core.presentation.base.view;

import android.support.annotation.LayoutRes;

import com.appinstitute.beau.core.presentation.base.presenter.Presenter;

public interface View<P extends Presenter> {
    @LayoutRes
    int getLayoutResource();
    P getPresenter();
}
