package com.appinstitute.beau.core.presentation.base.presenter;

import android.support.annotation.NonNull;

import com.appinstitute.beau.core.presentation.base.view.View;

import rx.Subscription;

public interface Presenter<V extends View> {

    void attachView(final V view);

    V getView();

    boolean isViewAttached();

    void unsubscribeOnDestroy(@NonNull final Subscription subscription);

    void detachView();
}
