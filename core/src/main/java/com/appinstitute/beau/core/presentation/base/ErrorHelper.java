package com.appinstitute.beau.core.presentation.base;

public interface ErrorHelper {

    String getErrorString(String[] errors);
}
