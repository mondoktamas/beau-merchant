package com.appinstitute.beau.core.di;

import com.appinstitute.beau.core.di.annotations.PerActivity;
import com.appinstitute.beau.core.di.modules.BaseActivityModule;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {BaseActivityModule.class})
public interface BaseActivityComponent {
}
