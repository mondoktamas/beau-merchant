package com.appinstitute.beau.core.domain.base;

import android.support.annotation.CallSuper;

import com.appinstitute.beau.core.data.network.exception.RetrofitException;
import com.appinstitute.beau.core.data.network.model.response.BaseResponse;

import java.io.IOException;

public abstract class DefaultInternetSubscriber<R> extends DefaultSubscriber<R> {

    @CallSuper
    @Override
    public void onError(Throwable throwable) {
        super.onError(throwable);
        //if the exception catched from another place (not retrofit) the cast it to unexpected
        if (!(throwable instanceof RetrofitException)) {
            throwable.printStackTrace();
            throwable = RetrofitException.unexpectedError(throwable);
        }

        final RetrofitException error = (RetrofitException) throwable;
        switch (error.getKind()) {
            case HTTP:
                handleHttpError(error);
                break;
            case NETWORK:
                //probably no internet connection
                handleNetworkError(error);
                break;
            default:
                handleUnexpectedError(error);
                break;
        }
    }

    private void handleHttpError(final RetrofitException error) {
        switch (error.getResponse().code()) {
            case 401:
                handleUnauthorizedException();
                break;
            case 417:
                String[] parsedError = parseError(error);
                if (parsedError == null) return;
                handleExpectationException(parsedError);
                break;
            case 422:
                parsedError = parseError(error);
                if (parsedError == null) return;
                handleUnprocessableEntity(parsedError);
                break;
            case 500:
                handleInternalServerError();
                break;
        }
    }

    private String[] parseError(final RetrofitException exception) {
        try {
            final BaseResponse parsedError = exception.getErrorBodyAs(BaseResponse.class);
            return parsedError.getErrors();
        } catch (IOException e) {
            return null;
        }
    }

    public void handleUnauthorizedException() {
    }

    public void handleUnprocessableEntity(final String[] errors) { }

    public void handleExpectationException(final String[] errors) { }

    public void handleInternalServerError() { }

    public void handleUnexpectedError(final RetrofitException exception) { }

    public void handleNetworkError(final RetrofitException error) { }
}
