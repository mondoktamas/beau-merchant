package com.appinstitute.beau.core.data.network.config;

import com.appinstitute.beau.core.data.network.model.response.BaseResponse;
import com.bluelinelabs.logansquare.LoganSquare;

import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Arrays;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

public class ErrorInterceptor implements Interceptor {

    private final Charset UTF8 = Charset.forName("UTF-8");

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();
        final long contentLength = request.body().contentLength();

        Response response = chain.proceed(request);

        if (response.code() == 200) {
            final ResponseBody responseBody = response.body();
            final MediaType contentType = responseBody.contentType();

            String jsonString;
            try {
                jsonString = parseResponse(response.body(), contentType, contentLength);
            } catch (ResponseParseException e) {
                return response;
            }
            final BaseResponse result = LoganSquare.parse(jsonString, BaseResponse.class);
            if (result != null && !result.isResult()) {
                List<String> errors = Arrays.asList(result.getErrors());
                if (errors.contains("session_id_required")) {
                    ResponseBody body = ResponseBody.create(contentType, jsonString);
                    response = new Response.Builder().body(body).code(401).build();
                } else {
                    ResponseBody body = ResponseBody.create(contentType, jsonString);
                    response = new Response.Builder()
                            .body(body)
                            .code(422)
                            .headers(response.headers())
                            .header("Status", "422 Unprocessable Entity")
                            .request(response.request())
                            .protocol(response.protocol())
                            .message("Unprocessable Entity")
                            .build();
                }
            }
        }
        return response;
    }

    private String parseResponse(final ResponseBody responseBody,
                                 final MediaType contentType,
                                 final long contentLength)
            throws IOException, ResponseParseException {
        final BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE); // Buffer the entire body.

        final Buffer buffer = source.buffer();

        Charset charset = UTF8;

        if (contentType != null) {
            try {
                charset = contentType.charset(UTF8);
            } catch (UnsupportedCharsetException e) {
                throw new ResponseParseException();
            }
        }

        if (!isPlaintext(buffer)) {
            throw new ResponseParseException();
        }

        if (contentLength != 0) {
            return buffer.clone().readString(charset);
        } else {
            return "";
        }
    }

    private boolean isPlaintext(final Buffer buffer) {
        try {
            Buffer prefix = new Buffer();
            long byteCount = buffer.size() < 64 ? buffer.size() : 64;
            buffer.copyTo(prefix, 0, byteCount);
            for (int i = 0; i < 16; i++) {
                if (prefix.exhausted()) {
                    break;
                }
                int codePoint = prefix.readUtf8CodePoint();
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException e) {
            return false; // Truncated UTF-8 sequence.
        }
    }
}
