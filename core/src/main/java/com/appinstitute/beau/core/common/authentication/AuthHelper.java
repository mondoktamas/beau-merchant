package com.appinstitute.beau.core.common.authentication;

import com.appinstitute.beau.core.presentation.base.model.BaseModel;

public interface AuthHelper<T extends BaseModel> {

    void setUserData(final T userData);
    T getUserModel();
    boolean isLoggedIn();
    String getToken();
    void releaseData();
}
