package com.appinstitute.beau.merchant.data.repository.account;

import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.merchant.data.repository.booking.OffersRepositoryImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class OffersRepositoryImplTest {
    @Mock ApiService mApiService;
    OffersRepositoryImpl mOffersRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mOffersRepository = new OffersRepositoryImpl(mApiService);
    }

    @Test
    public void test_loadOpenOffersCount() {
        mOffersRepository.loadOpenOffersCount();
        verify(mApiService).loadOpenOffersCount();
    }
}
