package com.appinstitute.beau.merchant.presentation.register.basic;

import android.app.AlertDialog;
import android.content.Intent;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.RoboletricBaseTestRunner;
import com.appinstitute.beau.merchant.presentation.model.UserModel;
import com.appinstitute.beau.merchant.presentation.register.complete.RegistrationCompleteActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.util.ActivityController;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

public class RegistrationActivityTest extends RoboletricBaseTestRunner {

    @Mock RegistrationPresenter mRegistrationPresenter;
    private RegistrationActivity mRegistrationActivity;
    private ActivityController<RegistrationActivity> mActivityActivityController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mActivityActivityController = Robolectric.buildActivity(RegistrationActivity.class).create().start().visible();
        mRegistrationActivity = mActivityActivityController.get();
        mRegistrationActivity.setPresenter(mRegistrationPresenter);
    }

    @Test
    public void test_getLaunchIntentShouldNotBeNull() {
        final Intent launchIntent = RegistrationActivity.getLaunchIntent(mRegistrationActivity);
        assertNotNull(launchIntent);
        assertTrue(mRegistrationActivity.getIntent().getComponent().equals(launchIntent.getComponent()));
    }

    @Test
    public void test_onResumeShouldSetMovementMethod() {
        mRegistrationActivity.onResume();
        assertNotNull(mRegistrationActivity.mTermsAndPolicy.getMovementMethod());
        assertNotNull(mRegistrationActivity.mRegisterText.getMovementMethod());
    }

    @Test
    public void test_onRegisterClickShouldCallPresenterMethodWithEmptyFields() {
        mRegistrationActivity.onRegisterClicked();
        verify(mRegistrationPresenter).registerClicked("", "", "");
    }

    @Test
    public void test_onRegisterClickShouldCallPresenterMethodWithEnteredValues() {
        mRegistrationActivity.mBusinessName.setText("Tomi");
        mRegistrationActivity.mUserId.setText("Tomi");
        mRegistrationActivity.mPassword.setText("123456");
        mRegistrationActivity.onRegisterClicked();
        verify(mRegistrationPresenter).registerClicked("Tomi", "Tomi", "123456");
    }

    @Test
    public void test_incorrectUserIdShouldShowAlert() {
        mRegistrationActivity.incorrectUserId();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_invalid_user_id));
    }

    @Test
    public void test_incorrectFieldsShouldShowAlert() {
        mRegistrationActivity.incorrectFields();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_validation_alert_message));
    }

    @Test
    public void test_incorrectPasswordShouldShowAlert() {
        mRegistrationActivity.incorrectPassword();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_invalid_password));
    }

    @Test
    public void test_alertShouldDismiss() {
        mRegistrationActivity.incorrectPassword();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        latestAlert.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
        assertTrue("Alert still showing", !latestAlert.isShowing());
    }

    @Test
    public void test_continueRegistrationFlowShouldStartNextActivity() {
        ShadowActivity shadowActivity = shadowOf(mRegistrationActivity);

        mRegistrationActivity.continueRegistrationFlow(new UserModel());
        Intent intent = new Intent(mRegistrationActivity, RegistrationCompleteActivity.class);
        assertTrue(shadowActivity.getNextStartedActivity().getComponent().equals(intent.getComponent()));
    }

    @After
    public void tearDown() {
        mActivityActivityController.pause().stop().destroy();
        super.tearDown();
    }
}
