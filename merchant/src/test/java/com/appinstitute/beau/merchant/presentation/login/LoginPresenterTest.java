package com.appinstitute.beau.merchant.presentation.login;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.data.network.exception.RetrofitException;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.merchant.data.network.model.response.SignInResponse;
import com.appinstitute.beau.merchant.domain.auth.SignInUseCase;
import com.appinstitute.beau.merchant.presentation.model.UserModel;
import com.appinstitute.beau.merchant.util.RetrofitTestUtil;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Subscription;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LoginPresenterTest {

    @Mock LoginView mLoginView;
    @Mock SignInUseCase mSignInUseCase;
    @Mock ErrorHelper mErrorHelper;
    @Mock AuthHelper mAuthHelper;
    @Captor ArgumentCaptor<LoginPresenter.SignInSubscriber> signInSubscriberArgumentCaptor;

    Retrofit mRetrofit;
    LoginPresenter mLoginPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mLoginPresenter = new LoginPresenter(mAuthHelper, mErrorHelper, mSignInUseCase);
        mLoginPresenter.attachView(mLoginView);

        mRetrofit = RetrofitTestUtil.getInstance();
    }

    @Test
    public void test_isViewAttached() {
        assertTrue(mLoginPresenter.isViewAttached());
        mLoginPresenter.detachView();
        assertFalse(mLoginPresenter.isViewAttached());
    }

    @Test
    public void test_shouldSwingFormIfEmptyBothFields() {
        mLoginPresenter.onLoginClicked("", "");
        verify(mLoginView).clearValidationErrors();
        verify(mLoginView).swingForm();
    }

    @Test
    public void test_shouldShowErrorIfEmptyUserId() {
        mLoginPresenter.onLoginClicked("", "pass");
        verify(mLoginView).clearValidationErrors();
        verify(mLoginView).userIdIsEmpty();
    }

    @Test
    public void test_shouldShowErrorIfEmptyPassword() {
        mLoginPresenter.onLoginClicked("userId", "");
        verify(mLoginView).clearValidationErrors();
        verify(mLoginView).passwordIsEmpty();
    }

    @Test
    public void test_shouldCallSignInUseCaseIfAllFieldsFilled() {
        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mSignInUseCase).execute(any());

        SignInResponse signInResponse = new SignInResponse();
        signInResponse.setResult(true);

        final UserModel userModel = new UserModel(null, "userId", "password");
        mLoginPresenter.onLoginClicked("userId", "password");
        verify(mLoginView).clearValidationErrors();
        verify(mSignInUseCase).setData("userId", "password");
        verify(mSignInUseCase).execute(signInSubscriberArgumentCaptor.capture());
        signInSubscriberArgumentCaptor.getValue().onStart();
        verify(mLoginView).showProgress(true);
        signInSubscriberArgumentCaptor.getValue().onNext(userModel);
        verify(mLoginView).signInSuccess();
        signInSubscriberArgumentCaptor.getValue().onCompleted();
        verify(mLoginView).showProgress(false);

    }

    @Test
    public void test_shouldCallSignInUseCaseWithError() {
        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mSignInUseCase).execute(any());

        SignInResponse signInResponse = new SignInResponse();
        signInResponse.setResult(true);

        mLoginPresenter.onLoginClicked("userId", "password");
        verify(mLoginView).clearValidationErrors();
        verify(mSignInUseCase).setData("userId", "password");
        verify(mSignInUseCase).execute(signInSubscriberArgumentCaptor.capture());
        signInSubscriberArgumentCaptor.getValue().onStart();
        verify(mLoginView).showProgress(true);

        final ResponseBody errorResponse = ResponseBody.create(MediaType.parse("application/json"),
                "{\"result\":false,\"errors\":[\"user_not_found\"]}");
        final RetrofitException retrofitException = RetrofitException.httpError(null,
                Response.error(422, errorResponse), mRetrofit);
        signInSubscriberArgumentCaptor.getValue().onError(retrofitException);
        verify(mErrorHelper).getErrorString(new String[] {"user_not_found"});
        verify(mLoginView).showProgress(false);
    }

    @Test(expected = NullPointerException.class)
    public void test_detachViewCallShouldDestroyViewReference() {
        mLoginPresenter.detachView();
        assertNull(mLoginPresenter.getView());
    }
}
