package com.appinstitute.beau.merchant.presentation.welcome;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class WelcomePresenterTest {
    @Mock WelcomeView mWelcomeView;
    private WelcomePresenter mWelcomePresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mWelcomePresenter = new WelcomePresenter();
        mWelcomePresenter.attachView(mWelcomeView);
    }

    @Test
    public void test_OnLoginClickedShouldLaunchLoginScreen() {
        mWelcomePresenter.onLoginClicked();
        verify(mWelcomeView).launchLoginScreen();
    }

    @Test
    public void test_onRegisterClickedShouldLaunchRegisterScreen() {
        mWelcomePresenter.onRegistrationClicked();
        verify(mWelcomeView).launchRegistrationScreen();
    }
}
