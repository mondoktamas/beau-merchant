package com.appinstitute.beau.merchant.domain;

import com.appinstitute.beau.merchant.data.repository.booking.OffersRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class OpenOffersCountUseCaseTest {

    @Mock OffersRepository mOffersRepository;

    OpenOffersCountUseCase mOpenOffersCountUseCase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mOpenOffersCountUseCase = new OpenOffersCountUseCase(mOffersRepository);
    }

    @Test
    public void test_buildObservableTask() {
        mOpenOffersCountUseCase.buildObservableTask();
        verify(mOffersRepository).loadOpenOffersCount();
    }
}
