package com.appinstitute.beau.merchant.data.repository.auth;

import com.appinstitute.beau.merchant.data.network.model.request.SignInRequest;
import com.appinstitute.beau.merchant.data.network.model.request.UserRequest;
import com.appinstitute.beau.merchant.data.network.model.response.UserResponse;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.merchant.data.repository.UserDataMapper;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import rx.Observable;
import rx.observers.TestSubscriber;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class AuthRepositoryImplTest {

    @Mock ApiService mApiService;
    @Mock UserDataMapper mDataMapper;

    AuthRepositoryImpl mAuthRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mAuthRepository = new AuthRepositoryImpl(mApiService, mDataMapper);
    }

    @Test
    public void test_signIn() {
        final String userId = "mondoktamas1";
        final String password = "Aa123456";

        final SignInRequest signInRequest = new SignInRequest(userId, password, null);
        final UserResponse userResponse = new UserResponse();
        final UserModel userModel = new UserModel(null, userId, password);

        doReturn(Observable.just(userResponse)).when(mApiService).signIn(signInRequest);
        doReturn(Observable.just(userModel)).when(mDataMapper).transform(userResponse);

        final TestSubscriber<UserModel> testSubscriber = new TestSubscriber<>();
        mAuthRepository.signIn(userId, password).subscribe(testSubscriber);

        verify(mApiService).signIn(signInRequest);
        verify(mDataMapper).transform(userResponse);

        assertEquals(testSubscriber.getOnNextEvents().size(), 1);
        testSubscriber.assertReceivedOnNext(Arrays.asList(userModel));
        testSubscriber.assertNoErrors();
    }

    @Test
    public void test_registerUser() {
        final UserModel userModel = new UserModel("pApps", "mondoktamas1", "Aa123456");
        final UserRequest userRequest = new UserRequest();
        final UserResponse userResponse = new UserResponse();

        doReturn(Observable.just(userRequest)).when(mDataMapper).transform(userModel);
        doReturn(Observable.just(userResponse)).when(mApiService).register(userRequest);
        doReturn(Observable.just(userModel)).when(mDataMapper).transform(userResponse);

        final TestSubscriber<UserModel> testSubscriber = new TestSubscriber<>();
        mAuthRepository.registerUser(userModel).subscribe(testSubscriber);

        verify(mDataMapper).transform(userModel);
        verify(mApiService).register(userRequest);
        verify(mDataMapper).transform(userResponse);

        assertEquals(testSubscriber.getOnNextEvents().size(), 1);
        testSubscriber.assertReceivedOnNext(Arrays.asList(userModel));
        testSubscriber.assertNoErrors();
    }
}
