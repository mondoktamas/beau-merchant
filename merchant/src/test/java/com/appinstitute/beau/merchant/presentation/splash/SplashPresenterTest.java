package com.appinstitute.beau.merchant.presentation.splash;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.merchant.domain.splash.SplashUseCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Subscription;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SplashPresenterTest {

    private static final boolean IS_LOGGED_IN = true;

    @Mock
    AuthHelper mAuthHelper;

    @Mock
    SplashView mSplashView;

    @Mock
    SplashUseCase mSplashUseCase;

    @Captor
    ArgumentCaptor<SplashPresenter.SplashSubscriber> mSubscriberArgumentCaptor;

    SplashPresenter mSplashPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mSplashPresenter = new SplashPresenter(mAuthHelper, mSplashUseCase);
        mSplashPresenter.attachView(mSplashView);
    }

    @Test
    public void test_startSplashTask() {
        final Subscription subscription = mock(Subscription.class);
        when(mAuthHelper.isLoggedIn()).thenReturn(true);
        doReturn(subscription).when(mSplashUseCase).execute(any());
        mSplashPresenter.startSplashTask();
        verify(mSplashUseCase).execute(mSubscriberArgumentCaptor.capture());
        mSubscriberArgumentCaptor.getValue().onCompleted();
        verify(mSplashView).launchNextScreen(IS_LOGGED_IN);
    }

    @Test
    public void test_startSplashTaskViewDetached() {
        final Subscription subscription = mock(Subscription.class);
        when(mAuthHelper.isLoggedIn()).thenReturn(true);
        doReturn(subscription).when(mSplashUseCase).execute(any());
        mSplashPresenter.startSplashTask();
        verify(mSplashUseCase).execute(mSubscriberArgumentCaptor.capture());
        mSplashPresenter.detachView();
        mSubscriberArgumentCaptor.getValue().onCompleted();
        assertFalse(mSplashPresenter.isViewAttached());
    }

    @Test
    public void test_isViewAttached() {
        assertTrue(mSplashPresenter.isViewAttached());
        mSplashPresenter.detachView();
        assertFalse(mSplashPresenter.isViewAttached());
    }

    @Test(expected = NullPointerException.class)
    public void test_detachViewCallShouldDestroyViewReference() {
        mSplashPresenter.detachView();
        assertNull(mSplashPresenter.getView());
    }
}
