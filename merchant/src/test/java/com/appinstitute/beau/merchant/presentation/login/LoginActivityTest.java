package com.appinstitute.beau.merchant.presentation.login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.RoboletricBaseTestRunner;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

public class LoginActivityTest extends RoboletricBaseTestRunner {

    @Mock
    LoginPresenter mLoginPresenter;
    private LoginActivity mLoginActivity;
    private ActivityController<LoginActivity> mActivityActivityController;

    @Before
    @Override
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mActivityActivityController = Robolectric.buildActivity(LoginActivity.class).create().start().visible();
        mLoginActivity = mActivityActivityController.get();
        mLoginActivity.setPresenter(mLoginPresenter);
    }

    @Test
    public void test_getLaunchIntentShouldNotBeNull() {
        final Intent launchIntent = LoginActivity.getLaunchIntent(mLoginActivity);
        assertNotNull(launchIntent);
        assertTrue(mLoginActivity.getIntent().getComponent().equals(launchIntent.getComponent()));
    }

    @Test
    public void test_onLoginClickShouldBeHandled() {
        mLoginActivity.findViewById(R.id.button_login).performClick();
        verify(mLoginPresenter).onLoginClicked("", "");
    }

    @Test
    public void test_validationErrorsShouldBeRemoved() {
        final String userIdError = "UserID error";
        final String passwordError = "Password error";
        mLoginActivity.mUserIdContainer.setError(userIdError);
        mLoginActivity.mPasswordContainer.setError(passwordError);
        assertNotNull(mLoginActivity.mUserIdContainer.getError());
        assertEquals(mLoginActivity.mUserIdContainer.getError(), userIdError);
        assertNotNull(mLoginActivity.mPasswordContainer.getError());
        assertEquals(mLoginActivity.mPasswordContainer.getError(), passwordError);
        mLoginActivity.clearValidationErrors();
        assertNull(mLoginActivity.mUserIdContainer.getError());
        assertNull(mLoginActivity.mPasswordContainer.getError());
    }

    @Test
    public void test_userIdIsEmpty() {
        mLoginActivity.mUserIdEdit.setText(null);
        mLoginActivity.userIdIsEmpty();
        assertEquals(mLoginActivity.mUserIdContainer.getError(),
                mLoginActivity.getText(R.string.login_in_error_empty_user_id));
    }

    @Test
    public void test_passwordIsEmpty() {
        mLoginActivity.mPasswordEdit.setText(null);
        mLoginActivity.passwordIsEmpty();
        assertEquals(mLoginActivity.mPasswordContainer.getError(),
                mLoginActivity.getText(R.string.login_in_error_empty_password));
    }

    @Test
    public void test_onSignInSuccess() {
        final ShadowActivity shadowActivity = shadowOf(mLoginActivity);
        mLoginActivity.signInSuccess();
        final Intent intent = new Intent(mLoginActivity, TabActivity.class);
        assertTrue(shadowActivity.getNextStartedActivity().getComponent().equals(intent.getComponent()));
        assertTrue(shadowActivity.isFinishing());
    }

    @Test
    public void test_onResumeShouldSetMovementMethod() {
        mLoginActivity.onResume();
        Assert.assertNotNull(mLoginActivity.mRegisterAccountLink.getMovementMethod());
    }

    @Test
    public void test_alertShouldShow() {
        final String errorMessage = "error_message";
        mLoginActivity.showErrorMessages(errorMessage);
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), errorMessage);
        latestAlert.getButton(Dialog.BUTTON_POSITIVE).performClick();
        assertFalse(latestAlert.isShowing());
    }

    @After
    @Override
    public void tearDown() {
        mActivityActivityController.pause().stop().destroy();
        super.tearDown();
    }
}
