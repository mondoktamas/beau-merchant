package com.appinstitute.beau.merchant.presentation;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.util.ArraySet;
import android.view.View;

import com.appinstitute.beau.merchant.BuildConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.util.ReflectionHelpers;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.robolectric.Robolectric.flushBackgroundThreadScheduler;
import static org.robolectric.Robolectric.flushForegroundThreadScheduler;
import static org.robolectric.shadows.ShadowApplication.runBackgroundTasks;
import static org.robolectric.shadows.ShadowLooper.runUiThreadTasksIncludingDelayedTasks;

@RunWith(RobolectricTestRunner.class)
@Config(application = ApplicationTest.class, constants = BuildConfig.class, sdk = 23)
public class RoboletricBaseTestRunner {

    protected final Context context = RuntimeEnvironment.application;
    protected final Application application = RuntimeEnvironment.application;
    protected final Resources resources = context.getResources();

    @Before
    public void setUp() {
    }

    @Test
    public void test_inParenClass() {
        assertTrue("Parent test", "test".equals("test"));
    }

    @After
    public void tearDown() {

        try {
            this.resetBackgroundThread();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.resetWindowManager();
    }

    public void finishThreads() {
        runBackgroundTasks();
        flushForegroundThreadScheduler();
        flushBackgroundThreadScheduler();
        runUiThreadTasksIncludingDelayedTasks();
    }

    private void resetBackgroundThread() throws Exception {
        final Class<?> btclass = Class.forName("com.android.internal.os.BackgroundThread");
        final Object backgroundThreadSingleton = ReflectionHelpers.getStaticField(btclass, "sInstance");
        if (backgroundThreadSingleton != null) {
            btclass.getMethod("quit").invoke(backgroundThreadSingleton);
            ReflectionHelpers.setStaticField(btclass, "sInstance", null);
            ReflectionHelpers.setStaticField(btclass, "sHandler", null);
        }
    }

    @SuppressLint("NewApi")
    private void resetWindowManager() {
        final Class<?> clazz = ReflectionHelpers.loadClass(this.getClass().getClassLoader(), "android.view.WindowManagerGlobal");
        final Object instance = ReflectionHelpers.callStaticMethod(clazz, "getInstance");

        final List<Object> roots = ReflectionHelpers.getField(instance, "mRoots");
        for (int i = 0; i < roots.size(); i++) {
            try {
                ReflectionHelpers.callInstanceMethod(instance, "removeViewLocked",
                        ReflectionHelpers.ClassParameter.from(int.class, i),
                        ReflectionHelpers.ClassParameter.from(boolean.class, false));
            } catch (Exception ignored) {
            }
        }

        final ArraySet<View> dyingViews = ReflectionHelpers.getField(instance, "mDyingViews");
        dyingViews.clear();
    }
}
