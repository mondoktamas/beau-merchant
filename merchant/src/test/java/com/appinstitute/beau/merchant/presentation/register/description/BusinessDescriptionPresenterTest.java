package com.appinstitute.beau.merchant.presentation.register.description;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class BusinessDescriptionPresenterTest {

    @Mock BusinessDescriptionView mView;
    BusinessDescriptionPresenter mPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mPresenter = new BusinessDescriptionPresenter();
        mPresenter.attachView(mView);
    }

    @Test
    public void test_saveDescriptionChanges() {
        final String description = "changed_description";
        mPresenter.updateDescription(description);
        verify(mView).saveDescriptionChanges(description);
    }

    @Test
    public void test_setDescription() {
        final String description = "description";
        mPresenter.setDescription(description);
        verify(mView).populateDescription(description);
    }
}
