package com.appinstitute.beau.merchant.presentation.welcome;

import android.content.Intent;

import com.appinstitute.beau.merchant.presentation.RoboletricBaseTestRunner;
import com.appinstitute.beau.merchant.presentation.login.LoginActivity;
import com.appinstitute.beau.merchant.presentation.register.basic.RegistrationActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;


public class WelcomeActivityTest extends RoboletricBaseTestRunner {

    @Mock WelcomePresenter mWelcomePresenter;
    private WelcomeActivity mWelcomeActivity;
    private ActivityController<WelcomeActivity> mActivityActivityController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mActivityActivityController = Robolectric.buildActivity(WelcomeActivity.class).create().start().visible();
        mWelcomeActivity = mActivityActivityController.get();
        mWelcomeActivity.setPresenter(mWelcomePresenter);
    }

    @Test
    public void test_loginClickedShouldBeCalledAfterButtonPress() {
        mWelcomeActivity.onLoginClicked();
        verify(mWelcomePresenter).onLoginClicked();
    }

    @Test
    public void test_registerClickedShouldBeCalledAfterButtonPress() {
        mWelcomeActivity.onRegisterClicked();
        verify(mWelcomePresenter).onRegistrationClicked();
    }

    @Test
    public void test_LoginScreenShouldBeLaunched() {
        ShadowActivity shadowActivity = shadowOf(mWelcomeActivity);

        mWelcomeActivity.launchLoginScreen();
        Intent intent = new Intent(mWelcomeActivity, LoginActivity.class);

        assertTrue(shadowActivity.getNextStartedActivity().getComponent().equals(intent.getComponent()));
    }

    @Test
    public void test_onResumeShouldSetMovementMethod() {
        mWelcomeActivity.onResume();
        assertNotNull(mWelcomeActivity.mTermsAndPolicy.getMovementMethod());
    }

    @Test
    public void test_getLaunchIntentShouldNotBeNull() {
        assertNotNull(WelcomeActivity.getLaunchIntent(mWelcomeActivity));
    }

    @Test
    public void test_welcomeScreenShouldBeLunched() {
        ShadowActivity shadowActivity = shadowOf(mWelcomeActivity);

        mWelcomeActivity.launchRegistrationScreen();
        Intent intent = new Intent(mWelcomeActivity, RegistrationActivity.class);

        assertTrue(shadowActivity.getNextStartedActivity().getComponent().equals(intent.getComponent()));
    }

    @After
    public void tearDown() {
        mActivityActivityController.pause().stop().destroy();
        super.tearDown();
    }
}
