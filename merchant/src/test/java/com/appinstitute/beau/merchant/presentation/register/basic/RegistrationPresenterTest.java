package com.appinstitute.beau.merchant.presentation.register.basic;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Validator.class)
public class RegistrationPresenterTest {

    @Mock
    AuthHelper mAuthHelper;

    @Mock
    RegistrationView mRegistrationView;

    RegistrationPresenter mRegistrationPresenter;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Validator.class);
        MockitoAnnotations.initMocks(this);
        mRegistrationPresenter = new RegistrationPresenter();
        mRegistrationPresenter.attachView(mRegistrationView);
    }

    @Test
    public void test_emptyBusinessName() {
        when(Validator.isEmpty("")).thenReturn(true);
        mRegistrationPresenter.registerClicked("", "fgdsd", "dfsda");
        verify(mRegistrationView).incorrectFields();
    }

    @Test
    public void test_incorrectUserId() {
        when(Validator.isCorrectUserId("123")).thenReturn(false);
        mRegistrationPresenter.registerClicked("efdfds", "123", "dfsda");
        verify(mRegistrationView).incorrectUserId();
    }

    @Test
    public void test_incorrectPassword() {
        when(Validator.isCorrectPassword("dfsda")).thenReturn(false);
        when(Validator.isCorrectUserId(any())).thenReturn(true);
        mRegistrationPresenter.registerClicked("efdfds", "123456", "dfsda");
        verify(mRegistrationView).incorrectPassword();
    }

    @Test
    public void test_correctCredentials() {
        when(Validator.isCorrectPassword(anyString())).thenReturn(true);
        when(Validator.isCorrectUserId(anyString())).thenReturn(true);
        mRegistrationPresenter.registerClicked("mondoktamas1", "mondoktamas1", "Aa123456");
        final UserModel userModel = new UserModel("mondoktamas1", "mondoktamas1", "Aa123456");
        verify(mRegistrationView).continueRegistrationFlow(userModel);
    }
}
