package com.appinstitute.beau.merchant.util;

import com.appinstitute.beau.core.data.network.factory.RxErrorHandlingCallAdapterFactory;
import com.github.aurae.retrofit2.LoganSquareConverterFactory;

import retrofit2.Retrofit;

/**
 * Created by tomashmondok on 9/12/16.
 */
public class RetrofitTestUtil {

    public static Retrofit getInstance() {
        return new Retrofit.Builder()
                .baseUrl("http://google.com")
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(LoganSquareConverterFactory.create())
                .build();
    }
}
