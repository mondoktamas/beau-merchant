package com.appinstitute.beau.merchant.data.network.factory;

import com.appinstitute.beau.core.data.network.exception.RetrofitException;
import com.appinstitute.beau.core.data.network.factory.RxCallAdapterWrapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;

import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
public class RxCallAdapterWrapperTest {

    private Retrofit mRetrofit;
    private CallAdapter mCallAdapter;
    private RxCallAdapterWrapper mRxCallAdapterWrapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mRxCallAdapterWrapper = new RxCallAdapterWrapper(mRetrofit, mCallAdapter);
    }

    @Test
    public void test_shouldBeReturnHttpException() {
        Response response = Response.success(new Object());
        RetrofitException retrofitException = mRxCallAdapterWrapper.asRetrofitException(new HttpException(response));
        String retrofitExceptionName = retrofitException.getKind().name();

        assertTrue("Expected HTTP exception actual " + retrofitExceptionName, retrofitExceptionName.equals("HTTP"));
    }

    @Test
    public void test_shouldBeReturnTimeOutException() {
        RetrofitException retrofitException = mRxCallAdapterWrapper.asRetrofitException(new TimeoutException());
        String expectedIOException = retrofitException.getCause().getClass().getName();
        String expectedTimeoutException = retrofitException.getCause().getCause().getClass().getName();

        assertTrue("Expected IOException actual " + expectedIOException, expectedIOException.equals(IOException.class.getName()));
        assertTrue("Expected TimeoutException actual " + expectedTimeoutException, expectedTimeoutException.equals(TimeoutException.class.getName()));
    }

    @Test
    public void test_shouldBeReturnConnectException() {
        RetrofitException retrofitException = mRxCallAdapterWrapper.asRetrofitException(new ConnectException());
        String expectedIOException = retrofitException.getCause().getClass().getName();
        String expectedConnectException= retrofitException.getCause().getCause().getClass().getName();

        assertTrue("Expected IOException actual " + expectedIOException, expectedIOException.equals(IOException.class.getName()));
        assertTrue("Expected ConnectException actual " + expectedConnectException, expectedConnectException.equals(ConnectException.class.getName()));
    }

    @Test
    public void test_shouldBeReturnSocketTimeoutException() {
        RetrofitException retrofitException = mRxCallAdapterWrapper.asRetrofitException(new SocketTimeoutException());
        String expectedIOException = retrofitException.getCause().getClass().getName();
        String expectedSocketTimeoutException= retrofitException.getCause().getCause().getClass().getName();

        assertTrue("Expected IOException actual " + expectedIOException, expectedIOException.equals(IOException.class.getName()));
        assertTrue("Expected SocketTimeoutException actual " + expectedSocketTimeoutException, expectedSocketTimeoutException.equals(SocketTimeoutException.class.getName()));
    }

    @Test
    public void test_shouldBeReturnUnknownHostException() {
        RetrofitException retrofitException = mRxCallAdapterWrapper.asRetrofitException(new UnknownHostException());
        String expectedIOException = retrofitException.getCause().getClass().getName();
        String expectedUnknownHostException= retrofitException.getCause().getCause().getClass().getName();

        assertTrue("Expected IOException actual " + expectedIOException, expectedIOException.equals(IOException.class.getName()));
        assertTrue("Expected UnknownHostException actual " + expectedUnknownHostException, expectedUnknownHostException.equals(UnknownHostException.class.getName()));
    }

    @Test
    public void test_shouldBeReturnUnexpectedError() {
        RetrofitException retrofitException = mRxCallAdapterWrapper.asRetrofitException(new RuntimeException());
        String expectedRuntimeException= retrofitException.getCause().getClass().getName();

        assertTrue("Expected RuntimeException actual " + expectedRuntimeException, expectedRuntimeException.equals(RuntimeException.class.getName()));
    }
}
