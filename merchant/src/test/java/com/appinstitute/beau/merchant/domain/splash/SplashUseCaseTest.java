package com.appinstitute.beau.merchant.domain.splash;

import com.appinstitute.beau.core.domain.base.DefaultSubscriber;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SplashUseCaseTest {

    @Mock SplashUseCase mSplashUseCase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_executeShouldCreateDelayedObserver() {
        when(mSplashUseCase.execute(any())).thenCallRealMethod();
        when(mSplashUseCase.buildObservableTask()).thenCallRealMethod();
        mSplashUseCase.execute(new DefaultSubscriber());
        verify(mSplashUseCase).buildObservableTask();
    }
}
