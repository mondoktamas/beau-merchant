package com.appinstitute.beau.merchant.domain;

import com.appinstitute.beau.merchant.data.repository.booking.BookingRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class NewBookingsCountUseCaseTest {

    @Mock BookingRepository mBookingRepository;

    NewBookingsCountUseCase mNewBookingsCountUseCase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mNewBookingsCountUseCase = new NewBookingsCountUseCase(mBookingRepository);
    }

    @Test
    public void test_buildObservableTask() {
        mNewBookingsCountUseCase.buildObservableTask();
        verify(mBookingRepository).loadNewBookingsCount();
    }
}
