package com.appinstitute.beau.merchant.presentation.register.description;

import android.content.Intent;

import com.appinstitute.beau.merchant.presentation.RoboletricBaseTestRunner;
import com.appinstitute.beau.merchant.presentation.constants.Keys;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.util.ActivityController;

import static android.app.Activity.RESULT_OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.buildActivity;
import static org.robolectric.Shadows.shadowOf;

public class BusinessDescriptionActivityTest extends RoboletricBaseTestRunner {

    @Mock BusinessDescriptionPresenter mPresenter;
    private BusinessDescriptionActivity mBusinessDescriptionActivity;
    private ActivityController<BusinessDescriptionActivity> mActivityActivityController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mActivityActivityController = Robolectric.buildActivity(BusinessDescriptionActivity.class).create().start().visible();
        mBusinessDescriptionActivity = mActivityActivityController.get();
        mBusinessDescriptionActivity.setPresenter(mPresenter);
    }

    @Test
    public void test_getLaunchIntentShouldNotBeNull() {
        final Intent launchIntent = BusinessDescriptionActivity.getLaunchIntent(mBusinessDescriptionActivity, "description");
        assertNotNull(launchIntent);
        assertTrue(mBusinessDescriptionActivity.getIntent().getComponent().equals(launchIntent.getComponent()));
    }

    @Test
    public void test_onPostCreateShouldSetUserModel() {
        final Intent intent = new Intent();
        final String description = "description";
        intent.putExtra(Keys.Extras.DESCRIPTION, description);
        final BusinessDescriptionActivity businessDescriptionActivity =
                buildActivity(BusinessDescriptionActivity.class)
                        .withIntent(intent)
                        .create().get();
        businessDescriptionActivity.setPresenter(mPresenter);
        businessDescriptionActivity.onPostCreate(null);
        verify(mPresenter).setDescription(description);
    }

    @Test
    public void test_populateDescription() {
        final String description = "description";
        mBusinessDescriptionActivity.populateDescription(description);
        assertEquals(description, mBusinessDescriptionActivity.mBusinessDescription.getText().toString());
    }

    @Test
    public void test_onSaveClicked() {
        final String description = "description";
        mBusinessDescriptionActivity.mBusinessDescription.setText(description);
        mBusinessDescriptionActivity.onSaveClicked();
        verify(mPresenter).updateDescription(description);
    }

    @Test
    public void test_saveDescriptionChangesShouldFinishWithResult() {
        final String description = "new_description_text";
        final ShadowActivity shadowActivity = shadowOf(mBusinessDescriptionActivity);
        mBusinessDescriptionActivity.saveDescriptionChanges(description);
        assertEquals(shadowActivity.getResultCode(), RESULT_OK);
        assertTrue(shadowActivity.getResultIntent().hasExtra(Keys.Extras.DESCRIPTION));
        assertEquals(shadowActivity.getResultIntent().getStringExtra(Keys.Extras.DESCRIPTION), description);
        assertTrue(shadowActivity.isFinishing());
    }

    @Test
    public void test_backNavigation() {
        mBusinessDescriptionActivity.onSupportNavigateUp();
        final ShadowActivity shadowActivity = shadowOf(mBusinessDescriptionActivity);
        assertTrue(shadowActivity.isFinishing());
    }
}
