package com.appinstitute.beau.merchant.presentation.register.complete;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.widget.TextView;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.common.permission.PermissionChecker;
import com.appinstitute.beau.merchant.common.permission.SecurityException;
import com.appinstitute.beau.merchant.presentation.RoboletricBaseTestRunner;
import com.appinstitute.beau.merchant.presentation.constants.Keys;
import com.appinstitute.beau.merchant.presentation.model.UserModel;
import com.appinstitute.beau.merchant.presentation.register.complete.adapter.CountryAdapter;
import com.appinstitute.beau.merchant.presentation.register.description.BusinessDescriptionActivity;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowDialog;
import org.robolectric.util.ActivityController;

import pl.aprilapps.easyphotopicker.EasyImageConfig;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Robolectric.buildActivity;
import static org.robolectric.Shadows.shadowOf;

public class RegistrationCompleteActivityTest extends RoboletricBaseTestRunner {

    @Mock PermissionChecker mPermissionChecker;
    @Mock RegistrationCompletePresenter mPresenter;

    private RegistrationCompleteActivity mRegistrationCompleteActivity;
    private ActivityController<RegistrationCompleteActivity> mActivityActivityController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        final Intent intent = new Intent();
        intent.putExtra(Keys.Extras.USER, new UserModel("pApps", "mondoktamas1", "Aa123456"));
        mActivityActivityController = buildActivity(RegistrationCompleteActivity.class)
                .withIntent(intent)
                .create()
                .postCreate(null)
                .start()
                .visible();
        mRegistrationCompleteActivity = mActivityActivityController.get();
        mRegistrationCompleteActivity.setPresenter(mPresenter);
    }

    @Test
    public void test_getLaunchIntentShouldNotBeNull() {
        final Intent launchIntent = RegistrationCompleteActivity.getLaunchIntent(mRegistrationCompleteActivity,
                new UserModel("pApps", "mondoktamas", "Aa123456"));
        assertNotNull(launchIntent);
        assertTrue(mRegistrationCompleteActivity.getIntent().getComponent().equals(launchIntent.getComponent()));
    }

    @Test
    public void test_onPostCreateShouldSetUserModel() {
        final Intent intent = new Intent();
        final UserModel userModel = new UserModel("pApps", "mondoktamas1", "Aa123456");
        intent.putExtra(Keys.Extras.USER, userModel);
        final RegistrationCompleteActivity registrationCompleteActivity =
                buildActivity(RegistrationCompleteActivity.class)
                        .withIntent(intent)
                        .create().get();
        registrationCompleteActivity.setPresenter(mPresenter);
        registrationCompleteActivity.onPostCreate(null);
        assertNotNull(registrationCompleteActivity.mBehavior);
        verify(mPresenter).setUserModel(userModel);
    }

    @Test
    public void test_bottomSheetDialogShown() {
        mRegistrationCompleteActivity.mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        mRegistrationCompleteActivity.showBottomSheetDialog();
        final BottomSheetDialog dialog = (BottomSheetDialog) ShadowDialog.getLatestDialog();
        assertTrue(dialog.isShowing());
    }

    @Test
    public void test_bottomSheetDialogCountrySelected() {
        mRegistrationCompleteActivity.mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        mRegistrationCompleteActivity.showBottomSheetDialog();
        final BottomSheetDialog dialog = (BottomSheetDialog) ShadowDialog.getLatestDialog();
        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        assertNotNull(recyclerView);
        // workaround robolectric recyclerView issue
        recyclerView.measure(0,0);
        recyclerView.layout(0,0,100,1000);
        final CountryAdapter.ViewHolder viewHolder = (CountryAdapter.ViewHolder) recyclerView.findViewHolderForAdapterPosition(0);
        viewHolder.itemView.findViewById(R.id.text_country).performClick();
        assertFalse(dialog.isShowing());
        assertEquals(mRegistrationCompleteActivity.mCountry.getText().toString(),
                ((TextView) viewHolder.itemView.findViewById(R.id.text_country)).getText().toString());
    }

    @Test
    public void test_populateUserInfo() {
        final String businessName = "pApps";
        final String userID = "mondoktamas1";
        final String password = "Aa123456";
        mRegistrationCompleteActivity.populateUserInfo(new UserModel(businessName, userID, password));
        assertEquals(businessName, mRegistrationCompleteActivity.mBusinessName.getText().toString());
        assertEquals(userID, mRegistrationCompleteActivity.mUserId.getText().toString());
        assertEquals(password, mRegistrationCompleteActivity.mPassword.getText().toString());
    }

    @Test
    public void test_onResumeShouldSetMovementMethod() {
        mRegistrationCompleteActivity.onResume();
        assertNotNull(mRegistrationCompleteActivity.mTermsAndPrivacyCheckbox.getMovementMethod());
    }

    @Test
    public void test_onUserIdChanged() {
        final String userId = "new_user_id";
        mRegistrationCompleteActivity.onUserIdChanged(userId);
        verify(mPresenter).updateUserId(userId);
    }

    @Test
    public void test_onBusinessNameChanged() {
        final String businessName = "new_business_name";
        mRegistrationCompleteActivity.onBusinessNameChanged(businessName);
        verify(mPresenter).updateBusinessName(businessName);
    }

    @Test
    public void test_onPasswordChanged() {
        final SpannableStringBuilder password = new SpannableStringBuilder("new_password");
        mRegistrationCompleteActivity.onPasswordChanged(password);
        verify(mPresenter).updatePassword(password);
    }

    @Test
    public void test_onFirstAddressChanged() {
        final String firstAddress = "new_first_address";
        mRegistrationCompleteActivity.onFirstAddressChanged(firstAddress);
        verify(mPresenter).updateFirstAddress(firstAddress);
    }

    @Test
    public void test_onSecondAddressChanged() {
        final String secondAddress = "new_second_address";
        mRegistrationCompleteActivity.onSecondAddressChanged(secondAddress);
        verify(mPresenter).updateSecondAddress(secondAddress);
    }

    @Test
    public void test_onTownChanged() {
        final String town = "new_town";
        mRegistrationCompleteActivity.onTownChanged(town);
        verify(mPresenter).updateTown(town);
    }

    @Test
    public void test_onCountryChanged() {
        final String country = "new_country";
        mRegistrationCompleteActivity.onCountryChanged(country);
        verify(mPresenter).updateCountry(country);
    }

    @Test
    public void test_onPostCodeChanged() {
        final String postCode = "new_post_code";
        mRegistrationCompleteActivity.onPostCodeChanged(postCode);
        verify(mPresenter).updatePostCode(postCode);
    }

    @Test
    public void test_onEmailChanged() {
        final String email = "new_email";
        mRegistrationCompleteActivity.onEmailChanged(email);
        verify(mPresenter).updateEmail(email);
    }

    @Test
    public void test_onPhoneChanged() {
        final String phone = "0501004653";
        mRegistrationCompleteActivity.onPhoneChanged(phone);
        verify(mPresenter).updatePhone(phone);
    }

    @Test
    public void test_onTermsAndPolicyCheckChanged() {
        mRegistrationCompleteActivity.onTermsAndPolicyCheckChanged(true);
        verify(mPresenter).termsCheckChanged(true);
    }

    @Test
    public void test_onCompleteRegistrationClicked() {
        mRegistrationCompleteActivity.onCompleteRegistrationClicked();
        verify(mPresenter).completeRegistration();
    }

    @Test
    public void test_onAddDescriptionClicked() {
        mRegistrationCompleteActivity.onAddDescriptionClicked();
        verify(mPresenter).addDescription();
    }

    @Test
    public void test_onAddPhotoClicked_NoCameraPermission() throws SecurityException {
        mRegistrationCompleteActivity.setPermissionChecker(mPermissionChecker);
        doThrow(new SecurityException(Manifest.permission.CAMERA))
                .when(mPermissionChecker)
                .verifyPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        mRegistrationCompleteActivity.onAddPhotoClicked();
        verify(mPermissionChecker).verifyPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        verify(mPermissionChecker).requestPermissions(mRegistrationCompleteActivity.PERMISSION_REQUEST_CODE, Manifest.permission.CAMERA);
    }

//    @Test
//    public void test_onAddPhotoClick_OpensChooser() {
//
//        mRegistrationCompleteActivity.setPermissionChecker(mPermissionChecker);
//        doNothing().when(mPermissionChecker)
//                .verifyPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        ShadowActivity shadowActivity = shadowOf(mRegistrationCompleteActivity);
//        shadowActivity.grantPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        mRegistrationCompleteActivity.onAddPhotoClicked();
//        final ShadowActivity.IntentForResult intentForResult = shadowActivity.getNextStartedActivityForResult();
//        assertEquals(intentForResult.requestCode, EasyImage.REQ_SOURCE_CHOOSER);
//    }

    @Test
    public void test_incorrectUserIdShouldShowAlert() {
        mRegistrationCompleteActivity.incorrectUserId();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        Assert.assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_invalid_user_id));
    }

    @Test
    public void test_incorrectPasswordShouldShowAlert() {
        mRegistrationCompleteActivity.incorrectPassword();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        Assert.assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_invalid_password));
    }

    @Test
    public void test_incorrectEmailShouldShowAlert() {
        mRegistrationCompleteActivity.incorrectEmail();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_complete_invalid_email));
    }

    @Test
    public void test_incorrectPhoneNumberShouldShowAlert() {
        mRegistrationCompleteActivity.incorrectPhoneNumber();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        Assert.assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_complete_invalid_phone));
    }

    @Test
    public void test_incorrectFieldsShouldShowAlert() {
        mRegistrationCompleteActivity.incorrectFields();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        Assert.assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_validation_alert_message));
    }

    @Test
    public void test_onRegistrationShouldShowAlert() {
        final String registrationFailedError = "registration_failed";
        mRegistrationCompleteActivity.onRegistrationFailed(registrationFailedError);
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        Assert.assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), registrationFailedError);
    }

    @Test
    public void test_onFailedToGetAddress() {
        mRegistrationCompleteActivity.onFailedToGetAddress();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_complete_failed_to_get_address));
    }

    @Test
    public void test_onRegistrationSuccessShouldStartNextActivity() {
        ShadowActivity shadowActivity = shadowOf(mRegistrationCompleteActivity);

        mRegistrationCompleteActivity.onRegistrationSuccess(new UserModel());
        Intent intent = new Intent(mRegistrationCompleteActivity, TabActivity.class);
        assertTrue(shadowActivity.getNextStartedActivity().getComponent().equals(intent.getComponent()));
    }

    @Test
    public void test_openBusinessDescriptionScreenShouldStartNextActivity() {
        ShadowActivity shadowActivity = shadowOf(mRegistrationCompleteActivity);

        mRegistrationCompleteActivity.openBusinessDescriptionScreen("description");
        Intent intent = new Intent(mRegistrationCompleteActivity, BusinessDescriptionActivity.class);
        ShadowActivity.IntentForResult nextIntent = shadowActivity.getNextStartedActivityForResult();
        assertEquals(nextIntent.requestCode, BusinessDescriptionActivity.REQUEST_CODE);
        assertTrue(nextIntent.intent.getComponent().equals(intent.getComponent()));
    }

    @Test
    public void test_errorAcceptTermsAndPolicyShouldShowAlert() {
        mRegistrationCompleteActivity.errorAcceptTermsAndPolicy();
        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        Assert.assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_complete_error_accept_terms));
    }

    @Test
    public void test_onCountryClickedShouldShowAlert() {
        mRegistrationCompleteActivity.onCountryClicked();
        final BottomSheetDialog dialog = (BottomSheetDialog) ShadowDialog.getLatestDialog();
        assertTrue(dialog.isShowing());
    }

    @Test
    public void test_onSaveAndRestoreInstanceState() {

        final Intent intent = new Intent();
        final UserModel userModel = new UserModel("pApps", "mondoktamas1", "Aa123456");
        intent.putExtra(Keys.Extras.USER, userModel);

        final Bundle outState = new Bundle();

        ActivityController<RegistrationCompleteActivity> activityController =
                buildActivity(RegistrationCompleteActivity.class)
                        .withIntent(intent).create();

        //set mock presenter
        RegistrationCompleteActivity registrationCompleteActivity = activityController.get();
        registrationCompleteActivity.setPresenter(mPresenter);

        activityController = activityController
                .start()
                .resume()
                .visible();

        activityController.saveInstanceState(outState).pause().stop().destroy();

        assertEquals(userModel, outState.getParcelable(RegistrationCompleteActivity.ARG_USER_MODEL));

        //recreate activity
        activityController = Robolectric.buildActivity(RegistrationCompleteActivity.class)
                .withIntent(intent).create(outState);

        //set mock presenter
        registrationCompleteActivity = activityController.get();
        registrationCompleteActivity.setPresenter(mPresenter);

        activityController.start()
                .restoreInstanceState(outState)
                .resume()
                .visible();

        verify(mPresenter).setUserModel(outState.getParcelable(RegistrationCompleteActivity.ARG_USER_MODEL));
    }

    @Test
    public void test_onRequestPermissionsResultWrongRequestCode() {
        mRegistrationCompleteActivity.onRequestPermissionsResult(-1, new String[]{}, new int[]{});
    }

//    @Test
//    public void test_onRequestPermissionsResultPermissionGranted() {
//        mRegistrationCompleteActivity.setPermissionChecker(mPermissionChecker);
//        when(mPermissionChecker.permissionsGranted(any(), any())).thenReturn(true);
//        mRegistrationCompleteActivity.onRequestPermissionsResult(RegistrationCompleteActivity.PERMISSION_REQUEST_CODE,
//                new String[]{}, new int[]{});
//        verify(mPermissionChecker).verifyPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//    }

    @Test
    public void test_onRequestPermissionsResultPermissionDenied() {
        mRegistrationCompleteActivity.setPermissionChecker(mPermissionChecker);
        when(mPermissionChecker.permissionsGranted(any(), any())).thenReturn(false);
        mRegistrationCompleteActivity.onRequestPermissionsResult(RegistrationCompleteActivity.PERMISSION_REQUEST_CODE,
                new String[]{}, new int[]{});

        final AlertDialog latestAlert = ShadowAlertDialog.getLatestAlertDialog();
        Assert.assertTrue("Alert not showed", latestAlert.isShowing());
        assertEquals(shadowOf(latestAlert).getMessage(), resources.getString(R.string.register_complete_permission_not_granted));
    }

    @Test
    public void test_onActivityResultDescriptionAdded() {
        final String newDescription = "new_description";
        final Intent resultData = new Intent();
        resultData.putExtra(Keys.Extras.DESCRIPTION, newDescription);
        mRegistrationCompleteActivity.onActivityResult(BusinessDescriptionActivity.REQUEST_CODE,
                Activity.RESULT_OK, resultData);
        verify(mPresenter).updateDescription(newDescription);
    }

    @Test
    public void test_onActivityResultImageAdded() {
        final Intent resultData = new Intent();
        mRegistrationCompleteActivity.onActivityResult(EasyImageConfig.REQ_PICK_PICTURE_FROM_GALLERY,
                Activity.RESULT_OK, resultData);
    }

    @After
    public void tearDown() {
        mActivityActivityController.pause().stop().destroy();
        super.tearDown();
    }
}
