package com.appinstitute.beau.merchant.data.network.exception;

import com.appinstitute.beau.core.data.network.enumclass.Kind;
import com.appinstitute.beau.core.data.network.exception.RetrofitException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import retrofit2.Response;
import retrofit2.Retrofit;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;

@RunWith(PowerMockRunner.class)
public class RetrofitExceptionTest {

    private RetrofitException mRetrofitException;
    private Retrofit mRetrofit;

    @Before
    public void setUp() {
        mRetrofitException = new RetrofitException("test", "test", Response.success(anyObject()), Kind.HTTP, new RuntimeException(), mRetrofit);
    }

    @Test
    public void test_urlShouldBeRight() {
        assertTrue("Url is wrong actual " + mRetrofitException.getUrl(), mRetrofitException.getUrl().equals("test"));
    }

    @Test
    public void test_responseShouldBeRight() {
        int responseCode = mRetrofitException.getResponse().code();
        String responseMessage = mRetrofitException.getResponse().message();

        assertTrue("Response code is wrong actual " + responseCode, responseCode == 200);
        assertTrue("Response message is wrong actual " + responseMessage, responseMessage.equals("OK"));
    }

    @Test
    public void test_kindShouldBeRight() {
        Kind kind = mRetrofitException.getKind();
        assertTrue("Enum kid is wrong actual " + kind, kind == Kind.HTTP);
    }

    @Test
    public void test_retrofitExceptionShouldBeReturnRetrofit() {
        assertTrue("Retrofit object is wrong", mRetrofitException.getRetrofit() == mRetrofit);
    }
}
