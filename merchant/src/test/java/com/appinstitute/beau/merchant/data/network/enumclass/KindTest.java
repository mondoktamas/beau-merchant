package com.appinstitute.beau.merchant.data.network.enumclass;

import com.appinstitute.beau.core.data.network.enumclass.Kind;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
public class KindTest {

    @Test
    public void test_shouldBeReturnHttpEnum() {
        assertTrue("Enum HTTP is wrong, actual " + Kind.HTTP.toString(), Kind.HTTP.toString().equals("HTTP"));
    }

    @Test
    public void test_shouldBeReturnNetworkEnum() {
        assertTrue("Enum NETWORK is wrong, actual " + Kind.NETWORK.toString(), Kind.NETWORK.toString().equals("NETWORK"));
    }

    @Test
    public void test_shouldBeReturnUnexpectedEnum() {
        assertTrue("Enum UNEXPECTED is wrong, actual " + Kind.UNEXPECTED.toString(), Kind.UNEXPECTED.toString().equals("UNEXPECTED"));
    }

    @Test
    public void test_shouldBeReturnAllValues() {
        List<String> kinds = new ArrayList<>(Arrays.asList("NETWORK", "HTTP", "UNEXPECTED"));
        for (int i = 0; i < Kind.values().length; i++) {
            assertTrue("Wrong enum expected " + kinds.get(i) + " actual " + Kind.values()[i], Kind.values()[i].toString().equals(kinds.get(i)));
        }
    }

    @Test
    public void test_shouldBeReturnNetworkKind() {
        assertTrue("Expected Kind.NETWORK, actual Kind.valueOf(\"NETWORK\") ", Kind.valueOf("NETWORK") == Kind.NETWORK);
    }

    @Test
    public void test_shouldBeReturnHttpKind() {
        assertTrue("Expected Kind.HTTP, actual Kind.valueOf(\"HTTP\") ", Kind.valueOf("HTTP") == Kind.HTTP);
    }

    @Test
    public void test_shouldBeReturnUnexpectedKind() {
        assertTrue("Expected Kind.UNEXPECTED, actual Kind.valueOf(\"HTTP\") ", Kind.valueOf("UNEXPECTED") == Kind.UNEXPECTED);
    }
}
