package com.appinstitute.beau.merchant.presentation.base.view;

import com.appinstitute.beau.merchant.presentation.RoboletricBaseTestRunner;
import com.appinstitute.beau.merchant.presentation.splash.SplashActivity;
import com.appinstitute.beau.merchant.presentation.splash.SplashPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;

/**
 * Created by tomashmondok on 1/13/17.
 */

public class BaseActivityTest extends RoboletricBaseTestRunner {

    @Mock SplashPresenter mSplashPresenter;

    private SplashActivity mSplashActivity;
    private ActivityController<SplashActivity> mActivityActivityController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mActivityActivityController = Robolectric.buildActivity(SplashActivity.class).create();
        mSplashActivity = mActivityActivityController.get();
    }

    @Test
    public void test_activityShouldBeNotNull() {
        assertNotNull("Base activity is null", mSplashActivity);
    }

    @Test
    public void test_presenterShouldNotBeNull() {
        assertNotNull("Base presenter is null", mSplashActivity.getPresenter());
    }

    @Test
    public void test_navigatorShouldNotBeNull() {
        assertNotNull("Navigator is null", mSplashActivity.getNavigator());
    }

    @Test
    public void test_presenterShouldBeDetached() {
        mSplashActivity.setPresenter(mSplashPresenter);
        mSplashActivity.onDestroy();
        verify(mSplashPresenter).detachView();
    }
}
