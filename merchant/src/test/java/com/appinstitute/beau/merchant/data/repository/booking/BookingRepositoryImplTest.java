package com.appinstitute.beau.merchant.data.repository.booking;

import com.appinstitute.beau.merchant.data.network.service.ApiService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class BookingRepositoryImplTest {

    @Mock ApiService mApiService;
    BookingRepositoryImpl mBookingRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mBookingRepository = new BookingRepositoryImpl(mApiService);
    }

    @Test
    public void test_loadNewBookingsCount() {
        mBookingRepository.loadNewBookingsCount();
        verify(mApiService).loadNewBookingsCount();
    }
}
