package com.appinstitute.beau.merchant.data.repository.account;

import com.appinstitute.beau.merchant.data.network.model.request.ContactRequest;
import com.appinstitute.beau.merchant.data.network.service.ApiService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class AccountRepositoryImplTest {

    @Mock ApiService mApiService;
    AccountRepositoryImpl mAccountRepositoryImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mAccountRepositoryImpl = new AccountRepositoryImpl(mApiService);
    }

    @Test
    public void test_setContactDetails() {
        final ContactRequest contactRequest = new ContactRequest();
        mAccountRepositoryImpl.setContactDetails(contactRequest);
        verify(mApiService).setContactDetails(contactRequest);
    }
}
