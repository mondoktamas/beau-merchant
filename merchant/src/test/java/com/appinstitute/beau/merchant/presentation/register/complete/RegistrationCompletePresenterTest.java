package com.appinstitute.beau.merchant.presentation.register.complete;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.data.network.exception.RetrofitException;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.merchant.domain.auth.ImageToBase64UseCase;
import com.appinstitute.beau.merchant.domain.auth.RegisterUseCase;
import com.appinstitute.beau.merchant.domain.auth.UserAddressUseCase;
import com.appinstitute.beau.merchant.presentation.model.UserModel;
import com.appinstitute.beau.merchant.util.RetrofitTestUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Subscription;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Validator.class)
@PowerMockIgnore("javax.net.ssl.*")
public class RegistrationCompletePresenterTest {

    @Mock ErrorHelper mErrorHelper;
    @Mock AuthHelper<UserModel> mAuthHelper;
    @Mock RegistrationCompleteView mRegistrationCompleteView;
    @Mock RegisterUseCase mRegisterUseCase;
    @Mock ImageToBase64UseCase mImageToBase64UseCase;
    @Mock UserAddressUseCase mUserAddressUseCase;

    @Captor ArgumentCaptor<RegistrationCompletePresenter.ConvertImageSubscriber> mConvertImageSubscriberArgumentCaptor;
    @Captor ArgumentCaptor<RegistrationCompletePresenter.RegistrationSubscriber> mRegistrationSubscriberArgumentCaptor;
    @Captor ArgumentCaptor<RegistrationCompletePresenter.UserAddressSubscriber> mUserAddressSubscriberArgumentCaptor;

    RegistrationCompletePresenter mPresenter;
    Retrofit mRetrofit;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Validator.class);
        MockitoAnnotations.initMocks(this);
        mPresenter = new RegistrationCompletePresenter(mRegisterUseCase,
                mUserAddressUseCase, mImageToBase64UseCase);
        mPresenter.attachView(mRegistrationCompleteView);
        mPresenter.setErrorHelper(mErrorHelper);
        mPresenter.setAuthHelper(mAuthHelper);

        mRetrofit = RetrofitTestUtil.getInstance();
    }

    @Test
    public void test_setUserModel() {
        final UserModel userModel = new UserModel("mondoktamas1", "mondoktamas1", "Aa123456");
        mPresenter.setUserModel(userModel);
        verify(mRegistrationCompleteView).populateUserInfo(userModel);
    }

    @Test
    public void test_termsCheckChanged() {
        mPresenter.termsCheckChanged(true);
        assertTrue(mPresenter.mTermsAndPolicyChecked);
        mPresenter.termsCheckChanged(false);
        assertFalse(mPresenter.mTermsAndPolicyChecked);
    }

    @Test
    public void test_updateUserId() {
        final String newUserId = "new_user_id";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateUserId(newUserId);
        assertEquals(newUserId, mPresenter.mUserModel.getUserId());
    }

    @Test
    public void test_updateBusinessName() {
        final String newBusinessName = "new_business_name";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateBusinessName(newBusinessName);
        assertEquals(newBusinessName, mPresenter.mUserModel.getBusinessName());
    }

    @Test
    public void test_updatePassword() {
        final String newPassword = "new_password";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updatePassword(newPassword);
        assertEquals(newPassword, mPresenter.mUserModel.getPassword());
    }

    @Test
    public void test_updateFirstAddress() {
        final String newFirstAddress = "new_first_address_line";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateFirstAddress(newFirstAddress);
        assertEquals(newFirstAddress, mPresenter.mUserModel.getAddressLineOne());
    }

    @Test
    public void test_updateSecondAddress() {
        final String newSecondAddressLine = "new_second_address_line";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateSecondAddress(newSecondAddressLine);
        assertEquals(newSecondAddressLine, mPresenter.mUserModel.getAddressLineTwo());
    }

    @Test
    public void test_updateTown() {
        final String newTown = "new_town";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateTown(newTown);
        assertEquals(newTown, mPresenter.mUserModel.getTown());
    }

    @Test
    public void test_updateCountry() {
        final String newCountry = "new_country";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateCountry(newCountry);
        assertEquals(newCountry, mPresenter.mUserModel.getCountry());
    }

    @Test
    public void test_updatePostCode() {
        final String newPostCode = "new_post_code";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updatePostCode(newPostCode);
        assertEquals(newPostCode, mPresenter.mUserModel.getPostCode());
    }

    @Test
    public void test_updateEmail() {
        final String newEmail = "mondok.tamas1+23@gmail.com";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateEmail(newEmail);
        assertEquals(newEmail, mPresenter.mUserModel.getEmail());
    }

    @Test
    public void test_updatePhone() {
        final String newPhone = "0501243243";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updatePhone(newPhone);
        assertEquals(newPhone, mPresenter.mUserModel.getPhone());
    }

    @Test
    public void test_updateDescription() {
        final String newDescription = "new_description_text";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateDescription(newDescription);
        assertEquals(newDescription, mPresenter.mUserModel.getDescription());
    }

    @Test
    public void test_addDescription() {
        final String newDescription = "new_description_text";
        mPresenter.setUserModel(new UserModel());
        mPresenter.updateDescription(newDescription);
        mPresenter.addDescription();
        verify(mRegistrationCompleteView).openBusinessDescriptionScreen(newDescription);
    }

    @Test
    public void test_updatePhoto() {
        final File newPhoto = new File("/sdcard/Downloads/photo.jpg");
        mPresenter.setUserModel(new UserModel());
        mPresenter.updatePhoto(newPhoto);
        assertEquals(newPhoto.getAbsolutePath(), mPresenter.mUserModel.getPathToImageFile());
    }

    @Test
    public void test_completeRegistrationTermsNotAccepted() {
        mPresenter.completeRegistration();
        verify(mRegistrationCompleteView).errorAcceptTermsAndPolicy();
    }

    @Test
    public void test_completeRegistrationEmptyUserFields() {
        when(Validator.isEmpty(anyString())).thenReturn(true);
        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(new UserModel());
        mPresenter.completeRegistration();
        verify(mRegistrationCompleteView).incorrectFields();
    }

    @Test
    public void test_completeRegistrationWrongUserId() {
        when(Validator.isCorrectUserId("123456")).thenReturn(false);

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelIncorrectUser());
        mPresenter.completeRegistration();

        verify(mRegistrationCompleteView).incorrectUserId();
    }

    @Test
    public void test_completeRegistrationWrongPassword() {
        when(Validator.isCorrectUserId("123456")).thenReturn(true);
        when(Validator.isCorrectPassword("dfsda")).thenReturn(false);

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelIncorrectUser());
        mPresenter.completeRegistration();

        verify(mRegistrationCompleteView).incorrectPassword();
    }

    @Test
    public void test_completeRegistrationWrongEmail() {
        when(Validator.isCorrectUserId("123456")).thenReturn(true);
        when(Validator.isCorrectPassword("dfsda")).thenReturn(true);
        when(Validator.isCorrectEmail("wrong_email")).thenReturn(false);

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelIncorrectUser());
        mPresenter.completeRegistration();

        verify(mRegistrationCompleteView).incorrectEmail();
    }

    @Test
    public void test_completeRegistrationWrongPhone() {
        when(Validator.isCorrectUserId("123456")).thenReturn(true);
        when(Validator.isCorrectPassword("dfsda")).thenReturn(true);
        when(Validator.isCorrectEmail("wrong_email")).thenReturn(true);
        when(Validator.isCorrectPhone("1234567890")).thenReturn(false);

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelIncorrectUser());
        mPresenter.completeRegistration();

        verify(mRegistrationCompleteView).incorrectPhoneNumber();
    }

    @Test
    public void test_completeRegistrationWithImage() {
        when(Validator.isEmpty(anyString())).thenReturn(false);
        when(Validator.isCorrectPhone(anyString())).thenReturn(true);
        when(Validator.isCorrectEmail(anyString())).thenReturn(true);
        when(Validator.isCorrectUserId(anyString())).thenReturn(true);
        when(Validator.isCorrectPassword(anyString())).thenReturn(true);

        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mImageToBase64UseCase).execute(any());
        doReturn(subscription).when(mUserAddressUseCase).execute(any());

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelWithImageUri());
        mPresenter.completeRegistration();

        verify(mImageToBase64UseCase).setImageFilePath(new File("/sdcard/Downloads/image.jpg").getAbsolutePath());
        verify(mImageToBase64UseCase).execute(mConvertImageSubscriberArgumentCaptor.capture());

        mConvertImageSubscriberArgumentCaptor.getValue().onStart();
        verify(mRegistrationCompleteView).showProgress(true);

        mConvertImageSubscriberArgumentCaptor.getValue().onNext("base_64_string");
        assertEquals(mPresenter.mUserModel.getImage(), "base_64_string");

        mConvertImageSubscriberArgumentCaptor.getValue().onCompleted();
        verify(mUserAddressUseCase).setUserModel(mPresenter.mUserModel);
    }

    @Test
    public void test_completeRegistrationWithImage_ErrorConvertingToBase64() throws Exception {
        when(Validator.isEmpty(anyString())).thenReturn(false);
        when(Validator.isCorrectPhone(anyString())).thenReturn(true);
        when(Validator.isCorrectEmail(anyString())).thenReturn(true);
        when(Validator.isCorrectUserId(anyString())).thenReturn(true);
        when(Validator.isCorrectPassword(anyString())).thenReturn(true);

        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mImageToBase64UseCase).execute(any());
        doReturn(subscription).when(mUserAddressUseCase).execute(any());

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelWithImageUri());
        mPresenter.completeRegistration();

        verify(mImageToBase64UseCase).setImageFilePath(new File("/sdcard/Downloads/image.jpg").getAbsolutePath());
        verify(mImageToBase64UseCase).execute(mConvertImageSubscriberArgumentCaptor.capture());

        mConvertImageSubscriberArgumentCaptor.getValue().onStart();
        verify(mRegistrationCompleteView).showProgress(true);

        mConvertImageSubscriberArgumentCaptor.getValue().onError(new Throwable());
        verify(mRegistrationCompleteView).showProgress(false);
    }

    @Test
    public void test_completeRegistrationWithoutImage() {
        when(Validator.isEmpty(anyString())).thenReturn(false);
        when(Validator.isCorrectPhone(anyString())).thenReturn(true);
        when(Validator.isCorrectEmail(anyString())).thenReturn(true);
        when(Validator.isCorrectUserId(anyString())).thenReturn(true);
        when(Validator.isCorrectPassword(anyString())).thenReturn(true);

        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mUserAddressUseCase).execute(any());
        doReturn(subscription).when(mRegisterUseCase).execute(any());

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelWithoutImageUri());
        mPresenter.completeRegistration();

        verify(mUserAddressUseCase).setUserModel(mPresenter.mUserModel);
        verify(mUserAddressUseCase).execute(mUserAddressSubscriberArgumentCaptor.capture());

        mUserAddressSubscriberArgumentCaptor.getValue().onStart();
        verify(mRegistrationCompleteView).showProgress(true);

        mUserAddressSubscriberArgumentCaptor.getValue().onCompleted();
        verify(mRegisterUseCase).setUserModel(mPresenter.mUserModel);
    }

    @Test
    public void test_completeRegistrationWithoutImage_ErrorGetLocation() {
        when(Validator.isEmpty(anyString())).thenReturn(false);
        when(Validator.isCorrectPhone(anyString())).thenReturn(true);
        when(Validator.isCorrectEmail(anyString())).thenReturn(true);
        when(Validator.isCorrectUserId(anyString())).thenReturn(true);
        when(Validator.isCorrectPassword(anyString())).thenReturn(true);

        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mUserAddressUseCase).execute(any());
        doReturn(subscription).when(mRegisterUseCase).execute(any());

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelWithoutImageUri());
        mPresenter.completeRegistration();

        verify(mUserAddressUseCase).setUserModel(mPresenter.mUserModel);
        verify(mUserAddressUseCase).execute(mUserAddressSubscriberArgumentCaptor.capture());

        mUserAddressSubscriberArgumentCaptor.getValue().onStart();
        verify(mRegistrationCompleteView).showProgress(true);

        mUserAddressSubscriberArgumentCaptor.getValue().onError(RetrofitException.unexpectedError(new Throwable()));
        verify(mRegistrationCompleteView).showProgress(false);
        verify(mRegistrationCompleteView).onFailedToGetAddress();
    }

    @Test
    public void test_completeRegistrationWithoutImage_SuccessRegister() {
        when(Validator.isEmpty(anyString())).thenReturn(false);
        when(Validator.isCorrectPhone(anyString())).thenReturn(true);
        when(Validator.isCorrectEmail(anyString())).thenReturn(true);
        when(Validator.isCorrectUserId(anyString())).thenReturn(true);
        when(Validator.isCorrectPassword(anyString())).thenReturn(true);

        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mUserAddressUseCase).execute(any());
        doReturn(subscription).when(mRegisterUseCase).execute(any());

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelWithoutImageUri());
        mPresenter.completeRegistration();

        verify(mUserAddressUseCase).setUserModel(mPresenter.mUserModel);
        verify(mUserAddressUseCase).execute(mUserAddressSubscriberArgumentCaptor.capture());

        mUserAddressSubscriberArgumentCaptor.getValue().onStart();
        verify(mRegistrationCompleteView).showProgress(true);

        mUserAddressSubscriberArgumentCaptor.getValue().onCompleted();
        verify(mRegisterUseCase).setUserModel(mPresenter.mUserModel);

        verify(mRegisterUseCase).execute(mRegistrationSubscriberArgumentCaptor.capture());
        final UserModel userModel = getLoggedInUserModel();
        mRegistrationSubscriberArgumentCaptor.getValue().onNext(userModel);
        verify(mAuthHelper).setUserData(userModel);

        mRegistrationSubscriberArgumentCaptor.getValue().onCompleted();
        verify(mRegistrationCompleteView).showProgress(false);
    }

    @Test
    public void test_completeRegistrationWithoutImage_FailureRegister() {
        when(Validator.isEmpty(anyString())).thenReturn(false);
        when(Validator.isCorrectPhone(anyString())).thenReturn(true);
        when(Validator.isCorrectEmail(anyString())).thenReturn(true);
        when(Validator.isCorrectUserId(anyString())).thenReturn(true);
        when(Validator.isCorrectPassword(anyString())).thenReturn(true);

        final Subscription subscription = mock(Subscription.class);
        doReturn(subscription).when(mUserAddressUseCase).execute(any());
        doReturn(subscription).when(mRegisterUseCase).execute(any());

        mPresenter.termsCheckChanged(true);
        mPresenter.setUserModel(getUserModelWithoutImageUri());
        mPresenter.completeRegistration();

        verify(mUserAddressUseCase).setUserModel(mPresenter.mUserModel);
        verify(mUserAddressUseCase).execute(mUserAddressSubscriberArgumentCaptor.capture());

        mUserAddressSubscriberArgumentCaptor.getValue().onStart();
        verify(mRegistrationCompleteView).showProgress(true);

        mUserAddressSubscriberArgumentCaptor.getValue().onCompleted();
        verify(mRegisterUseCase).setUserModel(mPresenter.mUserModel);

        verify(mRegisterUseCase).execute(mRegistrationSubscriberArgumentCaptor.capture());
        final ResponseBody errorResponse = ResponseBody.create(MediaType.parse("application/json"),
                "{\"result\":false,\"errors\":[\"user_not_found\"]}");
        final RetrofitException retrofitException = RetrofitException.httpError(null,
                Response.error(422, errorResponse), mRetrofit);
        mRegistrationSubscriberArgumentCaptor.getValue().onError(retrofitException);
        verify(mErrorHelper).getErrorString(new String[] {"user_not_found"});
        verify(mRegistrationCompleteView).showProgress(false);

    }

    private UserModel getUserModelIncorrectUser() {
        final UserModel userModel = new UserModel("efdfds", "123456", "dfsda");
        userModel.setEmail("wrong_email");
        userModel.setPhone("1234567890");
        return userModel;
    }

    private UserModel getUserModelWithImageUri() {
        final UserModel userModel = new UserModel("pApps", "mondoktamas", "Aa123456");
        userModel.setPathToImageFile(new File("/sdcard/Downloads/image.jpg").getAbsolutePath());
        return userModel;
    }

    private UserModel getUserModelWithoutImageUri() {
        final UserModel userModel = new UserModel("pApps", "mondoktamas", "Aa123456");
        return userModel;
    }

    private UserModel getLoggedInUserModel() {
        final UserModel userModel = new UserModel("pApps", "mondoktamas", "Aa123456");
        userModel.setSessionId("session_id_token");
        userModel.setImage("mnmblkjbkjbbkjbk");
        return userModel;
    }
}
