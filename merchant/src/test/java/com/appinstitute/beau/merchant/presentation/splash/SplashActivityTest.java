package com.appinstitute.beau.merchant.presentation.splash;

import android.content.Intent;

import com.appinstitute.beau.merchant.presentation.RoboletricBaseTestRunner;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;
import com.appinstitute.beau.merchant.presentation.welcome.WelcomeActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

public class SplashActivityTest extends RoboletricBaseTestRunner {

    @Mock
    SplashPresenter mSplashPresenter;
    private SplashActivity mSplashActivity;
    private ActivityController<SplashActivity> mActivityActivityController;

    @Before
    @Override
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mActivityActivityController = Robolectric.buildActivity(SplashActivity.class).create().start().visible();
        mSplashActivity = mActivityActivityController.get();
        mSplashActivity.setPresenter(mSplashPresenter);
    }

    @Test
    public void test_activityShouldBeNotNull() {
        assertNotNull("Splash activity is null", mSplashActivity);
    }

    @Test
    public void test_onResumeShouldStartDelayedTask() {
        mSplashActivity.onResume();
        verify(mSplashPresenter).startSplashTask();
    }

    @Test
    public void test_welcomeScreenShouldBeLunched() {
        ShadowActivity shadowActivity = shadowOf(mSplashActivity);

        mSplashActivity.launchNextScreen(false);
        Intent intent = new Intent(mSplashActivity, WelcomeActivity.class);
        assertTrue(shadowActivity.getNextStartedActivity().getComponent().equals(intent.getComponent()));
        assertTrue(shadowActivity.isFinishing());
    }

    @Test
    public void test_homeScreenShouldBeLanched() {
        ShadowActivity shadowActivity = shadowOf(mSplashActivity);

        mSplashActivity.launchNextScreen(true);
        Intent intent = new Intent(mSplashActivity, TabActivity.class);
        assertTrue(shadowActivity.getNextStartedActivity().getComponent().equals(intent.getComponent()));
        assertTrue(shadowActivity.isFinishing());
    }

    @After
    @Override
    public void tearDown() {
        mActivityActivityController.pause().stop().destroy();
        super.tearDown();
    }
}