package com.appinstitute.beau.merchant.domain.account;

import com.appinstitute.beau.merchant.data.repository.account.AccountRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class SetContactDetailsTest {

    @Mock AccountRepository mAccountRepository;

    SetContactDetails mSetContactDetails;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mSetContactDetails = new SetContactDetails(mAccountRepository);
    }

    @Test
    public void test_buildObservableTask() {
        mSetContactDetails.buildObservableTask();
        verify(mAccountRepository).setContactDetails(null);
    }
}
