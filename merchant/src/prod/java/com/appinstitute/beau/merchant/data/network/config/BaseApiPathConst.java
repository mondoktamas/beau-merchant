package com.appinstitute.beau.merchant.data.network.config;

public interface BaseApiPathConst {

    String API_BASE_URL = "http://beautybookings.appus.work/";
    String API_PATH = "merchant/";

}