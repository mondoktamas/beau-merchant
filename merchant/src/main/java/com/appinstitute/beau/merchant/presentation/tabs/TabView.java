package com.appinstitute.beau.merchant.presentation.tabs;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface TabView extends BaseView<TabPresenter> {
}
