package com.appinstitute.beau.merchant.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.appinstitute.beau.core.presentation.base.model.BaseModel;

public final class UserModel extends BaseModel implements Parcelable {

    private String mSessionId;
    private String mBusinessName;
    private String mUserId;
    private String mPassword;
    private String mAddressLineOne;
    private String mAddressLineTwo = "";
    private String mTown;
    private String mCountry;
    private String mPostCode;
    private String mEmail;
    private String mPhone;
    private double mLatitude;
    private double mLongitude;
    private String mDescription = "";
    private String mImage;
    private String mPathToImageFile;
    private String mPushToken;

    public UserModel() { }

    public UserModel(final String businessName,
                     final String userId,
                     final String password) {
        mBusinessName = businessName;
        mUserId = userId;
        mPassword = password;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setBusinessName(final String businessName) { mBusinessName = businessName; }

    public void setUserId(final String userId) { mUserId = userId; }

    public void setPassword(final String password) { mPassword = password; }

    public String getAddressLineOne() {
        return mAddressLineOne;
    }

    public void setAddressLineOne(final String addressLineOne) {
        mAddressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return mAddressLineTwo;
    }

    public void setAddressLineTwo(final String addressLineTwo) {
        mAddressLineTwo = addressLineTwo;
    }

    public String getTown() {
        return mTown;
    }

    public void setTown(final String town) {
        mTown = town;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(final String country) {
        mCountry = country;
    }

    public String getPostCode() {
        return mPostCode;
    }

    public void setPostCode(final String postCode) {
        mPostCode = postCode;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(final String email) {
        mEmail = email;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(final String phone) {
        mPhone = phone;
    }

    public void setLatitude(final double latitude) {
        mLatitude = latitude;
    }

    public void setLongitude(final double longitude) {
        mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getImage() {
        return mImage;
    }

    public String getPushToken() {
        return mPushToken;
    }

    public void setDescription(final String description) {
        mDescription = description;
    }

    public void setImage(final String image) {
        mImage = image;
    }

    public void setPushToken(final String pushToken) {
        mPushToken = pushToken;
    }

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(final String sessionId) {
        mSessionId = sessionId;
    }

    public String getPathToImageFile() {
        return mPathToImageFile;
    }

    public void setPathToImageFile(final String pathToImageFile) {
        mPathToImageFile = pathToImageFile;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "mSessionId='" + mSessionId + '\'' +
                ", mBusinessName='" + mBusinessName + '\'' +
                ", mUserId='" + mUserId + '\'' +
                ", mPassword='" + mPassword + '\'' +
                ", mAddressLineOne='" + mAddressLineOne + '\'' +
                ", mAddressLineTwo='" + mAddressLineTwo + '\'' +
                ", mTown='" + mTown + '\'' +
                ", mCountry='" + mCountry + '\'' +
                ", mPostCode='" + mPostCode + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mPhone='" + mPhone + '\'' +
                ", mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                ", mDescription='" + mDescription + '\'' +
                ", mImage='" + mImage + '\'' +
                ", mPushToken='" + mPushToken + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mSessionId);
        dest.writeString(this.mBusinessName);
        dest.writeString(this.mUserId);
        dest.writeString(this.mPassword);
        dest.writeString(this.mAddressLineOne);
        dest.writeString(this.mAddressLineTwo);
        dest.writeString(this.mTown);
        dest.writeString(this.mCountry);
        dest.writeString(this.mPostCode);
        dest.writeString(this.mEmail);
        dest.writeString(this.mPhone);
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeString(this.mDescription);
        dest.writeString(this.mImage);
        dest.writeString(this.mPathToImageFile);
        dest.writeString(this.mPushToken);
    }

    protected UserModel(Parcel in) {
        this.mSessionId = in.readString();
        this.mBusinessName = in.readString();
        this.mUserId = in.readString();
        this.mPassword = in.readString();
        this.mAddressLineOne = in.readString();
        this.mAddressLineTwo = in.readString();
        this.mTown = in.readString();
        this.mCountry = in.readString();
        this.mPostCode = in.readString();
        this.mEmail = in.readString();
        this.mPhone = in.readString();
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
        this.mDescription = in.readString();
        this.mImage = in.readString();
        this.mPathToImageFile = in.readString();
        this.mPushToken = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof UserModel)) return false;

        UserModel userModel = (UserModel) o;

        if (Double.compare(userModel.mLatitude, mLatitude) != 0) return false;
        if (Double.compare(userModel.mLongitude, mLongitude) != 0) return false;
        if (mSessionId != null ? !mSessionId.equals(userModel.mSessionId) : userModel.mSessionId != null)
            return false;
        if (mBusinessName != null ? !mBusinessName.equals(userModel.mBusinessName) : userModel.mBusinessName != null)
            return false;
        if (mUserId != null ? !mUserId.equals(userModel.mUserId) : userModel.mUserId != null)
            return false;
        if (mPassword != null ? !mPassword.equals(userModel.mPassword) : userModel.mPassword != null)
            return false;
        if (mAddressLineOne != null ? !mAddressLineOne.equals(userModel.mAddressLineOne) : userModel.mAddressLineOne != null)
            return false;
        if (mAddressLineTwo != null ? !mAddressLineTwo.equals(userModel.mAddressLineTwo) : userModel.mAddressLineTwo != null)
            return false;
        if (mTown != null ? !mTown.equals(userModel.mTown) : userModel.mTown != null) return false;
        if (mCountry != null ? !mCountry.equals(userModel.mCountry) : userModel.mCountry != null)
            return false;
        if (mPostCode != null ? !mPostCode.equals(userModel.mPostCode) : userModel.mPostCode != null)
            return false;
        if (mEmail != null ? !mEmail.equals(userModel.mEmail) : userModel.mEmail != null)
            return false;
        if (mPhone != null ? !mPhone.equals(userModel.mPhone) : userModel.mPhone != null)
            return false;
        if (mDescription != null ? !mDescription.equals(userModel.mDescription) : userModel.mDescription != null)
            return false;
        if (mImage != null ? !mImage.equals(userModel.mImage) : userModel.mImage != null)
            return false;
        if (mPathToImageFile != null ? !mPathToImageFile.equals(userModel.mPathToImageFile) : userModel.mPathToImageFile != null)
            return false;
        return mPushToken != null ? mPushToken.equals(userModel.mPushToken) : userModel.mPushToken == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = mSessionId != null ? mSessionId.hashCode() : 0;
        result = 31 * result + (mBusinessName != null ? mBusinessName.hashCode() : 0);
        result = 31 * result + (mUserId != null ? mUserId.hashCode() : 0);
        result = 31 * result + (mPassword != null ? mPassword.hashCode() : 0);
        result = 31 * result + (mAddressLineOne != null ? mAddressLineOne.hashCode() : 0);
        result = 31 * result + (mAddressLineTwo != null ? mAddressLineTwo.hashCode() : 0);
        result = 31 * result + (mTown != null ? mTown.hashCode() : 0);
        result = 31 * result + (mCountry != null ? mCountry.hashCode() : 0);
        result = 31 * result + (mPostCode != null ? mPostCode.hashCode() : 0);
        result = 31 * result + (mEmail != null ? mEmail.hashCode() : 0);
        result = 31 * result + (mPhone != null ? mPhone.hashCode() : 0);
        temp = Double.doubleToLongBits(mLatitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(mLongitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
        result = 31 * result + (mImage != null ? mImage.hashCode() : 0);
        result = 31 * result + (mPathToImageFile != null ? mPathToImageFile.hashCode() : 0);
        result = 31 * result + (mPushToken != null ? mPushToken.hashCode() : 0);
        return result;
    }
}
