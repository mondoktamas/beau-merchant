package com.appinstitute.beau.merchant.di.module;

import android.content.Context;

import com.appinstitute.beau.core.di.annotations.ActivityContext;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.presentation.navigation.Navigation;
import com.appinstitute.beau.merchant.presentation.navigation.NavigationImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private BaseActivity mActivity;

    public ActivityModule(final BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    BaseActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context providesContext() {
        return mActivity;
    }

    @Provides
    Navigation provideNavigation(final BaseActivity baseActivity) {
        return new NavigationImpl(baseActivity);
    }
}
