package com.appinstitute.beau.merchant.presentation.navigation;

import android.content.Intent;
import android.support.v4.app.FragmentManager;

import com.appinstitute.beau.core.di.annotations.PerActivity;
import com.appinstitute.beau.merchant.presentation.base.view.BaseFragment;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;

import javax.inject.Inject;

@PerActivity
public class NavigationImpl implements Navigation {

    private final BaseActivity mBaseActivity;

    @Inject
    public NavigationImpl(final BaseActivity context) {
        mBaseActivity = context;
    }

    @Override
    public void openFragment(final BaseFragment fragment) {
        final int fragmentContainerId = mBaseActivity.getFragmentContainer();

        if (fragmentContainerId == 0)
            throw new RuntimeException("Layout resource id should be provided. Check the method description");

        final FragmentManager fragmentManager = mBaseActivity.getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .add(fragmentContainerId, fragment)
                .commit();
    }

    @Override
    public void startActivity(final Intent launchIntent) {
        mBaseActivity.startActivity(launchIntent);
    }

    @Override
    public void startActivity(final Intent launchIntent, final boolean finishCurrent) {
        startActivity(launchIntent);
        mBaseActivity.finish();
    }

    @Override
    public void startActivityForResult(final Intent launchIntent, final int requestCode) {
        mBaseActivity.startActivityForResult(launchIntent, requestCode);
    }
}
