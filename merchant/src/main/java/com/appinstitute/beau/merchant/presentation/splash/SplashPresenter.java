package com.appinstitute.beau.merchant.presentation.splash;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.domain.base.DefaultSubscriber;
import com.appinstitute.beau.merchant.domain.splash.SplashUseCase;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;

import javax.inject.Inject;

import rx.Subscription;

@ConfigPersistent
public class SplashPresenter extends BasePresenter<SplashView> {

    private final AuthHelper mAuthHelper;
    private final SplashUseCase mSplashUseCase;

    @Inject
    public SplashPresenter(final AuthHelper authHelper, final SplashUseCase splashUseCase) {
        mAuthHelper = authHelper;
        mSplashUseCase = splashUseCase;
    }

    public void startSplashTask() {
        final Subscription subscription = mSplashUseCase.execute(new SplashSubscriber());
        unsubscribeOnDestroy(subscription);
    }

    public class SplashSubscriber extends DefaultSubscriber {

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().launchNextScreen(mAuthHelper.isLoggedIn());
        }
    }
}
