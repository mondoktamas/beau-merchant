package com.appinstitute.beau.merchant.presentation.register.complete;

import com.appinstitute.beau.core.presentation.base.view.BaseView;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

public interface RegistrationCompleteView extends BaseView<RegistrationCompletePresenter> {

    void populateUserInfo(final UserModel userModel);

    void incorrectFields();

    void errorAcceptTermsAndPolicy();

    void incorrectUserId();

    void incorrectPassword();

    void incorrectEmail();

    void incorrectPhoneNumber();

    void onRegistrationFailed(final String errorString);

    void onRegistrationSuccess(final UserModel userModel);

    void onFailedToGetAddress();

    void openBusinessDescriptionScreen(final String description);
}
