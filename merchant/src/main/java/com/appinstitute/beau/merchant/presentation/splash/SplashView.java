package com.appinstitute.beau.merchant.presentation.splash;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface SplashView extends BaseView<SplashPresenter> {
    void launchNextScreen(final boolean isLoggedIn);
}
