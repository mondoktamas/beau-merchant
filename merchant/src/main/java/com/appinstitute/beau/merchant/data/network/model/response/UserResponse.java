package com.appinstitute.beau.merchant.data.network.model.response;

import com.appinstitute.beau.core.data.network.model.response.BaseResponse;
import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class UserResponse extends BaseResponse {

    @JsonField(name = "session_id") private String mSessionId;
    @JsonField(name = "user_id") private String mUserId;
    @JsonField(name = "business_name") private String mBusinessName;
    @JsonField(name = "address_1") private String mAddressLineOne;
    @JsonField(name = "address_2") private String mAddressLineTwo;
    @JsonField(name = "town") private String mTown;
    @JsonField(name = "country") private String mCountry;
    @JsonField(name = "post_code") private String mPostCode;
    @JsonField(name = "email") private String mEmail;
    @JsonField(name = "phone") private String mPhone;
    @JsonField(name = "description") private String mDescription;
    @JsonField(name = "image_id") private String mImageId;
    @JsonField(name = "image_path") private String mImagePath;

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(String sessionId) {
        mSessionId = sessionId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    public String getAddressLineOne() {
        return mAddressLineOne;
    }

    public void setAddressLineOne(String addressLineOne) {
        mAddressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return mAddressLineTwo;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        mAddressLineTwo = addressLineTwo;
    }

    public String getTown() {
        return mTown;
    }

    public void setTown(String town) {
        mTown = town;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getPostCode() {
        return mPostCode;
    }

    public void setPostCode(String postCode) {
        mPostCode = postCode;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImageId() {
        return mImageId;
    }

    public void setImageId(String imageId) {
        mImageId = imageId;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof UserResponse)) return false;

        UserResponse that = (UserResponse) o;

        if (mSessionId != null ? !mSessionId.equals(that.mSessionId) : that.mSessionId != null)
            return false;
        if (mUserId != null ? !mUserId.equals(that.mUserId) : that.mUserId != null) return false;
        if (mBusinessName != null ? !mBusinessName.equals(that.mBusinessName) : that.mBusinessName != null)
            return false;
        if (mAddressLineOne != null ? !mAddressLineOne.equals(that.mAddressLineOne) : that.mAddressLineOne != null)
            return false;
        if (mAddressLineTwo != null ? !mAddressLineTwo.equals(that.mAddressLineTwo) : that.mAddressLineTwo != null)
            return false;
        if (mTown != null ? !mTown.equals(that.mTown) : that.mTown != null) return false;
        if (mCountry != null ? !mCountry.equals(that.mCountry) : that.mCountry != null)
            return false;
        if (mPostCode != null ? !mPostCode.equals(that.mPostCode) : that.mPostCode != null)
            return false;
        if (mEmail != null ? !mEmail.equals(that.mEmail) : that.mEmail != null) return false;
        if (mPhone != null ? !mPhone.equals(that.mPhone) : that.mPhone != null) return false;
        if (mDescription != null ? !mDescription.equals(that.mDescription) : that.mDescription != null)
            return false;
        if (mImageId != null ? !mImageId.equals(that.mImageId) : that.mImageId != null)
            return false;
        return mImagePath != null ? mImagePath.equals(that.mImagePath) : that.mImagePath == null;

    }

    @Override
    public int hashCode() {
        int result = mSessionId != null ? mSessionId.hashCode() : 0;
        result = 31 * result + (mUserId != null ? mUserId.hashCode() : 0);
        result = 31 * result + (mBusinessName != null ? mBusinessName.hashCode() : 0);
        result = 31 * result + (mAddressLineOne != null ? mAddressLineOne.hashCode() : 0);
        result = 31 * result + (mAddressLineTwo != null ? mAddressLineTwo.hashCode() : 0);
        result = 31 * result + (mTown != null ? mTown.hashCode() : 0);
        result = 31 * result + (mCountry != null ? mCountry.hashCode() : 0);
        result = 31 * result + (mPostCode != null ? mPostCode.hashCode() : 0);
        result = 31 * result + (mEmail != null ? mEmail.hashCode() : 0);
        result = 31 * result + (mPhone != null ? mPhone.hashCode() : 0);
        result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
        result = 31 * result + (mImageId != null ? mImageId.hashCode() : 0);
        result = 31 * result + (mImagePath != null ? mImagePath.hashCode() : 0);
        return result;
    }
}
