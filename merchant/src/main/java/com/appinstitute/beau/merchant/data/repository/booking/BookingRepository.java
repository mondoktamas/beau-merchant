package com.appinstitute.beau.merchant.data.repository.booking;

import com.appinstitute.beau.merchant.data.network.model.response.NewBookingsCountResponse;

import rx.Observable;

public interface BookingRepository {

    Observable<NewBookingsCountResponse> loadNewBookingsCount();
}
