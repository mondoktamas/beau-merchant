package com.appinstitute.beau.merchant.common.authentication;

import android.content.Context;
import android.content.SharedPreferences;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AuthHelperImpl implements AuthHelper<UserModel> {

    private static final String PREF_USER = "user_pref";
    private static final String PREF_USER_TOKEN = "session_id";
    private static final String PREF_USER_ID = "user_id";
    private static final String PREF_USER_BUSINESS_NAME = "business_name";
    private static final String PREF_USER_ADDRESS_1 = "address_line_one";
    private static final String PREF_USER_ADDRESS_2 = "address_line_two";
    private static final String PREF_USER_TOWN = "town";
    private static final String PREF_USER_COUNTRY = "country";
    private static final String PREF_USER_POSTCODE = "post_code";
    private static final String PREF_USER_EMAIL = "email";
    private static final String PREF_USER_PHONE = "phone";
    private static final String PREF_USER_DESCRIPTION = "description";
    private static final String PREF_USER_ACCOUNT_NAME = "account_name";
    private static final String PREF_USER_ACCOUNT_NUMBER = "account_number";
    private static final String PREF_USER_SORT_CODE = "sort_code";
    private static final String PREF_USER_IMAGE_ID = "image_id";

    private static final String PREF_USER_ONE_HOUR_NOTIFICATION = "one_hour_notification";
    private static final String PREF_USER_ONE_TEN_MINUTES_NOTIFICATION = "ten_minutes_notification";
    private static final String PREF_USER_TEN_MINUTES_EMAIL = "ten_minutes_email";
    private static final String PREF_USER_NEW_NOTIFICATION = "new_notification";
    private static final String PREF_USER_NEW_EMAIL = "new_email";
    private static final String PREF_USER_NEW_APPOUNTMENT_REQUEST_NOTIFICATION = "new_appointment_request_notification";
    private static final String PREF_USER_NEW_APPOUNTMENT_REQUEST_EMAIL = "new_appointment_request_email";
    private static final String PREF_USER_PUSH_TOKEN = "push_token";

    SharedPreferences mPreferences;

    private UserModel mUserModel;

    @Inject
    public AuthHelperImpl(final Context context) {
        mPreferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
    }

    @Override
    public void setUserData(final UserModel userData) {
        mUserModel.setSessionId(userData.getSessionId());
        mUserModel.setUserId(userData.getUserId());
        mUserModel.setBusinessName(userData.getBusinessName());
        mUserModel.setAddressLineOne(userData.getAddressLineOne());
        mUserModel.setAddressLineTwo(userData.getAddressLineTwo());
        mUserModel.setTown(userData.getTown());
        mUserModel.setCountry(userData.getCountry());
        mUserModel.setPostCode(userData.getPostCode());
        mUserModel.setEmail(userData.getEmail());
        mUserModel.setPhone(userData.getPhone());
        mUserModel.setDescription(userData.getDescription());


        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PREF_USER_TOKEN, mUserModel.getSessionId());
        editor.putString(PREF_USER_ID, mUserModel.getUserId());
        editor.putString(PREF_USER_BUSINESS_NAME, mUserModel.getBusinessName());
        editor.putString(PREF_USER_EMAIL, mUserModel.getEmail());
        editor.putString(PREF_USER_ADDRESS_1, mUserModel.getAddressLineOne());
        editor.putString(PREF_USER_ADDRESS_2, mUserModel.getAddressLineTwo());
        editor.putString(PREF_USER_TOWN, mUserModel.getTown());
        editor.putString(PREF_USER_COUNTRY, mUserModel.getCountry());
        editor.putString(PREF_USER_POSTCODE, mUserModel.getPostCode());
        editor.putString(PREF_USER_PHONE, mUserModel.getPhone());
        editor.putString(PREF_USER_DESCRIPTION, mUserModel.getDescription());

        editor.apply();
    }

    @Override
    public UserModel getUserModel() {
        if (mUserModel != null) {
            return mUserModel;
        } else {
            UserModel user = new UserModel();
            user.setSessionId(mPreferences.getString(PREF_USER_TOKEN, ""));
            user.setUserId(mPreferences.getString(PREF_USER_ID, ""));
            user.setBusinessName(mPreferences.getString(PREF_USER_BUSINESS_NAME, ""));
            user.setAddressLineOne(mPreferences.getString(PREF_USER_ADDRESS_1, ""));
            user.setAddressLineTwo(mPreferences.getString(PREF_USER_ADDRESS_2, ""));
            user.setTown(mPreferences.getString(PREF_USER_TOWN, ""));
            user.setCountry(mPreferences.getString(PREF_USER_COUNTRY, ""));
            user.setPostCode(mPreferences.getString(PREF_USER_POSTCODE, ""));
            user.setEmail(mPreferences.getString(PREF_USER_EMAIL, ""));
            user.setPhone(mPreferences.getString(PREF_USER_PHONE, ""));
            user.setDescription(mPreferences.getString(PREF_USER_DESCRIPTION, ""));

            return user;
        }
    }

    @Override
    public String getToken() {
        if (mUserModel != null) {
            return mUserModel.getSessionId();
        } else {
            mUserModel = getUserModel();
            return mUserModel.getSessionId();
        }
    }

    @Override
    public void releaseData() {
        mUserModel = null;
        mPreferences.edit().clear().apply();
    }

    @Override
    public boolean isLoggedIn() {
        if (getToken() != null && !getToken().equals("")) {
            return true;
        }

        return false;
    }
}
