package com.appinstitute.beau.merchant.presentation.constants;

public interface Keys {
    
    interface Args {

        String ARG_TAB_POSITION = "arg_tab_position";
    }

    interface Extras {

        String USER = "com.appinstitute.beau.user";
        String DESCRIPTION = "com.appinstitute.beau.business_description";


        String EXTRA_CURRENT_TAB = "com.appinstitute.beau.merchant.current_tab";
        String EXTRA_CURRENT_FRAGMENT_ARGS = "com.appinstitute.beau.merchant.current_fragment_args";
    }

    interface Prefs {

    }
}
