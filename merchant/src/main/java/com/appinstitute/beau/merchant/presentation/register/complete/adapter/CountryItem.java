package com.appinstitute.beau.merchant.presentation.register.complete.adapter;

public final class CountryItem {

    private String mTitle;

    public CountryItem(final String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }
}
