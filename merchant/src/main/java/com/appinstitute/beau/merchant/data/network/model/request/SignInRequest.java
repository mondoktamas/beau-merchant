package com.appinstitute.beau.merchant.data.network.model.request;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class SignInRequest {

    @JsonField(name="user_id") private String mUserId;

    @JsonField(name="password") private String mPassword;

    @JsonField(name = "push_token") private String mPushToken;

    public SignInRequest() {
    }

    public SignInRequest(final String userId, final String password, final String pushToken) {
        mUserId = userId;
        mPassword = password;
        mPushToken = pushToken;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPushToken() {
        return mPushToken;
    }

    public void setPushToken(String pushToken) {
        mPushToken = pushToken;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof SignInRequest)) return false;

        SignInRequest that = (SignInRequest) o;

        if (mUserId != null ? !mUserId.equals(that.mUserId) : that.mUserId != null) return false;
        if (mPassword != null ? !mPassword.equals(that.mPassword) : that.mPassword != null)
            return false;
        return mPushToken != null ? mPushToken.equals(that.mPushToken) : that.mPushToken == null;

    }

    @Override
    public int hashCode() {
        int result = mUserId != null ? mUserId.hashCode() : 0;
        result = 31 * result + (mPassword != null ? mPassword.hashCode() : 0);
        result = 31 * result + (mPushToken != null ? mPushToken.hashCode() : 0);
        return result;
    }
}
