package com.appinstitute.beau.merchant.di.module;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.merchant.data.repository.UserDataMapper;
import com.appinstitute.beau.merchant.data.repository.account.AccountRepository;
import com.appinstitute.beau.merchant.data.repository.account.AccountRepositoryImpl;
import com.appinstitute.beau.merchant.data.repository.auth.AuthRepository;
import com.appinstitute.beau.merchant.data.repository.auth.AuthRepositoryImpl;
import com.appinstitute.beau.merchant.data.repository.booking.BookingRepository;
import com.appinstitute.beau.merchant.data.repository.booking.BookingRepositoryImpl;
import com.appinstitute.beau.merchant.data.repository.booking.OffersRepository;
import com.appinstitute.beau.merchant.data.repository.booking.OffersRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @ConfigPersistent
    @Provides
    public AuthRepository provideAuthServiceRepository(final ApiService apiService,
                                                       final UserDataMapper userDataMapper) {
        return new AuthRepositoryImpl(apiService, userDataMapper);
    }

    @ConfigPersistent
    @Provides
    public AccountRepository provideAccountRepository(final ApiService apiService) {
        return new AccountRepositoryImpl(apiService);
    }

    @ConfigPersistent
    @Provides
    public BookingRepository provideBookingRepository(final ApiService apiService) {
        return new BookingRepositoryImpl(apiService);
    }

    @ConfigPersistent
    @Provides
    public OffersRepository provideOffersRepository(final ApiService apiService) {
        return new OffersRepositoryImpl(apiService);
    }
}
