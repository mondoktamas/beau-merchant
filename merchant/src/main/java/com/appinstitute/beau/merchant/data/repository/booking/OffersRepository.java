package com.appinstitute.beau.merchant.data.repository.booking;

import com.appinstitute.beau.merchant.data.network.model.response.OpenOffersCountResponse;

import rx.Observable;

public interface OffersRepository {
    Observable<OpenOffersCountResponse> loadOpenOffersCount();
}
