package com.appinstitute.beau.merchant.presentation.tabs.profile;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.merchant.data.network.model.response.NewBookingsCountResponse;
import com.appinstitute.beau.merchant.data.network.model.response.OpenOffersCountResponse;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.merchant.domain.NewBookingsCountUseCase;
import com.appinstitute.beau.merchant.domain.OpenOffersCountUseCase;
import com.appinstitute.beau.core.domain.base.DefaultInternetSubscriber;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.merchant.domain.auth.LogoutUseCase;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import javax.inject.Inject;

import static com.appinstitute.beau.core.common.DateUtils.getFormatedDate;

@ConfigPersistent
public class ProfilePresenter extends BasePresenter<ProfileView> {

    private AuthHelper mAuthHelper;
    private ErrorHelper mErrorHelper;
    private NewBookingsCountUseCase mNewBookingsCountUseCase;
    private OpenOffersCountUseCase mOpenOffersCountUseCase;
    private LogoutUseCase mLogoutUseCase;

    @Inject
    public ProfilePresenter(final AuthHelper authHelper,
                            final ErrorHelper errorHelper,
                            final NewBookingsCountUseCase newBookingsCountUseCase,
                            final OpenOffersCountUseCase openOffersCountUseCase,
                            final LogoutUseCase logoutUseCase) {
        mAuthHelper = authHelper;
        mErrorHelper = errorHelper;
        mNewBookingsCountUseCase = newBookingsCountUseCase;
        mOpenOffersCountUseCase = openOffersCountUseCase;
        mLogoutUseCase = logoutUseCase;
    }

    public void loadBusinessName() {
        getView().setBusinessName(((UserModel)mAuthHelper.getUserModel()).getBusinessName());
    }

    public void loadNewBookingsCount() {
        unsubscribeOnDestroy(mNewBookingsCountUseCase.execute(new NewBookingsCountSubscriber()));
    }

    public void loadCurrentDate() {
        getView().setCurrentDate(getFormatedDate());
    }

    public void loadOpenOffersCount() {
        unsubscribeOnDestroy(mOpenOffersCountUseCase.execute(new OpenOffersCountSubscriber()));
    }

    public void loadImageUrl() {
        getView().setImage(((UserModel)mAuthHelper.getUserModel()).getImage());
    }

    public void logout() {
        unsubscribeOnDestroy(mLogoutUseCase.execute(new LogoutSubscriber()));
    }

    class NewBookingsCountSubscriber extends DefaultInternetSubscriber<NewBookingsCountResponse> {

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }

        @Override
        public void onNext(final NewBookingsCountResponse newBookingsCountResponse) {
            if (!isViewAttached()) return;
            getView().setNewBookingsCount(newBookingsCountResponse.getCount());
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }
    }

    class OpenOffersCountSubscriber extends DefaultInternetSubscriber<OpenOffersCountResponse> {

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }

        @Override
        public void onNext(final OpenOffersCountResponse openOffersCountResponse) {
            if (!isViewAttached()) return;
            getView().setOpenOffersCount(openOffersCountResponse.getCount());
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }
    }

    public class LogoutSubscriber extends DefaultInternetSubscriber {
        @Override
        public void onError(final Throwable throwable) {
            super.onError(throwable);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void handleUnprocessableEntity(final String[] errors) {
            if (!isViewAttached()) return;
            getView().onLogoutFailed(mErrorHelper.getErrorString(errors));
        }

        @Override
        public void onCompleted() {
            mAuthHelper.releaseData();
            if (!isViewAttached()) return;
            getView().showProgress(false);
            getView().onLogoutSuccess();
        }

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }
    }
}
