package com.appinstitute.beau.merchant.di.module;

import com.appinstitute.beau.merchant.data.repository.account.AccountRepository;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.merchant.domain.account.SetContactDetails;

import dagger.Module;
import dagger.Provides;

@Module
public class InteractorModule {

    @ConfigPersistent
    @Provides
    public SetContactDetails provideContactDetailsInteractor(final AccountRepository accountRepository) {
        return new SetContactDetails(accountRepository);
    }


}
