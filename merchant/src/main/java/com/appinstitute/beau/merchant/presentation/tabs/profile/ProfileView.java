package com.appinstitute.beau.merchant.presentation.tabs.profile;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface ProfileView extends BaseView<ProfilePresenter> {
    void setBusinessName(String businessName);

    void setNewBookingsCount(int count);

    void setCurrentDate(String dateString);

    void setOpenOffersCount(int count);

    void setImage(String imageUrl);

    void onLogoutSuccess();

    void onLogoutFailed(String errorString);
}
