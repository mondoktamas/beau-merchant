package com.appinstitute.beau.merchant.presentation.register.basic;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputEditText;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.presentation.model.UserModel;
import com.appinstitute.beau.merchant.presentation.register.complete.RegistrationCompleteActivity;

import butterknife.BindView;
import butterknife.OnClick;

public final class RegistrationActivity extends BaseActivity<RegistrationPresenter>
        implements RegistrationView {

    @BindView(R.id.edit_business_name) TextInputEditText mBusinessName;
    @BindView(R.id.edit_user_id) TextInputEditText mUserId;
    @BindView(R.id.edit_password) TextInputEditText mPassword;

    @BindView(R.id.text_terms_and_policy) TextView mTermsAndPolicy;
    @BindView(R.id.text_register) TextView mRegisterText;

    public static Intent getLaunchIntent(final BaseActivity baseActivity) {
        return new Intent(baseActivity, RegistrationActivity.class);
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_registration;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTermsAndPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        mRegisterText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OnClick(R.id.button_register)
    void onRegisterClicked() {
        getPresenter().registerClicked(mBusinessName.getText().toString().trim(),
                mUserId.getText().toString().trim(),
                mPassword.getText().toString().trim());
    }

    @Override
    public void incorrectUserId() {
        showAlert(R.string.register_invalid_user_id);
    }

    @Override
    public void incorrectFields() {
        showAlert(R.string.register_validation_alert_message);
    }

    @Override
    public void incorrectPassword() {
        showAlert(R.string.register_invalid_password);
    }

    private void showAlert(final @StringRes int message) {
        new AlertDialog.Builder(this, R.style.App_Theme_AlertDialogInfo)
                .setTitle(R.string.register_validation_alert_title)
                .setMessage(message)
                .setPositiveButton(R.string.register_validation_alert_button,
                        (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    public void continueRegistrationFlow(final UserModel userModel) {
        getNavigator().startActivity(RegistrationCompleteActivity.getLaunchIntent(this, userModel));
    }
}
