package com.appinstitute.beau.merchant.data.repository;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.merchant.data.network.model.request.UserRequest;
import com.appinstitute.beau.merchant.data.network.model.response.UserResponse;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public class UserDataMapper {

    @Inject
    public UserDataMapper() {}

    public Observable<UserRequest> transform(final UserModel userModel) {
        return Observable.just(userModel)
                .map(userModel1 -> {
                    final UserRequest userRequest = new UserRequest();
                    userRequest.setUserId(userModel.getUserId());
                    userRequest.setPhone(userModel.getPhone());
                    userRequest.setEmail(userModel.getEmail());
                    userRequest.setAddressLineOne(userModel.getAddressLineOne());
                    userRequest.setAddressLineTwo(userModel.getAddressLineTwo());
                    userRequest.setBusinessName(userModel.getBusinessName());
                    userRequest.setPassword(userModel.getPassword());
                    userRequest.setCountry(userModel.getCountry());
                    userRequest.setPostCode(userModel.getPostCode());
                    userRequest.setTown(userModel.getTown());
                    userRequest.setLatitude(userModel.getLatitude());
                    userRequest.setLongitude(userModel.getLongitude());
                    userRequest.setDescription(userModel.getDescription());
                    userRequest.setImage(userModel.getImage());
                    userRequest.setPushToken(userModel.getPushToken());
                    return userRequest;
                });
    }

    public Observable<UserModel> transform(final UserResponse userResponse) {
        return Observable.just(userResponse)
                .map(userResponse1 -> {
                    final UserModel userModel = new UserModel();
                    userModel.setSessionId(userResponse1.getSessionId());
                    userModel.setUserId(userResponse1.getUserId());
                    userModel.setBusinessName(userResponse1.getBusinessName());
                    userModel.setAddressLineOne(userResponse1.getAddressLineOne());
                    userModel.setAddressLineTwo(userResponse1.getAddressLineTwo());
                    userModel.setTown(userResponse1.getTown());
                    userModel.setCountry(userResponse1.getCountry());
                    userModel.setPostCode(userResponse1.getPostCode());
                    userModel.setEmail(userResponse1.getEmail());
                    userModel.setPhone(userResponse1.getPhone());
                    userModel.setDescription(userResponse1.getDescription());
//                    userModel.setImageId(userResponse1.getImageId());
                    userModel.setImage(userResponse1.getImagePath());
                    return userModel;
                });
    }
}
