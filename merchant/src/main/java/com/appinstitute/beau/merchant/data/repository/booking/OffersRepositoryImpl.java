package com.appinstitute.beau.merchant.data.repository.booking;

import com.appinstitute.beau.merchant.data.network.model.response.OpenOffersCountResponse;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public class OffersRepositoryImpl implements OffersRepository {

    private final ApiService mApiService;

    @Inject
    public OffersRepositoryImpl(final ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Observable<OpenOffersCountResponse> loadOpenOffersCount() {
        return mApiService.loadOpenOffersCount();
    }
}
