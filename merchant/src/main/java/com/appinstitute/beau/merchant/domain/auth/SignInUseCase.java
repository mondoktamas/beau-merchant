package com.appinstitute.beau.merchant.domain.auth;

import com.appinstitute.beau.merchant.data.repository.auth.AuthRepository;
import com.appinstitute.beau.core.domain.base.BackgroundUseCase;

import javax.inject.Inject;

import rx.Observable;

public class SignInUseCase extends BackgroundUseCase {

    private String mUserId;
    private String mPassword;

    private AuthRepository mAuthRepository;

    @Inject
    public SignInUseCase(final AuthRepository repository) {
        mAuthRepository = repository;
    }

    public void setData(final String userID, final String password) {
        mUserId = userID;
        mPassword = password;
    }
    @Override
    protected Observable buildObservableTask() {
        return mAuthRepository.signIn(mUserId, mPassword);
    }
}
