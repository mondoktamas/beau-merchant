package com.appinstitute.beau.merchant.domain.auth;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.merchant.data.repository.auth.AuthRepository;

import javax.inject.Inject;

import rx.Observable;

public class LogoutUseCase extends BackgroundUseCase {

    private final AuthRepository mAuthRepository;

    @Inject
    public LogoutUseCase(final AuthRepository authRepository) {
        mAuthRepository = authRepository;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAuthRepository.logout();
    }
}
