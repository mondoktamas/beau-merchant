package com.appinstitute.beau.merchant.presentation.tabs;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;

import javax.inject.Inject;

@ConfigPersistent
public class TabPresenter extends BasePresenter<TabView> {

    @Inject
    public TabPresenter() {

    }
}
