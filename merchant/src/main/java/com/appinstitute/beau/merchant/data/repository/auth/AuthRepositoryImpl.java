package com.appinstitute.beau.merchant.data.repository.auth;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.merchant.data.network.model.request.SignInRequest;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.merchant.data.repository.UserDataMapper;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public class AuthRepositoryImpl implements AuthRepository {

    private final ApiService mApiService;
    private final UserDataMapper mUserDataMapper;

    @Inject
    public AuthRepositoryImpl(final ApiService apiService, final UserDataMapper userDataMapper) {
        mApiService = apiService;
        mUserDataMapper = userDataMapper;
    }

    @Override
    public Observable<UserModel> signIn(final String userId, final String password) {
        return mApiService.signIn(new SignInRequest(userId, password, null))
                .flatMap(mUserDataMapper::transform);
    }

    @Override
    public Observable<UserModel> registerUser(final UserModel userModel) {
        return mUserDataMapper.transform(userModel)
                .flatMap(mApiService::register)
                .flatMap(mUserDataMapper::transform);
    }

    @Override
    public Observable<Void> logout() {
        return mApiService.logout()
                .map(baseResponse -> null);
    }
}
