package com.appinstitute.beau.merchant.presentation.register.description;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;

import javax.inject.Inject;

@ConfigPersistent
public class BusinessDescriptionPresenter extends BasePresenter<BusinessDescriptionView> {

    private String mDescription;

    @Inject
    public BusinessDescriptionPresenter() { }

    public void updateDescription(final String description) {
        mDescription = description;
        getView().saveDescriptionChanges(mDescription);
    }

    public void setDescription(final String description) {
        mDescription = description;
        getView().populateDescription(mDescription);
    }
}
