package com.appinstitute.beau.merchant.presentation.register.complete;

import android.support.annotation.VisibleForTesting;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.domain.base.DefaultInternetSubscriber;
import com.appinstitute.beau.core.domain.base.DefaultSubscriber;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.merchant.domain.auth.ImageToBase64UseCase;
import com.appinstitute.beau.merchant.domain.auth.RegisterUseCase;
import com.appinstitute.beau.merchant.domain.auth.UserAddressUseCase;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import java.io.File;

import javax.inject.Inject;

@ConfigPersistent
public class RegistrationCompletePresenter extends BasePresenter<RegistrationCompleteView> {

    @Inject ErrorHelper mErrorHelper;
    @Inject AuthHelper mAuthHelper;

    private final RegisterUseCase mRegisterUseCase;
    private final UserAddressUseCase mUserAddressUseCase;
    private final ImageToBase64UseCase mImageToBase64UseCase;

    UserModel mUserModel;
    boolean mTermsAndPolicyChecked;

    @VisibleForTesting
    void setErrorHelper(final ErrorHelper errorHelper) {
        mErrorHelper = errorHelper;
    }

    @VisibleForTesting
    void setAuthHelper(final AuthHelper authHelper) {
        mAuthHelper = authHelper;
    }

    @Inject
    public RegistrationCompletePresenter(final RegisterUseCase registerUseCase,
                                         final UserAddressUseCase userAddressUseCase,
                                         final ImageToBase64UseCase imageToBase64UseCase) {
        mRegisterUseCase = registerUseCase;
        mUserAddressUseCase = userAddressUseCase;
        mImageToBase64UseCase = imageToBase64UseCase;
    }

    public void setUserModel(final UserModel userModel) {
        mUserModel = userModel;
        getView().populateUserInfo(mUserModel);
    }

    void termsCheckChanged(final boolean isChecked) {
        mTermsAndPolicyChecked = isChecked;
    }

    void completeRegistration() {
        if (!mTermsAndPolicyChecked) {
            getView().errorAcceptTermsAndPolicy();
            return;
        }
        if (Validator.isEmpty(mUserModel.getAddressLineOne()) ||
//                Validator.isEmpty(mUserModel.getAddressLineTwo()) ||
                Validator.isEmpty(mUserModel.getBusinessName()) ||
                Validator.isEmpty(mUserModel.getCountry()) ||
                Validator.isEmpty(mUserModel.getEmail()) ||
                Validator.isEmpty(mUserModel.getPassword()) ||
                Validator.isEmpty(mUserModel.getPhone()) ||
                Validator.isEmpty(mUserModel.getTown()) ||
                Validator.isEmpty(mUserModel.getUserId())) {
            getView().incorrectFields();
            return;
        }
        if (!Validator.isCorrectUserId(mUserModel.getUserId())) {
            getView().incorrectUserId();
            return;
        }
        if (!Validator.isCorrectPassword(mUserModel.getPassword())) {
            getView().incorrectPassword();
            return;
        }
        if (!Validator.isCorrectEmail(mUserModel.getEmail())) {
            getView().incorrectEmail();
            return;
        }
        if (!Validator.isCorrectPhone(mUserModel.getPhone())) {
            getView().incorrectPhoneNumber();
            return;
        }

        if (mUserModel.getPathToImageFile() == null || mUserModel.getPathToImageFile().equals("")) {
            mUserAddressUseCase.setUserModel(mUserModel);
            unsubscribeOnDestroy(mUserAddressUseCase.execute(new UserAddressSubscriber()));
        } else {
            mImageToBase64UseCase.setImageFilePath(mUserModel.getPathToImageFile());
            unsubscribeOnDestroy(mImageToBase64UseCase.execute(new ConvertImageSubscriber()));
        }
    }

    void updateUserId(final CharSequence userId) {
        mUserModel.setUserId(userId.toString());
    }

    void updateBusinessName(final CharSequence businessName) {
        mUserModel.setBusinessName(businessName.toString());
    }

    void updatePassword(final CharSequence password) {
        mUserModel.setPassword(password.toString());
    }

    void updateFirstAddress(final CharSequence firstAddress) {
        mUserModel.setAddressLineOne(firstAddress.toString());
    }

    void updateSecondAddress(final CharSequence secondAddress) {
        mUserModel.setAddressLineTwo(secondAddress.toString());
    }

    void updateTown(final CharSequence town) {
        mUserModel.setTown(town.toString());
    }

    void updateCountry(final CharSequence country) {
        mUserModel.setCountry(country.toString());
    }

    void updatePostCode(final CharSequence postCode) {
        mUserModel.setPostCode(postCode.toString());
    }

    void updateEmail(final CharSequence email) {
        mUserModel.setEmail(email.toString());
    }

    void updatePhone(final CharSequence phone) {
        mUserModel.setPhone(phone.toString());
    }

    void updateDescription(final String businessDescription) {
        mUserModel.setDescription(businessDescription);
    }

    void addDescription() {
        getView().openBusinessDescriptionScreen(mUserModel.getDescription());
    }

    void updatePhoto(final File selectedPhoto) {
        mUserModel.setPathToImageFile(selectedPhoto.getAbsolutePath());
    }

    class ConvertImageSubscriber extends DefaultSubscriber<String> {

        @Override
        public void onCompleted() {
            mUserAddressUseCase.setUserModel(mUserModel);
            unsubscribeOnDestroy(mUserAddressUseCase.execute(new UserAddressSubscriber()));
        }

        @Override
        public void onError(final Throwable e) {
            super.onError(e);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void onNext(final String base64String) {
            mUserModel.setImage(base64String);
        }

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }
    }

    class UserAddressSubscriber extends DefaultSubscriber {

        @Override
        public void onCompleted() {
            mRegisterUseCase.setUserModel(mUserModel);
            unsubscribeOnDestroy(mRegisterUseCase.execute(new RegistrationSubscriber()));
        }

        @Override
        public void onError(final Throwable e) {
            super.onError(e);
            if (!isViewAttached()) return;
            getView().showProgress(false);
            getView().onFailedToGetAddress();
        }

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }
    }

    class RegistrationSubscriber extends DefaultInternetSubscriber<UserModel> {

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void handleUnprocessableEntity(final String[] errors) {
            final String errorDescription = mErrorHelper.getErrorString(errors);
            if (!isViewAttached()) return;
            getView().onRegistrationFailed(errorDescription);
        }

        @Override
        public void onError(final Throwable e) {
            super.onError(e);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void onNext(final UserModel userModel) {
            mAuthHelper.setUserData(userModel);
            if (!isViewAttached()) return;
            getView().onRegistrationSuccess(userModel);
        }
    }
}
