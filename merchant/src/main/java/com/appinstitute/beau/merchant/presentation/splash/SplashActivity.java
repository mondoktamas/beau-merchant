package com.appinstitute.beau.merchant.presentation.splash;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;
import com.appinstitute.beau.merchant.presentation.welcome.WelcomeActivity;

public class SplashActivity extends BaseActivity<SplashPresenter> implements SplashView {

    @Override
    public int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().startSplashTask();
    }

    @Override
    public void launchNextScreen(final boolean isLoggedIn) {
        if (isLoggedIn) {
            getNavigator().startActivity(TabActivity.getLaunchIntent(this, TabActivity.TAB_PROFILE_POSITION));
        } else {
            getNavigator().startActivity(WelcomeActivity.getLaunchIntent(this));
        }

        finish();
    }
}
