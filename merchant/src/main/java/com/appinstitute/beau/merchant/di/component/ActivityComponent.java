package com.appinstitute.beau.merchant.di.component;

import com.appinstitute.beau.core.di.annotations.PerActivity;
import com.appinstitute.beau.merchant.di.module.ActivityModule;
import com.appinstitute.beau.merchant.presentation.login.LoginActivity;
import com.appinstitute.beau.merchant.presentation.register.basic.RegistrationActivity;
import com.appinstitute.beau.merchant.presentation.register.complete.RegistrationCompleteActivity;
import com.appinstitute.beau.merchant.presentation.register.description.BusinessDescriptionActivity;
import com.appinstitute.beau.merchant.presentation.splash.SplashActivity;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;
import com.appinstitute.beau.merchant.presentation.tabs.profile.ProfileFragment;
import com.appinstitute.beau.merchant.presentation.welcome.WelcomeActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {

    void inject(final SplashActivity splashActivity);
    void inject(final WelcomeActivity splashActivity);
    void inject(final LoginActivity loginActivity);
    void inject(final RegistrationActivity registrationActivity);
    void inject(final RegistrationCompleteActivity registrationCompleteActivity);
    void inject(final BusinessDescriptionActivity businessDescriptionActivity);
    void inject(final TabActivity tabActivity);

    void inject(final ProfileFragment profileFragment);
}
