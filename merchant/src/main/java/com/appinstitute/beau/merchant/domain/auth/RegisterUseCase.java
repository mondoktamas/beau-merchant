package com.appinstitute.beau.merchant.domain.auth;

import com.appinstitute.beau.merchant.data.repository.auth.AuthRepository;
import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;

public class RegisterUseCase extends BackgroundUseCase {

    private final AuthRepository mAuthRepository;
    private UserModel mUserModel;

    @Inject
    public RegisterUseCase(final AuthRepository authRepository) {
        mAuthRepository = authRepository;
    }

    public void setUserModel(final UserModel userModel) {
        mUserModel = userModel;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAuthRepository.registerUser(mUserModel);
    }
}
