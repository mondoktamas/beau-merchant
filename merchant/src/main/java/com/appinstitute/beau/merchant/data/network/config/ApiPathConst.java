package com.appinstitute.beau.merchant.data.network.config;

public interface ApiPathConst extends BaseApiPathConst {

    String METHOD_CONTACT = "account/contact";
    String METHOD_SIGN_IN = "login";
    String METHOD_NEW_BOOKINGS_COUNT = "booking/requests/new";
    String METHOD_OPEN_OFFERS_COUNT = "offer/list/count";
    String METHOD_REGISTER = "register";
    String METHOD_LOGOUT = "logout";
}
