package com.appinstitute.beau.merchant.presentation.register.basic;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import javax.inject.Inject;

@ConfigPersistent
public class RegistrationPresenter extends BasePresenter<RegistrationView> {

    @Inject
    public RegistrationPresenter() {}

    public void registerClicked(final String businessName,
                                final String userId,
                                final String password) {
        if (Validator.isEmpty(businessName)) {
            getView().incorrectFields();
            return;
        }
        if (!Validator.isCorrectUserId(userId)) {
            getView().incorrectUserId();
            return;
        }
        if (!Validator.isCorrectPassword(password)) {
            getView().incorrectPassword();
            return;
        }
        getView().continueRegistrationFlow(new UserModel(businessName, userId, password));
    }
}
