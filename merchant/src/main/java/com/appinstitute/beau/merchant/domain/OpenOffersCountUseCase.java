package com.appinstitute.beau.merchant.domain;

import com.appinstitute.beau.merchant.data.repository.booking.OffersRepository;
import com.appinstitute.beau.core.domain.base.BackgroundUseCase;

import javax.inject.Inject;

import rx.Observable;

public class OpenOffersCountUseCase extends BackgroundUseCase {

    private OffersRepository mOffersRepository;

    @Inject
    public OpenOffersCountUseCase(final OffersRepository repository) {
        mOffersRepository = repository;
    }

    @Override
    protected Observable buildObservableTask() {
        return mOffersRepository.loadOpenOffersCount();
    }
}
