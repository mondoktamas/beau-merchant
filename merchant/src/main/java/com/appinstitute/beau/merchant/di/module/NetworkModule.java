package com.appinstitute.beau.merchant.di.module;

import com.appinstitute.beau.core.data.network.factory.RxErrorHandlingCallAdapterFactory;
import com.appinstitute.beau.core.di.modules.BaseNetworkModule;
import com.appinstitute.beau.merchant.data.network.config.DefaultNetworkConfig;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.github.aurae.retrofit2.LoganSquareConverterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Module
public class NetworkModule extends BaseNetworkModule {

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(final DefaultNetworkConfig defaultNetworkConfig) {
        final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .readTimeout(defaultNetworkConfig.getConnectionTimeout(), TimeUnit.SECONDS)
                .writeTimeout(defaultNetworkConfig.getConnectionTimeout(), TimeUnit.SECONDS);
        for (Interceptor interceptor : defaultNetworkConfig.getInterceptors()) {
            okHttpBuilder.addInterceptor(interceptor);
        }
        return okHttpBuilder.build();
    }

    @Named("BaseRetrofit")
    @Singleton
    @Provides
    Retrofit provideBaseRetrofit(final OkHttpClient httpClient, final DefaultNetworkConfig defaultNetworkConfig) {
        return new Retrofit.Builder()
                .baseUrl(defaultNetworkConfig.getBaseUrl())
                .client(httpClient)
                .addConverterFactory(LoganSquareConverterFactory.create())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    ApiService provideBaseRestService(@Named("BaseRetrofit") final Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}
