package com.appinstitute.beau.merchant.presentation.tabs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;

import com.appinstitute.beau.core.presentation.tabs.SimpleTabSelectedListener;
import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.base.view.BackStackActivity;
import com.appinstitute.beau.merchant.presentation.constants.Keys;
import com.appinstitute.beau.merchant.presentation.tabs.profile.ProfileFragment;

import butterknife.BindView;


public class TabActivity extends BackStackActivity<TabPresenter> implements TabView {

    public static final int TAB_INVALID_POSITION = -1;
    public static final int TAB_PROFILE_POSITION = 0;
    public static final int TAB_EDIT_POSITION = 1;
    public static final int TAB_CALENDAR_POSITION = 2;
    public static final int TAB_INFO_POSITION = 3;

    @BindView(R.id.bottom_navigation)
    TabLayout mBottomNavBar;

    private int mCurrentTabId;

    private Fragment mCurrentFragment;

    public static Intent getLaunchIntent(final Context context,
                                         final int currentTab) {
        final Intent intent = new Intent(context, TabActivity.class);
        intent.putExtra(Keys.Extras.EXTRA_CURRENT_TAB, currentTab);
        return intent;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_tabs;
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArguments();
        setupBottomTabBar();

        if (savedInstanceState == null) {

            mBottomNavBar.getTabAt(TAB_PROFILE_POSITION).select();
        }
    }

    private void initArguments() {
        final Intent intent;
        if ((intent = getIntent()) == null) return;
        mCurrentTabId = intent.getIntExtra(Keys.Extras.EXTRA_CURRENT_TAB, TAB_PROFILE_POSITION);
    }

    private void setupBottomTabBar() {

        mBottomNavBar.addTab(mBottomNavBar.newTab()
                .setIcon(R.drawable.tab_icon_flover), false);
        mBottomNavBar.addTab(mBottomNavBar.newTab()
                .setIcon(R.drawable.tab_icon_pencil), false);
        mBottomNavBar.addTab(mBottomNavBar.newTab()
                .setIcon(R.drawable.tab_icon_calendar), false);
        mBottomNavBar.addTab(mBottomNavBar.newTab()
                .setIcon(R.drawable.tab_icon_information), false);

        mBottomNavBar.addOnTabSelectedListener(mSimpleTabSelectedListener);
    }

    @NonNull
    private Fragment rootTabFragment(final int tabId) {
        switch (tabId) {
            case TAB_PROFILE_POSITION:
                return ProfileFragment.getNewInstance();
            case TAB_EDIT_POSITION:
                return ProfileFragment.getNewInstance();
            case TAB_CALENDAR_POSITION:
                return ProfileFragment.getNewInstance();
            case TAB_INFO_POSITION:
                return ProfileFragment.getNewInstance();
            default:
                throw new IllegalArgumentException("No such tab");
        }
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleNewIntent(intent);
    }

    private void handleNewIntent(final Intent intent) {
        int currentTabIdx = intent.getIntExtra(Keys.Extras.EXTRA_CURRENT_TAB, TAB_PROFILE_POSITION);
//        int nestedFragment = intent.getIntExtra(Arguments.EXTRA_NESTED_FRAGMENT, TAB_INVALID_POSITION);
        Bundle currentFragmentArg = intent.getBundleExtra(Keys.Extras.EXTRA_CURRENT_FRAGMENT_ARGS);
//        Bundle nestedFragmentArg = intent.getParcelableExtra(Arguments.EXTRA_NESTED_FRAGMENT_ARGS);

        final TabLayout.Tab currentTab = mBottomNavBar.getTabAt(currentTabIdx);
        if (currentTab == null) return;

        if (currentFragmentArg != null)
            currentTab.setTag(currentFragmentArg);

//        if (nestedFragment != TAB_INVALID_POSITION)
//            mSimpleTabSelectedListener.setShouldOpenTabByPosition(false);
        currentTab.select();

//        switch (nestedFragment) {
//            case NESTED_CART_FRAGMENT:
//                if (mCurrentFragment instanceof CartFragment) {
//                    CartFragment cartFragment = (CartFragment) mCurrentFragment;
//                    cartFragment.reloadPage(nestedFragmentArg);
//                } else {
//                    showFragment(CartFragment.getNewInstance(nestedFragmentArg));
//                }
//                break;
//            case NESTED_WHISHLIST_FRAGMENT:
//                showFragment(WishlistFragment.getNewInstance());
//                break;
//            case NESTED_BOOK_DETAILS_FRAGMENT:
//                final BookDetailsFragment bookDetailsFragment = BookDetailsFragment.getNewInstance(nestedFragmentArg);
//                showFragment(bookDetailsFragment);
//                break;
//            case NESTED_DISCOVER_CATEGORY_FRAGMENT:
//                final DiscoverCategoryFragment discoverCategoryFragment = DiscoverCategoryFragment.getNewInstance(nestedFragmentArg);
//                showFragment(discoverCategoryFragment);
//                break;
//            case NESTED_CATEGORIES_FRAGMENT:
//                final CategoryListFragment categoriesFragment = CategoryListFragment.getNewInstance(true);
//                showFragment(categoriesFragment);
//                break;
//        }
        mCurrentTabId = currentTabIdx;
        mSimpleTabSelectedListener.setShouldOpenTabByPosition(true);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        mCurrentTabId = savedInstanceState.getInt(Keys.Args.ARG_TAB_POSITION);
        mBottomNavBar.setScrollPosition(mCurrentTabId, 0, true);
    }

    public void showFragment(@NonNull final Fragment fragment) {
        showFragment(fragment, true);
    }

    public void showFragment(@NonNull final Fragment fragment, final boolean addToBackStack) {
        if (mCurrentFragment != null && addToBackStack) {
            pushFragmentToBackStack(mCurrentTabId, mCurrentFragment);
        }
        replaceFragment(fragment);
    }

    private void backTo(final int tabId, @NonNull final Fragment fragment) {
        if (tabId != mCurrentTabId) {
            mCurrentTabId = tabId;
            mBottomNavBar.getTabAt(mCurrentTabId).select();
        }
        replaceFragment(fragment);
        getSupportFragmentManager().executePendingTransactions();
    }

    private void replaceFragment(@NonNull final Fragment fragment) {
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.frame_layout, fragment);
        tr.commitAllowingStateLoss();
        mCurrentFragment = fragment;
    }

    @Override
    public void onBackPressed() {
        final Pair<Integer, Fragment> pair = popFragmentFromBackStack();
        if (pair != null) {
            backTo(pair.first, pair.second);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        outState.putInt(Keys.Args.ARG_TAB_POSITION, mCurrentTabId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mBottomNavBar.removeOnTabSelectedListener(mSimpleTabSelectedListener);
        super.onDestroy();
    }

    private SimpleTabSelectedListener mSimpleTabSelectedListener = new SimpleTabSelectedListener() {
        @Override
        public void onTabSelected(final TabLayout.Tab tab) {
            if (mCurrentFragment != null && tab.getPosition() != mCurrentTabId) {
                if (tab.getPosition() == TAB_PROFILE_POSITION) {
                    clearBackStack(tab.getPosition());
                }
                if (isShouldOpenTabByPosition()) {
                    pushFragmentToBackStack(mCurrentTabId, mCurrentFragment);
                    openFragmentByTabPosition(tab.getPosition(), tab.getTag() == null ? null : (Bundle) tab.getTag());
                }
            } else if (mCurrentFragment == null) {
                if (isShouldOpenTabByPosition())
                    openFragmentByTabPosition(tab.getPosition(), tab.getTag() == null ? null : (Bundle) tab.getTag());
            }
        }
    };

    private void openFragmentByTabPosition(final int tabPosition, final Bundle arg) {
        mCurrentTabId = tabPosition;
        Fragment fragment = popFragmentFromBackStack(mCurrentTabId);
        if (fragment == null) {
            fragment = rootTabFragment(mCurrentTabId);
        }
        if (arg != null) {
            fragment.setArguments(arg);
        }
        replaceFragment(fragment);
    }

}
