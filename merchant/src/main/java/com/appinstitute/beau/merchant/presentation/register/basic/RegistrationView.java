package com.appinstitute.beau.merchant.presentation.register.basic;

import com.appinstitute.beau.core.presentation.base.view.BaseView;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

public interface RegistrationView extends BaseView<RegistrationPresenter> {

    void incorrectUserId();
    void incorrectFields();
    void incorrectPassword();

    void continueRegistrationFlow(final UserModel userModel);
}
