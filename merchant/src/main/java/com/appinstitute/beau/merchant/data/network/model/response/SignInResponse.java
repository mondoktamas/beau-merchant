package com.appinstitute.beau.merchant.data.network.model.response;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class SignInResponse extends ContactResponse {

    @JsonField(name="session_id")
    private String sessionId;
    @JsonField(name="user_id")
    private String userId;
    @JsonField(name="image_id")
    private String imageId;
    @JsonField(name="image")
    private String image;
    @JsonField(name="one_hour_notification")
    private String oneHourNotification;
    @JsonField(name="one_hour_email")
    private String oneHourEmail;
    @JsonField(name="ten_minutes_notification")
    private String tenMinutesNotification;
    @JsonField(name="ten_minutes_email")
    private String tenMinutesEmail;
    @JsonField(name="new_notification")
    private String newNotification;
    @JsonField(name="new_email")
    private String newEmail;
    @JsonField(name="new_appointment_request_notification")
    private String newAppointmentRequestNotification;
    @JsonField(name="new_appointment_request_email")
    private String newAppointmentRequestEmail;
    @JsonField(name="account_name")
    private String accountName;
    @JsonField(name="account_number")
    private String accountNumber;
    @JsonField(name="sort_code")
    private String sortCode;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getOneHourNotification() {
        return oneHourNotification;
    }

    public void setOneHourNotification(String oneHourNotification) {
        this.oneHourNotification = oneHourNotification;
    }

    public String getOneHourEmail() {
        return oneHourEmail;
    }

    public void setOneHourEmail(String oneHourEmail) {
        this.oneHourEmail = oneHourEmail;
    }

    public String getTenMinutesNotification() {
        return tenMinutesNotification;
    }

    public void setTenMinutesNotification(String tenMinutesNotification) {
        this.tenMinutesNotification = tenMinutesNotification;
    }

    public String getTenMinutesEmail() {
        return tenMinutesEmail;
    }

    public void setTenMinutesEmail(String tenMinutesEmail) {
        this.tenMinutesEmail = tenMinutesEmail;
    }

    public String getNewNotification() {
        return newNotification;
    }

    public void setNewNotification(String newNotification) {
        this.newNotification = newNotification;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getNewAppointmentRequestNotification() {
        return newAppointmentRequestNotification;
    }

    public void setNewAppointmentRequestNotification(String newAppointmentRequestNotification) {
        this.newAppointmentRequestNotification = newAppointmentRequestNotification;
    }

    public String getNewAppointmentRequestEmail() {
        return newAppointmentRequestEmail;
    }

    public void setNewAppointmentRequestEmail(String newAppointmentRequestEmail) {
        this.newAppointmentRequestEmail = newAppointmentRequestEmail;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
