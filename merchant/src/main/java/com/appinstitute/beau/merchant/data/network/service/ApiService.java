package com.appinstitute.beau.merchant.data.network.service;

import com.appinstitute.beau.core.data.network.model.response.BaseResponse;
import com.appinstitute.beau.merchant.data.network.config.ApiPathConst;
import com.appinstitute.beau.merchant.data.network.model.request.ContactRequest;
import com.appinstitute.beau.merchant.data.network.model.request.SignInRequest;
import com.appinstitute.beau.merchant.data.network.model.request.UserRequest;
import com.appinstitute.beau.merchant.data.network.model.response.ContactResponse;
import com.appinstitute.beau.merchant.data.network.model.response.NewBookingsCountResponse;
import com.appinstitute.beau.merchant.data.network.model.response.OpenOffersCountResponse;
import com.appinstitute.beau.merchant.data.network.model.response.UserResponse;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface ApiService {

    @POST(ApiPathConst.METHOD_CONTACT)
    Observable<ContactResponse> setContactDetails(@Body final ContactRequest contactRequest);

    @POST(ApiPathConst.METHOD_SIGN_IN)
    Observable<UserResponse> signIn(@Body final SignInRequest signInRequest);

    @POST(ApiPathConst.METHOD_NEW_BOOKINGS_COUNT)
    Observable<NewBookingsCountResponse> loadNewBookingsCount();

    @POST(ApiPathConst.METHOD_OPEN_OFFERS_COUNT)
    Observable<OpenOffersCountResponse> loadOpenOffersCount();

    @POST(ApiPathConst.METHOD_REGISTER)
    Observable<UserResponse> register(@Body final UserRequest userRequest);

    @POST(ApiPathConst.METHOD_LOGOUT)
    Observable<BaseResponse> logout();
}
