package com.appinstitute.beau.merchant.data.network.model.request;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class UserRequest {

    @JsonField(name = "user_id") String mUserId;
    @JsonField(name = "password") String mPassword;
    @JsonField(name = "business_name") String mBusinessName;
    @JsonField(name = "address_1") String mAddressLineOne;
    @JsonField(name = "address_2") String mAddressLineTwo;
    @JsonField(name = "town") String mTown;
    @JsonField(name = "country") String mCountry;
    @JsonField(name = "post_code") String mPostCode;
    @JsonField(name = "email") String mEmail;
    @JsonField(name = "phone") String mPhone;
    @JsonField(name = "description") String mDescription;
    @JsonField(name = "image") String mImage;
    @JsonField(name = "latitude") Double mLatitude;
    @JsonField(name = "longitude") Double mLongitude;
    @JsonField(name = "push_token") String mPushToken;

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(final String userId) {
        mUserId = userId;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(final String password) {
        mPassword = password;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(final String businessName) {
        mBusinessName = businessName;
    }

    public String getAddressLineOne() {
        return mAddressLineOne;
    }

    public void setAddressLineOne(final String addressLineOne) {
        mAddressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return mAddressLineTwo;
    }

    public void setAddressLineTwo(final String addressLineTwo) {
        mAddressLineTwo = addressLineTwo;
    }

    public String getTown() {
        return mTown;
    }

    public void setTown(final String town) {
        mTown = town;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(final String country) {
        mCountry = country;
    }

    public String getPostCode() {
        return mPostCode;
    }

    public void setPostCode(final String postCode) {
        mPostCode = postCode;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(final String email) {
        mEmail = email;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(final String phone) {
        mPhone = phone;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(final String description) {
        mDescription = description;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(final String image) {
        mImage = image;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(final Double latitude) {
        mLatitude = latitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(final Double longitude) {
        mLongitude = longitude;
    }

    public String getPushToken() {
        return mPushToken;
    }

    public void setPushToken(final String pushToken) {
        mPushToken = pushToken;
    }

    @Override
    public String toString() {
        return "UserRequest{" +
                "mUserId='" + mUserId + '\'' +
                ", mPassword='" + mPassword + '\'' +
                ", mBusinessName='" + mBusinessName + '\'' +
                ", mAddressLineOne='" + mAddressLineOne + '\'' +
                ", mAddressLineTwo='" + mAddressLineTwo + '\'' +
                ", mTown='" + mTown + '\'' +
                ", mCountry='" + mCountry + '\'' +
                ", mPostCode='" + mPostCode + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mPhone='" + mPhone + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mImage='" + mImage + '\'' +
                ", mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                ", mPushToken='" + mPushToken + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRequest)) return false;

        UserRequest that = (UserRequest) o;

        if (mUserId != null ? !mUserId.equals(that.mUserId) : that.mUserId != null) return false;
        if (mPassword != null ? !mPassword.equals(that.mPassword) : that.mPassword != null)
            return false;
        if (mBusinessName != null ? !mBusinessName.equals(that.mBusinessName) : that.mBusinessName != null)
            return false;
        if (mAddressLineOne != null ? !mAddressLineOne.equals(that.mAddressLineOne) : that.mAddressLineOne != null)
            return false;
        if (mAddressLineTwo != null ? !mAddressLineTwo.equals(that.mAddressLineTwo) : that.mAddressLineTwo != null)
            return false;
        if (mTown != null ? !mTown.equals(that.mTown) : that.mTown != null) return false;
        if (mCountry != null ? !mCountry.equals(that.mCountry) : that.mCountry != null)
            return false;
        if (mPostCode != null ? !mPostCode.equals(that.mPostCode) : that.mPostCode != null)
            return false;
        if (mEmail != null ? !mEmail.equals(that.mEmail) : that.mEmail != null) return false;
        if (mPhone != null ? !mPhone.equals(that.mPhone) : that.mPhone != null) return false;
        if (mDescription != null ? !mDescription.equals(that.mDescription) : that.mDescription != null)
            return false;
        if (mImage != null ? !mImage.equals(that.mImage) : that.mImage != null) return false;
        if (mLatitude != null ? !mLatitude.equals(that.mLatitude) : that.mLatitude != null)
            return false;
        if (mLongitude != null ? !mLongitude.equals(that.mLongitude) : that.mLongitude != null)
            return false;
        return mPushToken != null ? mPushToken.equals(that.mPushToken) : that.mPushToken == null;

    }

    @Override
    public int hashCode() {
        int result = mUserId != null ? mUserId.hashCode() : 0;
        result = 31 * result + (mPassword != null ? mPassword.hashCode() : 0);
        result = 31 * result + (mBusinessName != null ? mBusinessName.hashCode() : 0);
        result = 31 * result + (mAddressLineOne != null ? mAddressLineOne.hashCode() : 0);
        result = 31 * result + (mAddressLineTwo != null ? mAddressLineTwo.hashCode() : 0);
        result = 31 * result + (mTown != null ? mTown.hashCode() : 0);
        result = 31 * result + (mCountry != null ? mCountry.hashCode() : 0);
        result = 31 * result + (mPostCode != null ? mPostCode.hashCode() : 0);
        result = 31 * result + (mEmail != null ? mEmail.hashCode() : 0);
        result = 31 * result + (mPhone != null ? mPhone.hashCode() : 0);
        result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
        result = 31 * result + (mImage != null ? mImage.hashCode() : 0);
        result = 31 * result + (mLatitude != null ? mLatitude.hashCode() : 0);
        result = 31 * result + (mLongitude != null ? mLongitude.hashCode() : 0);
        result = 31 * result + (mPushToken != null ? mPushToken.hashCode() : 0);
        return result;
    }
}
