package com.appinstitute.beau.merchant.presentation.welcome;

import android.content.Intent;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.presentation.login.LoginActivity;
import com.appinstitute.beau.merchant.presentation.register.basic.RegistrationActivity;

import butterknife.BindView;
import butterknife.OnClick;

public final class WelcomeActivity extends BaseActivity<WelcomePresenter> implements WelcomeView {

    @BindView(R.id.text_terms_and_policy) TextView mTermsAndPolicy;

    public static Intent getLaunchIntent(final BaseActivity baseActivity) {
        return new Intent(baseActivity, WelcomeActivity.class);
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTermsAndPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void launchLoginScreen() {
        getNavigator().startActivity(LoginActivity.getLaunchIntent(this));
    }

    @Override
    public void launchRegistrationScreen() {
        getNavigator().startActivity(RegistrationActivity.getLaunchIntent(this));
    }

    @OnClick(R.id.button_login)
    void onLoginClicked() {
        getPresenter().onLoginClicked();
    }

    @OnClick(R.id.button_register)
    void onRegisterClicked() {
        getPresenter().onRegistrationClicked();
    }
}
