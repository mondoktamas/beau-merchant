package com.appinstitute.beau.merchant.domain.auth;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;

import rx.Observable;

public class ImageToBase64UseCase extends BackgroundUseCase {

    private String mImageFilePath;

    @Inject
    public ImageToBase64UseCase() { }

    public void setImageFilePath(final String imageFilePath) {
        mImageFilePath = imageFilePath;
    }

    @Override
    protected Observable buildObservableTask() {
        return Observable.just(mImageFilePath)
                .map(this::getBitmapFromFile)
                .map(this::convertBitmapToBase64String);
    }

    private Bitmap getBitmapFromFile(final String imageFilePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(imageFilePath, options);
    }

    private String convertBitmapToBase64String(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
}
