package com.appinstitute.beau.merchant.di.component;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.di.module.ActivityModule;
import com.appinstitute.beau.merchant.di.module.InteractorModule;
import com.appinstitute.beau.merchant.di.module.RepositoryModule;

import dagger.Component;

/**
 * A dagger component that will live during the lifecycle of an Activity but it won't
 * be destroy during configuration changes. Check {@link BaseActivity} to see how this components
 * survives configuration changes.
 */
@ConfigPersistent
@Component(dependencies = ApplicationComponent.class, modules = {RepositoryModule.class, InteractorModule.class})
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(final ActivityModule activityModule);

}