package com.appinstitute.beau.merchant.presentation.navigation;

import android.content.Intent;

import com.appinstitute.beau.merchant.presentation.base.view.BaseFragment;

public interface Navigation {

    void openFragment(final BaseFragment fragment);

    void startActivity(final Intent launchIntent);

    void startActivity(final Intent launchIntent, final boolean finishCurrent);

    void startActivityForResult(final Intent launchIntent, final int requestCode);
}
