package com.appinstitute.beau.merchant.domain;

import com.appinstitute.beau.merchant.data.repository.booking.BookingRepository;
import com.appinstitute.beau.core.domain.base.BackgroundUseCase;

import javax.inject.Inject;

import rx.Observable;

public class NewBookingsCountUseCase extends BackgroundUseCase {

    private BookingRepository mBookingRepository;

    @Inject
    public NewBookingsCountUseCase(final BookingRepository repository) {
        mBookingRepository = repository;
    }

    @Override
    protected Observable buildObservableTask() {
        return mBookingRepository.loadNewBookingsCount();
    }
}
