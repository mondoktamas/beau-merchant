package com.appinstitute.beau.merchant.data.network.model.response;

import com.appinstitute.beau.core.data.network.model.response.BaseResponse;
import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class OpenOffersCountResponse extends BaseResponse {
    @JsonField(name = "offers_count")
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
