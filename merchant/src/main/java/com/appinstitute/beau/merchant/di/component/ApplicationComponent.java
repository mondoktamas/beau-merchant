package com.appinstitute.beau.merchant.di.component;

import android.app.Application;
import android.content.Context;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ApplicationContext;
import com.appinstitute.beau.core.presentation.BeauApplication;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.merchant.di.module.ApplicationModule;
import com.appinstitute.beau.merchant.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(BeauApplication application);

    @ApplicationContext
    Context context();
    Application application();

    ApiService apiService();

    AuthHelper authHelper();

    ErrorHelper errorHelper();
}
