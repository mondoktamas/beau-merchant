package com.appinstitute.beau.merchant.presentation.register.description;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface BusinessDescriptionView extends BaseView<BusinessDescriptionPresenter> {

    void populateDescription(final String description);
    void saveDescriptionChanges(final String description);
}
