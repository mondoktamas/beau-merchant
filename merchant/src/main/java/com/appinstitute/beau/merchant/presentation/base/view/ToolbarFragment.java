package com.appinstitute.beau.merchant.presentation.base.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.appinstitute.beau.core.presentation.base.presenter.Presenter;

public abstract class ToolbarFragment<P extends Presenter> extends BaseFragment<P> {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initToolbar();
    }

    public abstract void initToolbar();

}
