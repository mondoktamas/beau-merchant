package com.appinstitute.beau.merchant.presentation;

import android.content.Context;

import com.appinstitute.beau.core.presentation.BeauApplication;
import com.appinstitute.beau.merchant.di.component.ApplicationComponent;
import com.appinstitute.beau.merchant.di.component.DaggerApplicationComponent;
import com.appinstitute.beau.merchant.di.module.ApplicationModule;

public class BeauMerchantApplication extends BeauApplication {

    protected ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        getComponent().inject(this);
    }

    public static BeauMerchantApplication get(final Context context) {
        return (BeauMerchantApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }


}