package com.appinstitute.beau.merchant.domain.account;

import com.appinstitute.beau.merchant.data.repository.account.AccountRepository;
import com.appinstitute.beau.core.domain.base.BackgroundUseCase;

import javax.inject.Inject;

import rx.Observable;

public class SetContactDetails extends BackgroundUseCase {

    private final AccountRepository mAccountRepository;

    @Inject
    public SetContactDetails(final AccountRepository accountRepository) {
        mAccountRepository = accountRepository;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAccountRepository.setContactDetails(null);
    }
}
