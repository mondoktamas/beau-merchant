package com.appinstitute.beau.merchant.domain.auth;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.appinstitute.beau.core.di.annotations.ApplicationContext;
import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.merchant.presentation.model.UserModel;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import rx.Observable;

public class UserAddressUseCase extends BackgroundUseCase {

    private final Context mContext;
    private UserModel mUserModel;

    private final String LOCATION_NAME = "%s %s, %s, %s";

    @Inject
    public UserAddressUseCase(final @ApplicationContext Context context) {
        mContext = context;
    }

    public void setUserModel(final UserModel userModel) {
        mUserModel = userModel;
    }

    @Override
    protected Observable buildObservableTask() {
        return Observable.create(subscriber -> {
            final Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            final String locationName = String.format(LOCATION_NAME, mUserModel.getPostCode(),
                    mUserModel.getAddressLineOne(), mUserModel.getTown(), mUserModel.getCountry());
            try {
                final List<Address> addresses = geocoder.getFromLocationName(locationName, 5);
                if (addresses != null && addresses.size() > 0) {

                    mUserModel.setLatitude(addresses.get(0).getLatitude());
                    mUserModel.setLongitude(addresses.get(0).getLongitude());
                    subscriber.onCompleted();
                } else {
                    subscriber.onError(new RuntimeException("Cannot detect location"));
                }
            } catch (IOException e) {
                subscriber.onError(e);
            }
        });
    }
}
