package com.appinstitute.beau.merchant.presentation.login;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;

import butterknife.BindView;
import butterknife.OnClick;

public final class LoginActivity extends BaseActivity<LoginPresenter> implements LoginView {

    @BindView(R.id.layout_form) LinearLayout mFormLayout;
    @BindView(R.id.layout_user_id) TextInputLayout mUserIdContainer;
    @BindView(R.id.edit_user_id) EditText mUserIdEdit;
    @BindView(R.id.layout_password) TextInputLayout mPasswordContainer;
    @BindView(R.id.edit_password) EditText mPasswordEdit;
    @BindView(R.id.text_register) TextView mRegisterAccountLink;

    Animation mSwingAnimation;

    public static Intent getLaunchIntent(final BaseActivity baseActivity) {
        return new Intent(baseActivity, LoginActivity.class);
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSwingAnimation = AnimationUtils.loadAnimation(this, R.anim.swing);
    }

    @OnClick(R.id.button_login)
    public void onLoginClicked(View view) {
        getPresenter().onLoginClicked(mUserIdEdit.getText().toString().trim(),
                mPasswordEdit.getText().toString().trim());
    }

    @Override
    public void swingForm() {
        mFormLayout.startAnimation(mSwingAnimation);
    }

    @Override
    public void clearValidationErrors() {
        mUserIdContainer.setError(null);
        mPasswordContainer.setError(null);
    }

    @Override
    public void userIdIsEmpty() {
        mUserIdContainer.setError(getText(R.string.login_in_error_empty_user_id));
    }

    @Override
    public void passwordIsEmpty() {
        mPasswordContainer.setError(getText(R.string.login_in_error_empty_password));
    }

    @Override
    public void signInSuccess() {
        Toast.makeText(this, R.string.login_sign_in_success, Toast.LENGTH_SHORT).show();
        getNavigator().startActivity(TabActivity.getLaunchIntent(this, TabActivity.TAB_PROFILE_POSITION));
        finish();
    }

    @Override
    public void showErrorMessages(String errorString) {
        new AlertDialog.Builder(this, R.style.App_Theme_AlertDialogInfo)
                .setTitle(R.string.register_validation_alert_title)
                .setMessage(errorString)
                .setPositiveButton(R.string.register_validation_alert_button,
                        (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRegisterAccountLink.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
