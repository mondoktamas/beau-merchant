package com.appinstitute.beau.merchant.data.repository.account;

import com.appinstitute.beau.merchant.data.network.model.request.ContactRequest;
import com.appinstitute.beau.merchant.data.network.model.response.ContactResponse;
import com.appinstitute.beau.merchant.data.network.model.response.SignInResponse;

import rx.Observable;

public interface AccountRepository {
    Observable<ContactResponse> setContactDetails(final ContactRequest contactRequest);

}
