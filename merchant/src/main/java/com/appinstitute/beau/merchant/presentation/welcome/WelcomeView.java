package com.appinstitute.beau.merchant.presentation.welcome;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface WelcomeView extends BaseView<WelcomePresenter> {
    void launchLoginScreen();
    void launchRegistrationScreen();
}
