package com.appinstitute.beau.merchant.data.repository.booking;

import com.appinstitute.beau.merchant.data.network.model.response.NewBookingsCountResponse;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public class BookingRepositoryImpl implements BookingRepository {

    private final ApiService mApiService;

    @Inject
    public BookingRepositoryImpl(final ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Observable<NewBookingsCountResponse> loadNewBookingsCount() {
        return mApiService.loadNewBookingsCount();
    }
}
