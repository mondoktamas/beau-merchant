package com.appinstitute.beau.merchant.presentation.register.description;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.presentation.constants.Keys;

import butterknife.BindView;
import butterknife.OnClick;

public class BusinessDescriptionActivity extends BaseActivity<BusinessDescriptionPresenter>
        implements BusinessDescriptionView {

    public static final int REQUEST_CODE = 213;

    @BindView(R.id.edit_business_description) EditText mBusinessDescription;

    public static Intent getLaunchIntent(final BaseActivity baseActivity, final String description) {
        final Intent launchIntent = new Intent(baseActivity, BusinessDescriptionActivity.class);
        launchIntent.putExtra(Keys.Extras.DESCRIPTION, description);
        return launchIntent;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_business_description;
    }

    @Override
    protected void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        final String description = getIntent().getStringExtra(Keys.Extras.DESCRIPTION);
        getPresenter().setDescription(description);
    }

    @Override
    public void populateDescription(final String description) {
        mBusinessDescription.setText(description);
    }

    @Override
    public void saveDescriptionChanges(final String description) {
        final Intent resultIntent = new Intent();
        resultIntent.putExtra(Keys.Extras.DESCRIPTION, description);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @OnClick(R.id.button_save)
    void onSaveClicked() {
        getPresenter().updateDescription(mBusinessDescription.getText().toString());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
