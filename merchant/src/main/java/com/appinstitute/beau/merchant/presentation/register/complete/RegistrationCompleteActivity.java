package com.appinstitute.beau.merchant.presentation.register.complete;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CheckBox;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.common.permission.PermissionChecker;
import com.appinstitute.beau.merchant.common.permission.SecurityException;
import com.appinstitute.beau.merchant.presentation.base.view.BaseActivity;
import com.appinstitute.beau.merchant.presentation.constants.Keys;
import com.appinstitute.beau.merchant.presentation.model.UserModel;
import com.appinstitute.beau.merchant.presentation.register.complete.adapter.CountryAdapter;
import com.appinstitute.beau.merchant.presentation.register.complete.adapter.CountryItem;
import com.appinstitute.beau.merchant.presentation.register.description.BusinessDescriptionActivity;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;

import java.io.File;
import java.util.Collections;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class RegistrationCompleteActivity extends BaseActivity<RegistrationCompletePresenter>
        implements RegistrationCompleteView {

    static final String ARG_USER_MODEL = "arg_user_model";
    static final int PERMISSION_REQUEST_CODE = 321;

    @Inject
    PermissionChecker mPermissionChecker;

    @BindView(R.id.check_terms_and_policy) CheckBox mTermsAndPrivacyCheckbox;
    @BindView(R.id.edit_user_id) TextInputEditText mUserId;
    @BindView(R.id.edit_password) TextInputEditText mPassword;
    @BindView(R.id.edit_business_name) TextInputEditText mBusinessName;
    @BindView(R.id.edit_country) TextInputEditText mCountry;
    @BindView(R.id.edit_post_code) TextInputEditText mPostCode;
    @BindView(R.id.bottom_sheet) View mBottomSheetView;

    BottomSheetBehavior mBehavior;
    BottomSheetDialog mBottomSheetDialog;

    public static Intent getLaunchIntent(final BaseActivity baseActivity, final UserModel userModel) {
        final Intent launchIntent = new Intent(baseActivity, RegistrationCompleteActivity.class);
        launchIntent.putExtra(Keys.Extras.USER, userModel);
        return launchIntent;
    }

    @VisibleForTesting
    void setPermissionChecker(final PermissionChecker permissionChecker) {
        mPermissionChecker = permissionChecker;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_complete_registration;
    }

    @Override
    protected void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initBottomSheet();
        final UserModel userModel = getIntent().getParcelableExtra(Keys.Extras.USER);
        getPresenter().setUserModel(userModel);
    }

    private void initBottomSheet() {
        mBehavior = BottomSheetBehavior.from(mBottomSheetView);
    }

    void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_country, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CountryAdapter(Collections.singletonList(new CountryItem("United Kingdom")),
                this::onCountrySelected));
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    void onCountrySelected(final CountryItem countryItem) {
        mCountry.setText(countryItem.getTitle());
        mPostCode.requestFocus();
        if (mBottomSheetDialog != null) {
            mBottomSheetDialog.dismiss();
        }
    }

    @Override
    public void populateUserInfo(final UserModel userModel) {
        mBusinessName.setText(userModel.getBusinessName());
        mPassword.setText(userModel.getPassword());
        mUserId.setText(userModel.getUserId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTermsAndPrivacyCheckbox.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OnTextChanged(R.id.edit_user_id)
    void onUserIdChanged(final CharSequence textChanged) {
        getPresenter().updateUserId(textChanged);
    }

    @OnTextChanged(R.id.edit_business_name)
    void onBusinessNameChanged(final CharSequence textChanged) {
        getPresenter().updateBusinessName(textChanged);
    }

    @OnTextChanged(R.id.edit_password)
    void onPasswordChanged(final CharSequence textChanged) {
        getPresenter().updatePassword(textChanged);
    }

    @OnTextChanged(R.id.edit_first_address_line)
    void onFirstAddressChanged(final CharSequence textChanged) {
        getPresenter().updateFirstAddress(textChanged);
    }

    @OnTextChanged(R.id.edit_second_address_line)
    void onSecondAddressChanged(final CharSequence textChanged) {
        getPresenter().updateSecondAddress(textChanged);
    }

    @OnTextChanged(R.id.edit_town)
    void onTownChanged(final CharSequence textChanged) {
        getPresenter().updateTown(textChanged);
    }

    @OnTextChanged(R.id.edit_country)
    void onCountryChanged(final CharSequence textChanged) {
        getPresenter().updateCountry(textChanged);
    }

    @OnTextChanged(R.id.edit_post_code)
    void onPostCodeChanged(final CharSequence textChanged) {
        getPresenter().updatePostCode(textChanged);
    }

    @OnTextChanged(R.id.edit_email)
    void onEmailChanged(final CharSequence textChanged) {
        getPresenter().updateEmail(textChanged);
    }

    @OnTextChanged(R.id.edit_phone)
    void onPhoneChanged(final CharSequence textChanged) {
        getPresenter().updatePhone(textChanged);
    }

    @OnCheckedChanged(R.id.check_terms_and_policy)
    void onTermsAndPolicyCheckChanged(final boolean isChecked) {
        getPresenter().termsCheckChanged(isChecked);
    }

    @OnClick(R.id.button_complete_registration)
    void onCompleteRegistrationClicked() {
        getPresenter().completeRegistration();
    }

    @OnClick(R.id.button_add_description)
    void onAddDescriptionClicked() { getPresenter().addDescription(); }

    @OnClick(R.id.edit_country)
    void onCountryClicked() {
        showBottomSheetDialog();
    }

    @OnClick(R.id.button_add_photo)
    void onAddPhotoClicked() {
        try {
            mPermissionChecker.verifyPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            EasyImage.openChooserWithGallery(this, getString(R.string.register_complete_add_photo_title), 0);
        } catch (SecurityException e) {
            mPermissionChecker.requestPermissions(PERMISSION_REQUEST_CODE, e.getRequiredPermissions());
        }
    }

    @Override
    public void incorrectUserId() {
        showAlert(R.string.register_invalid_user_id);
    }

    @Override
    public void incorrectFields() {
        showAlert(R.string.register_validation_alert_message);
    }

    @Override
    public void incorrectPassword() {
        showAlert(R.string.register_invalid_password);
    }

    @Override
    public void incorrectEmail() {
        showAlert(R.string.register_complete_invalid_email);
    }

    @Override
    public void incorrectPhoneNumber() {
        showAlert(R.string.register_complete_invalid_phone);
    }

    @Override
    public void onRegistrationFailed(final String error) {
        showAlert(error);
    }

    @Override
    public void onRegistrationSuccess(final UserModel userModel) {
        getNavigator().startActivity(TabActivity.getLaunchIntent(this, TabActivity.TAB_PROFILE_POSITION));
        finishAffinity();
    }

    @Override
    public void onFailedToGetAddress() {
        showAlert(R.string.register_complete_failed_to_get_address);
    }

    @Override
    public void openBusinessDescriptionScreen(final String description) {
        getNavigator().startActivityForResult(BusinessDescriptionActivity.
                getLaunchIntent(this, description), BusinessDescriptionActivity.REQUEST_CODE);
    }

    @Override
    public void errorAcceptTermsAndPolicy() {
        showAlert(R.string.register_complete_error_accept_terms);
    }

    private void showAlert(final @StringRes int message) {
        showAlert(getString(message));
    }

    private void showAlert(final String message) {
        new AlertDialog.Builder(this, R.style.App_Theme_AlertDialogInfo)
                .setTitle(R.string.register_validation_alert_title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(R.string.register_validation_alert_button, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(final int requestCode,
                                    final int resultCode,
                                    final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BusinessDescriptionActivity.REQUEST_CODE && resultCode == RESULT_OK) {
            getPresenter().updateDescription(data.getStringExtra(Keys.Extras.DESCRIPTION));
        } else {
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePicked(final File imageFile, final EasyImage.ImageSource source, final int type) {
                    getPresenter().updatePhoto(imageFile);
                }
            });
        }
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        final UserModel userModel = savedInstanceState.getParcelable(ARG_USER_MODEL);
        getPresenter().setUserModel(userModel);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (mPermissionChecker.permissionsGranted(permissions, grantResults))
                    onAddPhotoClicked();
                else
                    showAlert(R.string.register_complete_permission_not_granted);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_USER_MODEL, getIntent().getParcelableExtra(Keys.Extras.USER));
    }
}
