package com.appinstitute.beau.merchant.data.repository.auth;

import com.appinstitute.beau.merchant.presentation.model.UserModel;

import rx.Observable;

public interface AuthRepository {
    Observable<UserModel> signIn(final String mUserId, final String mPassword);
    Observable<UserModel> registerUser(final UserModel userRequest);
    Observable<Void> logout();
}
