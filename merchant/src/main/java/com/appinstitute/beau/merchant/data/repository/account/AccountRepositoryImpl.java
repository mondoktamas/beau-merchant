package com.appinstitute.beau.merchant.data.repository.account;

import com.appinstitute.beau.merchant.data.network.model.request.ContactRequest;
import com.appinstitute.beau.merchant.data.network.model.response.ContactResponse;
import com.appinstitute.beau.merchant.data.network.service.ApiService;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public class AccountRepositoryImpl implements AccountRepository {

    private final ApiService mApiService;

    @Inject
    public AccountRepositoryImpl(final ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Observable<ContactResponse> setContactDetails(final ContactRequest contactRequest) {
        return mApiService.setContactDetails(contactRequest);
    }
}
