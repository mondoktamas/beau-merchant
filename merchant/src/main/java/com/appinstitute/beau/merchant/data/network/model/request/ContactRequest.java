package com.appinstitute.beau.merchant.data.network.model.request;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class ContactRequest {
    @JsonField(name="business_name")
    String mBusinessName;
    @JsonField(name="address_1")
    String mAddress1;
    @JsonField(name="address_2")
    String mAddress2;
    @JsonField(name="town")
    String mTown;
    @JsonField(name="country")
    String mCountry;
    @JsonField(name="post_code")
    String mPostCode;
    @JsonField(name="email")
    String mEmail;
    @JsonField(name="phone")
    String mPhone;
    @JsonField(name="image")
    String mImage;
    @JsonField(name="latitude")
    String mLatitude;
    @JsonField(name="longitude")
    String mLongitude;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactRequest)) return false;

        ContactRequest that = (ContactRequest) o;

        if (mBusinessName != null ? !mBusinessName.equals(that.mBusinessName) : that.mBusinessName != null)
            return false;
        if (mAddress1 != null ? !mAddress1.equals(that.mAddress1) : that.mAddress1 != null)
            return false;
        if (mAddress2 != null ? !mAddress2.equals(that.mAddress2) : that.mAddress2 != null)
            return false;
        if (mTown != null ? !mTown.equals(that.mTown) : that.mTown != null) return false;
        if (mCountry != null ? !mCountry.equals(that.mCountry) : that.mCountry != null)
            return false;
        if (mPostCode != null ? !mPostCode.equals(that.mPostCode) : that.mPostCode != null)
            return false;
        if (mEmail != null ? !mEmail.equals(that.mEmail) : that.mEmail != null) return false;
        if (mPhone != null ? !mPhone.equals(that.mPhone) : that.mPhone != null) return false;
        if (mImage != null ? !mImage.equals(that.mImage) : that.mImage != null) return false;
        if (mLatitude != null ? !mLatitude.equals(that.mLatitude) : that.mLatitude != null)
            return false;
        return mLongitude != null ? mLongitude.equals(that.mLongitude) : that.mLongitude == null;

    }

    @Override
    public int hashCode() {
        int result = mBusinessName != null ? mBusinessName.hashCode() : 0;
        result = 31 * result + (mAddress1 != null ? mAddress1.hashCode() : 0);
        result = 31 * result + (mAddress2 != null ? mAddress2.hashCode() : 0);
        result = 31 * result + (mTown != null ? mTown.hashCode() : 0);
        result = 31 * result + (mCountry != null ? mCountry.hashCode() : 0);
        result = 31 * result + (mPostCode != null ? mPostCode.hashCode() : 0);
        result = 31 * result + (mEmail != null ? mEmail.hashCode() : 0);
        result = 31 * result + (mPhone != null ? mPhone.hashCode() : 0);
        result = 31 * result + (mImage != null ? mImage.hashCode() : 0);
        result = 31 * result + (mLatitude != null ? mLatitude.hashCode() : 0);
        result = 31 * result + (mLongitude != null ? mLongitude.hashCode() : 0);
        return result;
    }
}
