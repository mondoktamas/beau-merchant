package com.appinstitute.beau.merchant.presentation.tabs.profile;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appinstitute.beau.merchant.R;
import com.appinstitute.beau.merchant.data.network.config.BaseApiPathConst;
import com.appinstitute.beau.merchant.presentation.base.view.ToolbarFragment;
import com.appinstitute.beau.merchant.presentation.tabs.TabActivity;
import com.appinstitute.beau.merchant.presentation.welcome.WelcomeActivity;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileFragment extends ToolbarFragment<ProfilePresenter> implements ProfileView {

    @BindView(R.id.toolbar) Toolbar mToolbar;

    @BindView(R.id.background_image) ImageView mBackgroundImage;
    @BindView(R.id.text_title) TextView mTitleText;

    @BindView(R.id.text_current_date) TextView mCurrentDateText;
    @BindView(R.id.text_new_bookings_count) TextView mNewBookingsCountText;
    @BindView(R.id.text_open_offers_count) TextView mOpenOffersCountText;

    public static ProfileFragment getNewInstance(){
        return new ProfileFragment();
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public void initToolbar() {
        ((TabActivity)getActivity()).setSupportActionBar(mToolbar);
        ActionBar supportActionBar = ((TabActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(R.string.profile_title);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_exit);
        }
    }

    @Override
    protected void setupView(View view) {
        getPresenter().loadBusinessName();
        getPresenter().loadImageUrl();
        getPresenter().loadCurrentDate();
        getPresenter().loadNewBookingsCount();
        getPresenter().loadOpenOffersCount();

    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showVerificationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setBusinessName(final String businessName) {
        mTitleText.setText(businessName);
    }

    @Override
    public void setNewBookingsCount(int count) {
        mNewBookingsCountText.setText(getString(R.string.profile_new_bookings_count, count));
    }

    @Override
    public void setCurrentDate(String dateString) {
        mCurrentDateText.setText(dateString);
    }

    @Override
    public void setOpenOffersCount(int count) {
        mOpenOffersCountText.setText(getString(R.string.profile_open_offers_count, count));
    }

    @Override
    public void setImage(String imageUrl) {
        Glide.with(this)
                .load(BaseApiPathConst.API_BASE_URL + imageUrl)
                .crossFade()
                .into(mBackgroundImage);
    }

    @Override
    public void onLogoutSuccess() {
        getNavigator().startActivity(WelcomeActivity.getLaunchIntent(getBaseActivity()), true);
    }

    private void showVerificationDialog() {
        new AlertDialog.Builder(getActivity(), R.style.App_Theme_AlertDialogInfo)
                .setTitle(R.string.register_validation_alert_title)
                .setMessage(R.string.logout_verify_message)
                .setPositiveButton(R.string.logout_yes,
                        (dialog, which) -> getPresenter().logout())
                .setNegativeButton(R.string.logout_no, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    public void onLogoutFailed(final String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_view_booked_appointments)
    public void onViewBookedAppointmentsClicked() {
        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_manage_available_offers)
    public void onManageAvailableOffersClicked() {
        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_price_list)
    public void onPriceListClicked() {
        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_company_details)
    public void onCompanyDetailsClicked() {
        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_staff_list)
    public void onStaffListClicked() {
        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_my_community_feed)
    public void onMyCommunityFeedClicked() {
        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_account_settings)
    public void onAccountSettingsClicked() {
        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
    }
}
