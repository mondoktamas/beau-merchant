package com.appinstitute.beau.customer.data.network.config;

import com.appinstitute.beau.core.data.network.config.NetworkConfiguration;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;

/**
 * Created by tomashmondok on 1/12/17.
 */

@Singleton
public final class DefaultNetworkConfig implements NetworkConfiguration {

    private final String HEADER_SESSION_TOKEN = "Session-Token";
    private final int CONNECTION_TIMEOUT = 40; //sec

    @Inject
    public DefaultNetworkConfig(/* Pass the database object here to get the access token of the user*/) {

    }

    @Override
    public int getConnectionTimeout() {
        return CONNECTION_TIMEOUT;
    }

    private String getAuthTokenHeaderKey() {
        return HEADER_SESSION_TOKEN;
    }

    private String getAuthToken() {
        return "";
    }

    @Override
    public String getBaseUrl() {
        return ApiPathConst.API_BASE_URL + ApiPathConst.API_PATH;
    }

    @Override
    public List<Interceptor> getInterceptors() {
        return Collections.singletonList(getAuthRequestInterceptor());
    }

    private Interceptor getAuthRequestInterceptor() {
        return chain -> {
            Request request = chain.request();

            final String authHeaderKey = getAuthTokenHeaderKey();
            final String authHeaderToken = getAuthToken();

            if (authHeaderKey != null && authHeaderToken != null) {
                request = request.newBuilder()
                        .addHeader(authHeaderKey, authHeaderToken)
                        .build();
            }
            return chain.proceed(request);
        };
    }
}
