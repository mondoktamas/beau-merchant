package com.appinstitute.beau.customer.data.network.config;

public interface BaseApiPathConst {

    String API_BASE_URL = "http://beautybookings.appus.work/";
    String API_PATH = "customer/";

}