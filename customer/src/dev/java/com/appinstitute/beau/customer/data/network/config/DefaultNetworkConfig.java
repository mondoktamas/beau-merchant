package com.appinstitute.beau.customer.data.network.config;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.data.network.config.ErrorInterceptor;
import com.appinstitute.beau.core.data.network.config.NetworkConfiguration;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;

@Singleton
public final class DefaultNetworkConfig implements NetworkConfiguration {

    private final String HEADER_SESSION_TOKEN = "sessionId";
    private final int CONNECTION_TIMEOUT = 40; //sec

    AuthHelper mAuthHelper;

    @Inject
    public DefaultNetworkConfig(final AuthHelper authHelper) {
        mAuthHelper = authHelper;
    }

    @Override
    public int getConnectionTimeout() {
        return CONNECTION_TIMEOUT;
    }

    private String getAuthTokenHeaderKey() {
        return HEADER_SESSION_TOKEN;
    }

    private String getAuthToken() {
        return mAuthHelper.getToken();
    }

    @Override
    public String getBaseUrl() {
        return BaseApiPathConst.API_BASE_URL + BaseApiPathConst.API_PATH;
    }

    @Override
    public List<Interceptor> getInterceptors() {
        return Arrays.asList(getAuthRequestInterceptor(), new ErrorInterceptor(), getLoggingInterceptor());
    }

    private Interceptor getLoggingInterceptor() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    private Interceptor getAuthRequestInterceptor() {
        return chain -> {
            Request request = chain.request();

            final String authHeaderKey = getAuthTokenHeaderKey();
            final String authHeaderToken = getAuthToken();

            if (authHeaderKey != null && authHeaderToken != null) {
                request = request.newBuilder()
                        .addHeader(authHeaderKey, authHeaderToken)
                        .build();
            }
            return chain.proceed(request);
        };
    }
}
