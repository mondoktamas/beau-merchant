package com.appinstitute.beau.customer.di.component;

import com.appinstitute.beau.core.di.annotations.PerActivity;
import com.appinstitute.beau.customer.di.module.ActivityModule;
import com.appinstitute.beau.customer.presentation.ui.account.MyAccountFragment;
import com.appinstitute.beau.customer.presentation.ui.auth.forgot.ForgotActivity;
import com.appinstitute.beau.customer.presentation.ui.auth.login.LoginActivity;
import com.appinstitute.beau.customer.presentation.ui.auth.register.basic.RegistrationActivity;
import com.appinstitute.beau.customer.presentation.ui.auth.register.complete.RegistrationCompleteActivity;
import com.appinstitute.beau.customer.presentation.ui.main.MainActivity;
import com.appinstitute.beau.customer.presentation.ui.main.profile.ProfileFragment;
import com.appinstitute.beau.customer.presentation.ui.main.search.SearchFragment;
import com.appinstitute.beau.customer.presentation.ui.splash.SplashActivity;
import com.appinstitute.beau.customer.presentation.ui.welcome.WelcomeActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {

    void inject(final SplashActivity splashActivity);
    void inject(final WelcomeActivity splashActivity);
    void inject(final LoginActivity loginActivity);
    void inject(final ForgotActivity forgotActivity);
    void inject(final RegistrationActivity registrationActivity);
    void inject(final RegistrationCompleteActivity registrationCompleteActivity);
    void inject(final MainActivity mainActivity);

    void inject(final ProfileFragment profileFragment);
    void inject(final MyAccountFragment myAccountFragment);
    void inject(final SearchFragment searchFragment);
}
