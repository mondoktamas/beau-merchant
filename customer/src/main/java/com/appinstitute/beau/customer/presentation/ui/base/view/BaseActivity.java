package com.appinstitute.beau.customer.presentation.ui.base.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;

import com.appinstitute.beau.core.presentation.base.presenter.Presenter;
import com.appinstitute.beau.core.presentation.base.view.BaseView;
import com.appinstitute.beau.customer.di.component.ActivityComponent;
import com.appinstitute.beau.customer.di.component.ConfigPersistentComponent;
import com.appinstitute.beau.customer.di.component.DaggerConfigPersistentComponent;
import com.appinstitute.beau.customer.di.module.ActivityModule;
import com.appinstitute.beau.customer.presentation.BeauCustomerApplication;
import com.appinstitute.beau.customer.presentation.navigation.Navigation;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity<P extends Presenter> extends AppCompatActivity implements BaseView<P> {

    @Inject
    P mPresenter;

    @Inject
    Navigation mNavigation;

    protected ProgressDialog mProgressDialog;

    private ActivityComponent mActivityComponent;

    protected Unbinder mUnbinder;

    @VisibleForTesting
    public void setPresenter(final P presenter) {
        mPresenter = presenter;
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        mUnbinder = ButterKnife.bind(this);

        final ConfigPersistentComponent configPersistentComponent = DaggerConfigPersistentComponent.builder()
                .applicationComponent(BeauCustomerApplication.get(this).getComponent())
                .build();
        mActivityComponent = configPersistentComponent.activityComponent(new ActivityModule(this));

        injectToComponent();
        mPresenter.attachView(this);
    }

    protected abstract void injectToComponent();

    /**
     * This method provides the layout resource id where the fragment will be attached
     * @return the id of layout
     */
    public @IdRes int getFragmentContainer() {
        return 0;
    }

    @Override
    public void showMessage(final String message) {

    }

    @Override
    public void showProgress(final boolean visible) {
        showProgressInternal(visible);
    }

    private void showProgressInternal(final boolean visible) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(com.example.core.R.string.alert_loading));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(true);
        }

        if (visible) {
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public ActivityComponent getComponent() {
        return mActivityComponent;
    }

    @Override
    public P getPresenter() {
        return mPresenter;
    }

    public Navigation getNavigator() {
        return mNavigation;
    }

    @Override
    protected void onDestroy() {
        mUnbinder.unbind();
        mPresenter.detachView();
        super.onDestroy();
    }
}
