package com.appinstitute.beau.customer.presentation.ui.main;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface MainView extends BaseView<MainPresenter> {
}
