package com.appinstitute.beau.customer.data.network.service;

import com.appinstitute.beau.core.data.network.model.response.BaseResponse;
import com.appinstitute.beau.customer.data.network.config.ApiPathConst;
import com.appinstitute.beau.customer.data.network.model.request.ForgotPasswordRequest;
import com.appinstitute.beau.customer.data.network.model.request.SignInRequest;
import com.appinstitute.beau.customer.data.network.model.request.SignUpRequest;
import com.appinstitute.beau.customer.data.network.model.response.UserResponse;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface AuthApiService {
    @POST(ApiPathConst.METHOD_SIGN_IN)
    Observable<UserResponse> signIn(@Body final SignInRequest signInRequest);

    @POST(ApiPathConst.METHOD_SIGN_UP)
    Observable<UserResponse> register(@Body SignUpRequest signUpRequest);

    @POST(ApiPathConst.METHOD_FORGOT_PASSWORD)
    Observable<Void> forgotPassword(@Body ForgotPasswordRequest forgotPasswordRequest);

    @POST(ApiPathConst.METHOD_LOGOUT)
    Observable<BaseResponse> logout();
}
