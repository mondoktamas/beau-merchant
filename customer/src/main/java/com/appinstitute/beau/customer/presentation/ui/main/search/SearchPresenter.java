package com.appinstitute.beau.customer.presentation.ui.main.search;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;

import javax.inject.Inject;

@ConfigPersistent
public class SearchPresenter extends BasePresenter<SearchView> {

    private AuthHelper mAuthHelper;
    @Inject
    public SearchPresenter(final AuthHelper authHelper) {
        mAuthHelper = authHelper;
    }


}
