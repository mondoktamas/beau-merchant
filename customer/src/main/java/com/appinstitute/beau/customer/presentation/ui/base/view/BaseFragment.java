package com.appinstitute.beau.customer.presentation.ui.base.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.appinstitute.beau.core.presentation.base.presenter.Presenter;
import com.appinstitute.beau.core.presentation.base.view.BaseView;
import com.appinstitute.beau.customer.di.component.ActivityComponent;
import com.appinstitute.beau.customer.presentation.navigation.Navigation;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment<P extends Presenter> extends Fragment implements BaseView<P> {

    @Inject
    P mPresenter;

    private Unbinder mUnbinder;

    //we delegate the job to the base activity
    @Override
    public void showMessage(final String message) {
        if (!isActivityAlive()) return;
        ((BaseView) getActivity()).showMessage(message);
    }


    //we delegate the job to the base activity
    @Override
    public void showProgress(final boolean visible) {
        if (!isActivityAlive()) return;
        ((BaseView) getActivity()).showProgress(visible);
    }

    /**
     * Check if the activity is not destroyed
     * @return
     */
    private boolean isActivityAlive() {
        return getActivity() != null && !getActivity().isFinishing();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        injectToComponent();
        mPresenter.attachView(this);
    }

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        android.view.View view =  inflater.inflate(getLayoutResource(), container, false);
        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        setupView(savedInstanceState);
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public P getPresenter() {
        return mPresenter;
    }

    public Navigation getNavigator() {
        return ((BaseActivity) getActivity()).getNavigator();
    }

    public ActivityComponent getComponent() {
        return ((BaseActivity) getActivity()).getComponent();
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected abstract void injectToComponent();
    protected void setupView(final Bundle savedInstanceState) { }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        mPresenter.detachView();
    }
}
