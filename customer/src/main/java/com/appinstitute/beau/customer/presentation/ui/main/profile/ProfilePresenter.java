package com.appinstitute.beau.customer.presentation.ui.main.profile;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;

import javax.inject.Inject;

@ConfigPersistent
public class ProfilePresenter extends BasePresenter<ProfileView> {

    private AuthHelper mAuthHelper;
    @Inject
    public ProfilePresenter(final AuthHelper authHelper) {
        mAuthHelper = authHelper;
    }


}
