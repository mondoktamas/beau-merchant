package com.appinstitute.beau.customer.domain.auth;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.customer.data.repository.account.AuthRepository;

import javax.inject.Inject;

import rx.Observable;

public class ForgotPasswordUseCase extends BackgroundUseCase {

    private final AuthRepository mAccountRepository;
    private String mEmail;

    @Inject
    public ForgotPasswordUseCase(final AuthRepository repository) {
        mAccountRepository = repository;
    }

    public void setData(final String email) {
        mEmail = email;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAccountRepository.forgotPassword(mEmail);
    }
}
