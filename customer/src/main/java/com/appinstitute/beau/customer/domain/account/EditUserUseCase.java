package com.appinstitute.beau.customer.domain.account;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.customer.data.repository.account.AccountRepository;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;


public class EditUserUseCase extends BackgroundUseCase {

    private UserModel mUserModel;
    private AccountRepository mAccountRepository;

    @Inject
    public EditUserUseCase(final AccountRepository accountRepository) {
        mAccountRepository = accountRepository;
    }

    public void setUserModel(final UserModel userModel) {
        mUserModel = userModel;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAccountRepository.update(mUserModel);
    }
}
