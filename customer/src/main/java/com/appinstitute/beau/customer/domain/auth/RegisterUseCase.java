package com.appinstitute.beau.customer.domain.auth;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.customer.data.repository.account.AuthRepository;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;

public class RegisterUseCase extends BackgroundUseCase {

    private final AuthRepository mAuthRepository;
    private UserModel mUserModel;

    @Inject
    public RegisterUseCase(final AuthRepository authRepository) {
        mAuthRepository = authRepository;
    }

    public void setUserModel(final UserModel userModel) {
        mUserModel = userModel;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAuthRepository.registerUser(mUserModel);
    }
}
