package com.appinstitute.beau.customer.presentation.ui.auth.login;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface LoginView extends BaseView<LoginPresenter> {
    void swingForm();

    void clearValidationErrors();

    void userIdIsEmpty();

    void passwordIsEmpty();

    void signInSuccess();

    void showErrorMessages(String errorString);

    void wrongLoginAndEmailCombination();
}
