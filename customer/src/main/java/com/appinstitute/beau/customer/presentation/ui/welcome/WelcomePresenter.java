package com.appinstitute.beau.customer.presentation.ui.welcome;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;

import javax.inject.Inject;

@ConfigPersistent
public class WelcomePresenter extends BasePresenter<WelcomeView> {

    @Inject
    public WelcomePresenter() {}

    public void onLoginClicked() {
        getView().launchLoginScreen();
    }

    public void onRegistrationClicked() {
        getView().launchRegistrationScreen();
    }

    public void onFacebookClicked() {
        getView().launchFacebookLoginScreen();
    }
}
