package com.appinstitute.beau.customer.presentation.ui.auth.register.complete.adapter;

public final class CountryItem {

    private String mTitle;

    public CountryItem(final String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }
}
