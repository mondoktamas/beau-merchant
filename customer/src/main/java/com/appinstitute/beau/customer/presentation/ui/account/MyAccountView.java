package com.appinstitute.beau.customer.presentation.ui.account;

import com.appinstitute.beau.core.presentation.base.view.BaseView;
import com.appinstitute.beau.customer.presentation.model.UserModel;

public interface MyAccountView extends BaseView<MyAccountPresenter> {
    void showAccountInfo(final UserModel userModel);
    void onGetAccountFailed(final String errorString);
    void onLogoutSuccess();
    void onLogoutFailed(final String error);
    void openMyCards();
    void openPersonalDetails();
}
