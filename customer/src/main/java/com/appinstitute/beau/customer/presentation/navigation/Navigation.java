package com.appinstitute.beau.customer.presentation.navigation;

import android.content.Intent;

import com.appinstitute.beau.customer.presentation.ui.base.view.BaseFragment;

public interface Navigation {

    void openFragment(final BaseFragment fragment);

    void startActivity(final Intent launchIntent);

    void startActivity(final Intent launchIntent, final boolean finishCurrent);

    void startActivityForResult(final Intent launchIntent, final int requestCode);
}
