package com.appinstitute.beau.customer.data.repository.account.impl;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.customer.data.network.service.AccountApiService;
import com.appinstitute.beau.customer.data.repository.account.AccountRepository;
import com.appinstitute.beau.customer.data.repository.mapper.UserDataMapper;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public class AccountRepositoryImpl implements AccountRepository {

    private AccountApiService mAccountApiService;
    private final UserDataMapper mUserDataMapper;

    @Inject
    public AccountRepositoryImpl(final AccountApiService accountApiService,
                                 final UserDataMapper userDataMapper) {
        mAccountApiService = accountApiService;
        mUserDataMapper = userDataMapper;
    }


    @Override
    public Observable<UserModel> update(UserModel userModel) {
        return mUserDataMapper.transformToUserRequest(userModel)
                .flatMap(mAccountApiService::update)
                .flatMap(mUserDataMapper::transform);
    }

    @Override
    public Observable<UserModel> getAccount() {
        return mAccountApiService.getAccount()
                .flatMap(mUserDataMapper::transform);
    }
}
