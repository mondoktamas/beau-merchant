package com.appinstitute.beau.customer.presentation.ui.auth.register.complete;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface RegistrationCompleteView extends BaseView<RegistrationCompletePresenter> {

    void clearValidationErrors();

    void emptyAddressOne();

    void emptyAddressTwo();

    void emptyTown();

    void emptyCountry();

    void emptyPostcode();

    void updateSuccess();

    void onUpdateFailure(String errorString);
}
