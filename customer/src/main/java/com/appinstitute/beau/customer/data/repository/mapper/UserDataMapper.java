package com.appinstitute.beau.customer.data.repository.mapper;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.customer.data.network.model.request.SignUpRequest;
import com.appinstitute.beau.customer.data.network.model.request.UserRequest;
import com.appinstitute.beau.customer.data.network.model.response.UserResponse;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public final class UserDataMapper {

    @Inject
    public UserDataMapper() {}

    public Observable<UserModel> transform(final UserResponse response) {
        return Observable.just(response)
                .map(userModel1 -> {
                    final UserModel model = new UserModel();
                    model.setSessionId(response.getSessionId());
                    model.setId(response.getId());
                    model.setName(response.getName());
                    model.setEmail(response.getEmail());
                    model.setAvatar(response.getAvatar());
                    model.setAddress1(response.getAddress1());
                    model.setAddress2(response.getAddress2());
                    model.setStripeCustomerId(response.getStripeCustomerId());
                    model.setCountry(response.getCountry());
                    model.setTown(response.getTown());
                    model.setPostcode(response.getPostcode());
                    model.setAuthWay(response.getAuthorizationWay());
                    model.setTotalPoints(response.getTotalPoints());

                    return model;
                });
    }

    public Observable<SignUpRequest> transform(final UserModel model) {
        return Observable.just(model)
                .map(userModel -> {
                    final SignUpRequest signUpRequest = new SignUpRequest();
                    signUpRequest.setName(userModel.getName());
                    signUpRequest.setEmail(userModel.getEmail());
                    signUpRequest.setPassword(userModel.getPassword());
                    signUpRequest.setAvatar(userModel.getAvatar());

                    return signUpRequest;
                });
    }

    public Observable<UserRequest> transformToUserRequest(final UserModel model) {
        return Observable.just(model)
                .map(userModel -> {
                    final UserRequest userRequest = new UserRequest();
                    userRequest.setName(userModel.getName());
                    userRequest.setEmail(userModel.getEmail());
                    userRequest.setPhone(userModel.getPhone());
                    userRequest.setTown(userModel.getTown());
                    userRequest.setCountry(userModel.getCountry());
                    userRequest.setPostCode(userModel.getPostcode());
                    userRequest.setAddress1(userModel.getAddress1());
                    userRequest.setAddress2(userModel.getAddress2());

                    return userRequest;
                });
    }
}
