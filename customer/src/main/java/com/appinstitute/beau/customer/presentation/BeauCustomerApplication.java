package com.appinstitute.beau.customer.presentation;

import android.content.Context;

import com.appinstitute.beau.core.presentation.BeauApplication;
import com.appinstitute.beau.customer.di.component.ApplicationComponent;
import com.appinstitute.beau.customer.di.component.DaggerApplicationComponent;
import com.appinstitute.beau.customer.di.module.ApplicationModule;

public class BeauCustomerApplication extends BeauApplication {

    protected ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        getComponent().inject(this);
    }

    public static BeauCustomerApplication get(final Context context) {
        return (BeauCustomerApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }
}