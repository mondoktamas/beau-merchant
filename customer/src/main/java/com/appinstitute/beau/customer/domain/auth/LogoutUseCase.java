package com.appinstitute.beau.customer.domain.auth;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.customer.data.repository.account.AuthRepository;

import javax.inject.Inject;

import rx.Observable;

public class LogoutUseCase extends BackgroundUseCase {

    private final AuthRepository mAuthRepository;

    @Inject
    public LogoutUseCase(final AuthRepository authRepository) {
        mAuthRepository = authRepository;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAuthRepository.logout();
    }
}
