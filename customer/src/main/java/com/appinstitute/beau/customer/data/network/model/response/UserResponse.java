package com.appinstitute.beau.customer.data.network.model.response;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class UserResponse extends ContactResponse {

    @JsonField(name="session_id") private String sessionId;
    @JsonField(name="customer_id") private Long id;
    @JsonField(name="name") private String name;
    @JsonField(name="email") private String email;
    @JsonField(name="phone") private String phone;
    @JsonField(name="stripe_customer_id") private String stripeCustomerId;
    @JsonField(name="avatar") private String avatar;
    @JsonField(name="address_line_one") private String addressLineOne;
    @JsonField(name="address_line_two") private String addressLineTwo;
    @JsonField(name="town") private String town;
    @JsonField(name="country") private String country;
    @JsonField(name="postcode") private String postcode;
    @JsonField(name="authorization_way") private String authorizationWay;
    @JsonField(name="total_points") private Double totalPoints;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(final Double totalPoints) {
        this.totalPoints = totalPoints;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddressLineOne() {
        return addressLineOne;
    }

    public void setAddressLineOne(String addressLineOne) {
        this.addressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }

    @Override
    public String getTown() {
        return town;
    }

    @Override
    public void setTown(String town) {
        this.town = town;
    }

    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getAuthorizationWay() {
        return authorizationWay;
    }

    public void setAuthorizationWay(String authorizationWay) {
        this.authorizationWay = authorizationWay;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }
}
