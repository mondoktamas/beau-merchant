package com.appinstitute.beau.customer.domain.splash;

import com.appinstitute.beau.core.domain.base.DefaultSubscriber;
import com.appinstitute.beau.core.domain.base.UseCase;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

public class SplashUseCase extends UseCase<DefaultSubscriber> {

    private final int DELAY = 3;

    @Inject
    public SplashUseCase() {}

    @Override
    protected Observable buildObservableTask() {
        return Observable.empty().delay(DELAY, TimeUnit.SECONDS);
    }
}
