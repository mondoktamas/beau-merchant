package com.appinstitute.beau.customer.presentation.ui.main.search;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface SearchView extends BaseView<SearchPresenter> {

}
