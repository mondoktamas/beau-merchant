package com.appinstitute.beau.customer.presentation.ui.account;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.data.network.config.ApiPathConst;
import com.appinstitute.beau.customer.presentation.model.UserModel;
import com.appinstitute.beau.customer.presentation.ui.base.view.ToolbarFragment;
import com.appinstitute.beau.customer.presentation.ui.welcome.WelcomeActivity;
import com.appinstitute.beau.customer.presentation.ui.widget.BorderedCircleTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;

import butterknife.BindView;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropSquareTransformation;

public class MyAccountFragment extends ToolbarFragment<MyAccountPresenter>
        implements MyAccountView, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_USER_MODEL = "ARG_USER_MODEL";

    @BindView(R.id.layout_root) FrameLayout mRootLayout;
    @BindView(R.id.text_account_name) TextView mAccountName;
    @BindView(R.id.text_account_points) TextView mAccountPoints;
    @BindView(R.id.image_account) ImageView mAccountImage;
    @BindView(R.id.layout_swipe_refresh) SwipeRefreshLayout mSwipeRefreshLayout;

    UserModel mUserModel;

    public static MyAccountFragment getNewInstance() {
        return new MyAccountFragment();
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_my_account;
    }

    @Override
    public void setupView(@Nullable final Bundle savedInstanceState) {
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorWebUrl,
                R.color.colorPrimary);
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            getPresenter().fetchAccountInfo();
        } else {
            mUserModel = savedInstanceState.getParcelable(ARG_USER_MODEL);
            showAccountInfo(mUserModel);
        }
    }

    @Override
    protected int getTitle() {
        return R.string.account_title;
    }

    @Override
    public void showAccountInfo(final UserModel userModel) {
        mSwipeRefreshLayout.setRefreshing(false);
        mUserModel = userModel;
        mAccountName.setText(getString(R.string.account_welcome_text, userModel.getName()));
        mAccountPoints.setText(getString(R.string.account_points, userModel.getTotalPoints()));

        Glide.with(this)
                .load(ApiPathConst.API_BASE_URL + userModel.getAvatar())
                .placeholder(R.drawable.img_avatar)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .bitmapTransform(new BorderedCircleTransformation(getContext()))
                .into(new GlideDrawableImageViewTarget(mAccountImage));

        Glide.with(this)
                .load(ApiPathConst.API_BASE_URL + userModel.getAvatar())
                .placeholder(R.drawable.img_avatar)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .bitmapTransform(new CropSquareTransformation(getContext()), new BlurTransformation(getContext(), 75))
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(final GlideDrawable resource, final GlideAnimation<? super GlideDrawable> glideAnimation) {
                        mRootLayout.setBackground(resource);
                    }
                });
    }

    @Override
    public void onGetAccountFailed(final String errorString) {
    }

    @Override
    public void onLogoutSuccess() {
        getNavigator().startActivity(WelcomeActivity.getLaunchIntent(getBaseActivity()), true);
    }

    @Override
    public void onLogoutFailed(final String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openMyCards() {

    }

    @Override
    public void openPersonalDetails() {

    }

    @OnClick(R.id.button_personal_details)
    public void onPersonalDetailsClicked() {
        getPresenter().openPersonalDetails();
    }

    @OnClick(R.id.button_my_cards)
    public void onMyCardsClicked() {
        getPresenter().openMyCards();
    }

    @OnClick(R.id.button_logout)
    public void onLogoutClicked() {
        showVerificationDialog();
    }

    private void showVerificationDialog() {
        new AlertDialog.Builder(getActivity(), R.style.App_Theme_AlertDialogInfo)
                .setTitle(R.string.alert_title_info)
                .setMessage(R.string.logout_verify_message)
                .setPositiveButton(R.string.logout_yes,
                        (dialog, which) -> getPresenter().logout())
                .setNegativeButton(R.string.logout_no, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_USER_MODEL, mUserModel);
    }

    @Override
    public void onRefresh() {
        getPresenter().fetchAccountInfo();
    }
}
