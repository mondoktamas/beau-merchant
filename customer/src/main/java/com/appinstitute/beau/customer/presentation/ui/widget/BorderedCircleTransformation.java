package com.appinstitute.beau.customer.presentation.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;

import com.appinstitute.beau.customer.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;

public final class BorderedCircleTransformation implements Transformation<Bitmap> {

    private final BitmapPool mBitmapPool;
    private final PathEffect mPathEffect = new CornerPathEffect(10);
    private final int mGradientFrom, mGradientTo;
    private final int mStrokeSize;

    public BorderedCircleTransformation(final Context context) {
        mBitmapPool = Glide.get(context).getBitmapPool();
        mGradientFrom = ContextCompat.getColor(context, R.color.account_border_gradient_from);
        mGradientTo = ContextCompat.getColor(context, R.color.account_border_gradient_to);
        mStrokeSize = context.getResources().getDimensionPixelSize(R.dimen.account_border_size);
    }

    @Override
    public Resource<Bitmap> transform(final Resource<Bitmap> resource,
                                      final int outWidth,
                                      final int outHeight) {
        final Bitmap source = resource.get();
        final int size = Math.min(source.getWidth(), source.getHeight());

        final int width = (source.getWidth() - size) / 2;
        final int height = (source.getHeight() - size) / 2;

        Bitmap bitmap = mBitmapPool.get(size, size, Bitmap.Config.ARGB_8888);
        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        }

        final Canvas canvas = new Canvas(bitmap);
        final Paint paint = new Paint();
        final BitmapShader shader =
                new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        if (width != 0 || height != 0) {
            // source isn't square, move viewport to center
            final Matrix matrix = new Matrix();
            matrix.setTranslate(-width, -height);
            shader.setLocalMatrix(matrix);
        }
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setPathEffect(mPathEffect);
        paint.setStrokeWidth(mStrokeSize);
        paint.setShader(new LinearGradient(0, 0, 0, size, mGradientFrom, mGradientTo, Shader.TileMode.MIRROR));
        canvas.drawCircle(r, r, r - mStrokeSize / 2f, paint);

        return BitmapResource.obtain(bitmap, mBitmapPool);
    }

    @Override public String getId() {
        return "BorderedCircleTransformation()";
    }
}
