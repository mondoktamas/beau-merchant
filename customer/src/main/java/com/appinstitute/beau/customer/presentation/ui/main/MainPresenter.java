package com.appinstitute.beau.customer.presentation.ui.main;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;

import javax.inject.Inject;

@ConfigPersistent
public class MainPresenter extends BasePresenter<MainView> {

    @Inject
    public MainPresenter() {

    }
}
