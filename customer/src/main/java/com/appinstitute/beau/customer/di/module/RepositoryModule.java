package com.appinstitute.beau.customer.di.module;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.customer.data.network.service.AccountApiService;
import com.appinstitute.beau.customer.data.network.service.AuthApiService;
import com.appinstitute.beau.customer.data.repository.account.AccountRepository;
import com.appinstitute.beau.customer.data.repository.account.AuthRepository;
import com.appinstitute.beau.customer.data.repository.account.impl.AccountRepositoryImpl;
import com.appinstitute.beau.customer.data.repository.account.impl.AuthRepositoryImpl;
import com.appinstitute.beau.customer.data.repository.mapper.UserDataMapper;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @ConfigPersistent
    @Provides
    public AuthRepository provideAuthRepository(final AuthApiService apiService, final UserDataMapper userDataMapper) {
        return new AuthRepositoryImpl(apiService, userDataMapper);
    }

    @ConfigPersistent
    @Provides
    public AccountRepository provideAccountRepository(final AccountApiService apiService, final UserDataMapper userDataMapper) {
        return new AccountRepositoryImpl(apiService, userDataMapper);
    }

}
