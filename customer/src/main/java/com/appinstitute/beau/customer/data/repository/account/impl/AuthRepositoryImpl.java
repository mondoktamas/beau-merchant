package com.appinstitute.beau.customer.data.repository.account.impl;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.customer.data.network.model.request.ForgotPasswordRequest;
import com.appinstitute.beau.customer.data.network.model.request.SignInRequest;
import com.appinstitute.beau.customer.data.network.service.AuthApiService;
import com.appinstitute.beau.customer.data.repository.account.AuthRepository;
import com.appinstitute.beau.customer.data.repository.mapper.UserDataMapper;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

import rx.Observable;

@ConfigPersistent
public class AuthRepositoryImpl implements AuthRepository {

    private final AuthApiService mApiService;
    private final UserDataMapper mUserDataMapper;

    @Inject
    public AuthRepositoryImpl(final AuthApiService apiService, final UserDataMapper userDataMapper) {
        mApiService = apiService;
        mUserDataMapper = userDataMapper;
    }

    @Override
    public Observable<UserModel> signIn(final String mUserId, final String mPassword) {
        return mApiService.signIn(new SignInRequest(mUserId, mPassword, null))
                .flatMap(mUserDataMapper::transform);
    }

    @Override
    public Observable<UserModel> registerUser(final UserModel userModel) {
        return mUserDataMapper.transform(userModel)
                .flatMap(mApiService::register)
                .flatMap(mUserDataMapper::transform);
    }

    @Override
    public Observable<Void> forgotPassword(final String email) {
        return mApiService.forgotPassword(new ForgotPasswordRequest(email));
    }

    @Override
    public Observable<Void> logout() {
        return mApiService.logout()
                .map(baseResponse -> null);
    }
}
