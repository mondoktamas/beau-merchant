package com.appinstitute.beau.customer.presentation.ui.base.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.appinstitute.beau.core.presentation.base.presenter.Presenter;
import com.appinstitute.beau.customer.R;

public abstract class ToolbarFragment<P extends Presenter> extends BaseFragment<P> {

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTitle();
    }

    private void setTitle() {
        int titleRes = getTitle();
        if (titleRes == -1) titleRes = R.string.app_name;
        getBaseActivity().setTitle(titleRes);
    }

    protected @StringRes int getTitle() {
        return -1;
    }
}
