package com.appinstitute.beau.customer.presentation.ui.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.constants.Keys;
import com.appinstitute.beau.customer.presentation.ui.account.MyAccountFragment;
import com.appinstitute.beau.customer.presentation.ui.backstack.BackStackActivity;
import com.appinstitute.beau.customer.presentation.ui.main.profile.ProfileFragment;
import com.appinstitute.beau.customer.presentation.ui.main.search.SearchFragment;

import butterknife.BindView;


public class MainActivity extends BackStackActivity<MainPresenter> implements MainView,
        NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    public static final int TAB_INVALID_POSITION = -1;
    public static final int TAB_SEARCH_POSITION = 0;
    public static final int TAB_SAVED_SEARCHES_POSITION = 1;
    public static final int TAB_ACCOUNT_POSITION = 2;
    public static final int TAB_BOOKINGS_POSITION = 3;
    public static final int TAB_POINTS_POSITION = 4;
    public static final int TAB_COMMUNITY_POSITION = 5;

    @BindView(R.id.content_layout) LinearLayout mContentLayout;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view) NavigationView mNavigationView;
    @BindView(R.id.frame_layout) FrameLayout mFrameLayout;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    private int mCurrentTabId;

    private Fragment mCurrentFragment;
    ActionBarDrawerToggle mDrawerToggle;

    public static Intent getLaunchIntent(final Context context) {
        return getLaunchIntent(context, TAB_SEARCH_POSITION);
    }

    public static Intent getLaunchIntent(final Context context,
                                         final int currentTab) {
        final Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(Keys.Extras.EXTRA_CURRENT_TAB, currentTab);
        return intent;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArguments();
        initToolbar();
        setupDrawer();

        if (savedInstanceState == null) {
            MenuItem item = mNavigationView.getMenu().getItem(mCurrentTabId);
            mNavigationView.getMenu().performIdentifierAction(item.getItemId(), TAB_SEARCH_POSITION);
            item.setChecked(true);
        }
    }

    private void initArguments() {
        final Intent intent;
        if ((intent = getIntent()) == null) return;
        mCurrentTabId = intent.getIntExtra(Keys.Extras.EXTRA_CURRENT_TAB, TAB_SEARCH_POSITION);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);

    }

    private void setupDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mContentLayout.setTranslationX(slideOffset * drawerView.getWidth());
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
            }

        };

        mDrawerToggle.setToolbarNavigationClickListener(this);

        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mNavigationView.setNavigationItemSelectedListener(this);

    }

    @NonNull
    private Fragment rootTabFragment(final int tabId) {
        switch (tabId) {
            case TAB_SEARCH_POSITION:
                return ProfileFragment.getNewInstance();
            case TAB_SAVED_SEARCHES_POSITION:
                return SearchFragment.getNewInstance();
            case TAB_ACCOUNT_POSITION:
                return MyAccountFragment.getNewInstance();
            case TAB_BOOKINGS_POSITION:
                return SearchFragment.getNewInstance();
            case TAB_POINTS_POSITION:
                return ProfileFragment.getNewInstance();
            case TAB_COMMUNITY_POSITION:
                return SearchFragment.getNewInstance();
            default:
                throw new IllegalArgumentException("No such tab");
        }
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleNewIntent(intent);
    }

    private void handleNewIntent(final Intent intent) {
        int currentTabIdx = intent.getIntExtra(Keys.Extras.EXTRA_CURRENT_TAB, TAB_SEARCH_POSITION);

        MenuItem currentTab = mNavigationView.getMenu().getItem(currentTabIdx);

        if (currentTab == null) return;
        mNavigationView.getMenu().performIdentifierAction(currentTab.getItemId(), TAB_SEARCH_POSITION);
        currentTab.setChecked(true);

        switch (currentTabIdx) {
            case TAB_SEARCH_POSITION:
                final SearchFragment searchFragment = SearchFragment.getNewInstance();
                showFragment(searchFragment);
                break;
            case TAB_SAVED_SEARCHES_POSITION:
                final ProfileFragment bookDetailsFragment = ProfileFragment.getNewInstance();
                showFragment(bookDetailsFragment);
                break;
            case TAB_ACCOUNT_POSITION:
                final SearchFragment accountFragment = SearchFragment.getNewInstance();
                showFragment(accountFragment);
                break;
            case TAB_BOOKINGS_POSITION:
                final ProfileFragment bookingsFragment = ProfileFragment.getNewInstance();
                showFragment(bookingsFragment);
                break;
            case TAB_POINTS_POSITION:
                final SearchFragment pointsFragment = SearchFragment.getNewInstance();
                showFragment(pointsFragment);
                break;
            case TAB_COMMUNITY_POSITION:
                final ProfileFragment communityFragment = ProfileFragment.getNewInstance();
                showFragment(communityFragment);
                break;
        }
        mCurrentTabId = currentTabIdx;

    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        mCurrentTabId = savedInstanceState.getInt(Keys.Args.ARG_TAB_POSITION);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void showFragment(@NonNull final Fragment fragment) {
        showFragment(fragment, true);
    }

    public void showFragment(@NonNull final Fragment fragment, final boolean addToBackStack) {
        if (mCurrentFragment != null && addToBackStack) {
            pushFragmentToBackStack(mCurrentTabId, mCurrentFragment);
        }
        replaceFragment(fragment);
    }

    private void backTo(final int tabId, @NonNull final Fragment fragment) {
        if (tabId != mCurrentTabId) {
            mCurrentTabId = tabId;
            MenuItem item = mNavigationView.getMenu().getItem(mCurrentTabId);
            mNavigationView.getMenu().performIdentifierAction(item.getItemId(), TAB_SEARCH_POSITION);
            item.setChecked(true);
        }
        replaceFragment(fragment);
        getSupportFragmentManager().executePendingTransactions();
    }

    private void replaceFragment(@NonNull final Fragment fragment) {
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.frame_layout, fragment);
        tr.commitAllowingStateLoss();
        mCurrentFragment = fragment;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            final Pair<Integer, Fragment> pair = popFragmentFromBackStack();
            if (pair != null) {
                backTo(pair.first, pair.second);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        outState.putInt(Keys.Args.ARG_TAB_POSITION, mCurrentTabId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void openFragmentByTabPosition(final int tabPosition, final Bundle arg) {
        mCurrentTabId = tabPosition;
        Fragment fragment = popFragmentFromBackStack(mCurrentTabId);
        if (fragment == null) {
            fragment = rootTabFragment(mCurrentTabId);
        }
        if (arg != null) {
            fragment.setArguments(arg);
        }
        replaceFragment(fragment);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int position = TAB_SEARCH_POSITION;

        switch (item.getItemId()) {
            case R.id.nav_search:
                position = TAB_SEARCH_POSITION;
                break;
            case R.id.nav_saved_searches:
                position = TAB_SAVED_SEARCHES_POSITION;
                break;
            case R.id.nav_my_account:
                position = TAB_ACCOUNT_POSITION;
                break;
            case R.id.nav_my_bookings:
                position = TAB_BOOKINGS_POSITION;
                break;
            case R.id.nav_my_points:
                position = TAB_POINTS_POSITION;
                break;
            case R.id.nav_community:
                position = TAB_COMMUNITY_POSITION;
                break;
        }

        if (mCurrentFragment != null && position != mCurrentTabId) {
            if (position == TAB_SEARCH_POSITION) {
                clearBackStack(position);
            }
            pushFragmentToBackStack(mCurrentTabId, mCurrentFragment);
            openFragmentByTabPosition(position, null);

        } else if (mCurrentFragment == null) {
            openFragmentByTabPosition(position, null);

        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }
}
