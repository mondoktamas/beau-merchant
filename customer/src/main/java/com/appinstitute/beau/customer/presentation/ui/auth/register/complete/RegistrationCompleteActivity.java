package com.appinstitute.beau.customer.presentation.ui.auth.register.complete;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.ui.auth.register.complete.adapter.CountryAdapter;
import com.appinstitute.beau.customer.presentation.ui.auth.register.complete.adapter.CountryItem;
import com.appinstitute.beau.customer.presentation.ui.base.view.BaseActivity;
import com.appinstitute.beau.customer.presentation.ui.main.MainActivity;

import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;

public final class RegistrationCompleteActivity extends BaseActivity<RegistrationCompletePresenter>
        implements RegistrationCompleteView {

    @BindView(R.id.layout_address_1) TextInputLayout mAddressOneLayout;
    @BindView(R.id.edit_address_1) TextInputEditText mAddressOneEdit;
    @BindView(R.id.layout_address_2) TextInputLayout mAddressTwoLayout;
    @BindView(R.id.edit_address_2) TextInputEditText mAddressTwoEdit;
    @BindView(R.id.layout_town) TextInputLayout mTownLayout;
    @BindView(R.id.edit_town) TextInputEditText mTownEdit;
    @BindView(R.id.layout_country) TextInputLayout mCountryLayout;
    @BindView(R.id.edit_country) TextInputEditText mCountryEdit;
    @BindView(R.id.layout_postcode) TextInputLayout mPostcodeLayout;
    @BindView(R.id.edit_postcode) TextInputEditText mPostcodeEdit;
    @BindView(R.id.bottom_sheet) View mBottomSheetView;

    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    public static Intent getLaunchIntent(final BaseActivity baseActivity) {
        final Intent launchIntent = new Intent(baseActivity, RegistrationCompleteActivity.class);
        return launchIntent;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_complete_registration;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBottomSheet();
    }

    private void initBottomSheet() {
        mBehavior = BottomSheetBehavior.from(mBottomSheetView);
    }

    private void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_country, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CountryAdapter(Collections.singletonList(new CountryItem("United Kingdom")),
                item -> {
                    mCountryEdit.setText(item.getTitle());
                    mPostcodeEdit.requestFocus();
                    if (mBottomSheetDialog != null) {
                        mBottomSheetDialog.dismiss();
                    }
                }));
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    @OnClick(R.id.edit_country)
    void onCountryClicked() {
        showBottomSheetDialog();
    }

    @OnClick(R.id.button_complete_registration)
    public void onSaveClicked() {
        getPresenter().onSaveClicked(mAddressOneEdit.getText().toString().trim(),
                mAddressTwoEdit.getText().toString().trim(),
                mTownEdit.getText().toString().trim(),
                mCountryEdit.getText().toString().trim(),
                mPostcodeEdit.getText().toString().trim());
    }

    @Override
    public void clearValidationErrors() {
        mAddressOneLayout.setError(null);
        mAddressTwoLayout.setError(null);
        mTownLayout.setError(null);
        mCountryLayout.setError(null);
        mPostcodeLayout.setError(null);
    }

    @Override
    public void emptyAddressOne() {
        mAddressOneLayout.setError(getText(R.string.register_complete_error_empty_address1));
    }

    @Override
    public void emptyAddressTwo() {
        mAddressTwoLayout.setError(getText(R.string.register_complete_error_empty_address2));
    }

    @Override
    public void emptyTown() {
        mTownLayout.setError(getText(R.string.register_complete_error_empty_town));
    }

    @Override
    public void emptyCountry() {
        mCountryLayout.setError(getText(R.string.register_complete_error_empty_country));
    }

    @Override
    public void emptyPostcode() {
        mPostcodeLayout.setError(getText(R.string.register_complete_error_empty_postcode));
    }

    @Override
    public void updateSuccess() {
        getNavigator().startActivity(MainActivity.getLaunchIntent(this, MainActivity.TAB_SEARCH_POSITION));
    }

    @Override
    public void onUpdateFailure(String errorString) {
        new AlertDialog.Builder(this, R.style.App_Theme_AlertDialogInfo)
                .setTitle(R.string.register_failure_message_title)
                .setMessage(errorString)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }
}
