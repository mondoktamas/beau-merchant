package com.appinstitute.beau.customer.data.network.service;

import com.appinstitute.beau.customer.data.network.config.ApiPathConst;
import com.appinstitute.beau.customer.data.network.model.request.UserRequest;
import com.appinstitute.beau.customer.data.network.model.response.UserResponse;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface AccountApiService {

    @POST(ApiPathConst.METHOD_ACCOUNT_UPDATE)
    Observable<UserResponse> update(@Body UserRequest userRequest);

    @POST(ApiPathConst.METHOD_GET_ACCOUNT)
    Observable<UserResponse> getAccount();
}
