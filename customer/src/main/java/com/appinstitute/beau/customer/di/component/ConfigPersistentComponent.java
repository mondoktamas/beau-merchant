package com.appinstitute.beau.customer.di.component;

import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.customer.di.module.ActivityModule;
import com.appinstitute.beau.customer.di.module.InteractorModule;
import com.appinstitute.beau.customer.di.module.RepositoryModule;
import com.appinstitute.beau.customer.presentation.ui.base.view.BaseActivity;

import dagger.Component;

/**
 * A dagger component that will live during the lifecycle of an Activity but it won't
 * be destroy during configuration changes. Check {@link BaseActivity} to see how this components
 * survives configuration changes.
 */
@ConfigPersistent
@Component(dependencies = ApplicationComponent.class, modules = {RepositoryModule.class, InteractorModule.class})
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(final ActivityModule activityModule);
}