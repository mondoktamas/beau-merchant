package com.appinstitute.beau.customer.presentation.ui.auth.register.basic;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.domain.base.DefaultInternetSubscriber;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.customer.domain.auth.RegisterUseCase;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

@ConfigPersistent
public final class RegistrationPresenter extends BasePresenter<RegistrationView> {

    private AuthHelper mAuthHelper;
    private ErrorHelper mErrorHelper;
    private RegisterUseCase mRegisterUseCase;

    @Inject
    public RegistrationPresenter(final AuthHelper authHelper,
                                 final ErrorHelper errorHelper,
                                 final RegisterUseCase registerUseCase) {
        mAuthHelper = authHelper;
        mErrorHelper = errorHelper;
        mRegisterUseCase = registerUseCase;
    }

    public void registerClicked(final String fullName,
                                final String email,
                                final String password,
                                final String passwordConfirm) {
        getView().clearValidationErrors();
        if (Validator.isEmpty(fullName)) {
            getView().emptyFullName();
            return;
        }
        if (!Validator.isCorrectEmail(email)) {
            getView().incorrectEmail();
            return;
        }
        if (!Validator.isCorrectPassword(password)) {
            getView().incorrectPassword();
            return;
        }
        if (!Validator.isEquals(password, passwordConfirm)) {
            getView().passwordsDoNotMutch();
            return;
        }

        mRegisterUseCase.setUserModel(new UserModel(fullName, email, password));
        mRegisterUseCase.execute(new SignUpSubscriber());
    }

    class SignUpSubscriber extends DefaultInternetSubscriber<UserModel> {

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }

        @Override
        public void onNext(UserModel userModel) {
            mAuthHelper.setUserData(userModel);
            getView().registerSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            super.onError(throwable);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void handleUnprocessableEntity(String[] errors) {
            getView().onSignUpFailure(mErrorHelper.getErrorString(errors));
        }

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }
    }
}
