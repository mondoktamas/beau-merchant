package com.appinstitute.beau.customer.presentation.ui.auth.forgot;

import com.appinstitute.beau.core.data.network.exception.RetrofitException;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.domain.base.DefaultInternetSubscriber;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.customer.domain.auth.ForgotPasswordUseCase;

import javax.inject.Inject;

@ConfigPersistent
public class ForgotPresenter  extends BasePresenter<ForgotView> {

    private ErrorHelper mErrorHandler;
    private ForgotPasswordUseCase mForgotPasswordUseCase;

    @Inject
    public ForgotPresenter(final ErrorHelper errorHelper,
                           final ForgotPasswordUseCase forgotPasswordUseCase) {
        mErrorHandler = errorHelper;
        mForgotPasswordUseCase = forgotPasswordUseCase;
    }

    public void onSendClicked(final String email) {
        getView().clearValidationErrors();
        if (Validator.isEmpty(email)) {
            getView().emptyEmail();
            return;
        }

        if (!Validator.isCorrectEmail(email)) {
            getView().incorrectEmail();
            return;
        }

        mForgotPasswordUseCase.setData(email);
        mForgotPasswordUseCase.execute(new ForgotSubscriber());
    }

    class ForgotSubscriber extends DefaultInternetSubscriber<Void> {

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }

        @Override
        public void onNext(Void aVoid) {
            getView().requestSuccess();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void handleUnexpectedError(RetrofitException exception) {
            getView().requestFailure();
        }

        @Override
        public void handleUnprocessableEntity(String[] errors) {
            getView().showErrorMessages(mErrorHandler.getErrorString(errors));
        }

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }
    }
}
