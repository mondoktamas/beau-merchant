package com.appinstitute.beau.customer.data.repository.account;

import com.appinstitute.beau.customer.presentation.model.UserModel;

import rx.Observable;

public interface AccountRepository {

    Observable<UserModel> update(final UserModel userModel);

    Observable<UserModel> getAccount();
}
