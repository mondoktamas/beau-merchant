package com.appinstitute.beau.customer.presentation.ui.main.profile;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.ui.base.view.ToolbarFragment;

public class ProfileFragment extends ToolbarFragment<ProfilePresenter> implements ProfileView {

//    @BindView(R.id.toolbar) Toolbar mToolbar;
//
//    @BindView(R.id.background_image) ImageView mBackgroundImage;
//    @BindView(R.id.text_title) TextView mTitleText;
//
//    @BindView(R.id.text_current_date) TextView mCurrentDateText;
//    @BindView(R.id.text_new_bookings_count) TextView mNewBookingsCountText;
//    @BindView(R.id.text_open_offers_count) TextView mOpenOffersCountText;

    public static ProfileFragment getNewInstance(){
        return new ProfileFragment();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public void setBusinessName(final String businessName) {
//        mTitleText.setText(businessName);
    }

    @Override
    public void setNewBookingsCount(int count) {
//        mNewBookingsCountText.setText(getString(R.string.profile_new_bookings_count, count));
    }

    @Override
    public void setCurrentDate(String dateString) {
//        mCurrentDateText.setText(dateString);
    }

    @Override
    public void setOpenOffersCount(int count) {
//        mOpenOffersCountText.setText(getString(R.string.profile_open_offers_count, count));
    }

    @Override
    public void setImage(String imageUrl) {
//        Glide.with(this)
//                .load(BaseApiPathConst.API_BASE_URL + imageUrl)
//                .crossFade()
//                .into(mBackgroundImage);
    }

//    @OnClick(R.id.button_view_booked_appointments)
//    public void onViewBookedAppointmentsClicked() {
//        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
//    }
//
//    @OnClick(R.id.button_manage_available_offers)
//    public void onManageAvailableOffersClicked() {
//        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
//    }
//
//    @OnClick(R.id.button_price_list)
//    public void onPriceListClicked() {
//        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
//    }
//
//    @OnClick(R.id.button_company_details)
//    public void onCompanyDetailsClicked() {
//        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
//    }
//
//    @OnClick(R.id.button_staff_list)
//    public void onStaffListClicked() {
//        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
//    }
//
//    @OnClick(R.id.button_my_community_feed)
//    public void onMyCommunityFeedClicked() {
//        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
//    }
//
//    @OnClick(R.id.button_account_settings)
//    public void onAccountSettingsClicked() {
//        Toast.makeText(getActivity(), "Not Yet Implemented", Toast.LENGTH_SHORT).show();
//    }
}
