package com.appinstitute.beau.customer.presentation.ui.auth.login;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.data.network.exception.RetrofitException;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.domain.base.DefaultInternetSubscriber;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.customer.domain.auth.SignInUseCase;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

@ConfigPersistent
public class LoginPresenter extends BasePresenter<LoginView> {

    private AuthHelper mAuthHelper;
    private ErrorHelper mErrorHandler;
    private final SignInUseCase mSignInUseCase;

    @Inject
    public LoginPresenter(final AuthHelper authHelper, final ErrorHelper errorHelper, final SignInUseCase signInUseCase) {
        mAuthHelper = authHelper;
        mErrorHandler = errorHelper;
        mSignInUseCase = signInUseCase;
    }

    public void onLoginClicked(final String userId, final String password) {
        getView().clearValidationErrors();
        if (Validator.isEmpty(userId) && Validator.isEmpty(password)) {
            getView().swingForm();
            return;
        }

        if (Validator.isEmpty(userId)) {
            getView().userIdIsEmpty();
            return;
        }
        if (Validator.isEmpty(password)) {
            getView().passwordIsEmpty();
            return;
        }

        mSignInUseCase.setData(userId, password);
        unsubscribeOnDestroy(mSignInUseCase.execute(new SignInSubscriber()));
    }

    class SignInSubscriber extends DefaultInternetSubscriber<UserModel> {

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }

        @Override
        public void onNext(UserModel userModel) {
            mAuthHelper.setUserData(userModel);
            getView().signInSuccess();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            e.printStackTrace();
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void handleUnexpectedError(RetrofitException exception) {
            getView().wrongLoginAndEmailCombination();
        }

        @Override
        public void handleUnprocessableEntity(String[] errors) {
            getView().showErrorMessages(mErrorHandler.getErrorString(errors));
        }

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }
    }

    /*

    Client - nelson+client@appinsitute.co.uk - Qwe123qwe
    Merchant - nelsonsalon - Pass Qwe123qwe
     */
}
