package com.appinstitute.beau.customer.presentation.ui.welcome;

import android.content.Intent;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.widget.Toast;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.ui.auth.login.LoginActivity;
import com.appinstitute.beau.customer.presentation.ui.auth.register.basic.RegistrationActivity;
import com.appinstitute.beau.customer.presentation.ui.base.view.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public final class WelcomeActivity extends BaseActivity<WelcomePresenter> implements WelcomeView {

    @BindView(R.id.text_terms_and_policy) TextView mTermsAndPolicy;

    public static Intent getLaunchIntent(final BaseActivity baseActivity) {
        return new Intent(baseActivity, WelcomeActivity.class);
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTermsAndPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void launchLoginScreen() {
        getNavigator().startActivity(LoginActivity.getLaunchIntent(this));
    }

    @Override
    public void launchRegistrationScreen() {
        getNavigator().startActivity(RegistrationActivity.getLaunchIntent(this));
    }

    @Override
    public void launchFacebookLoginScreen() {
        Toast.makeText(this, "Not Yet implemented", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_login)
    void onLoginClicked() {
        getPresenter().onLoginClicked();
    }

    @OnClick(R.id.button_register)
    void onRegisterClicked() {
        getPresenter().onRegistrationClicked();
    }

    @OnClick(R.id.button_facebook)
    void onFacebookClicked() {
        getPresenter().onFacebookClicked();
    }
}
