package com.appinstitute.beau.customer.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.appinstitute.beau.core.presentation.base.model.BaseModel;

public final class UserModel extends BaseModel implements Parcelable {

    private String mSessionId;
    private Long mId;
    private String mName;
    private String mEmail;
    private String mPhone;
    private String mPassword;
    private String mStripeCustomerId;
    private String mAvatar;
    private String mAddress1;
    private String mAddress2;
    private String mTown;
    private String mCountry;
    private String mPostcode;
    private String mAuthWay;
    private Double mTotalPoints;

    public UserModel() { }

    public UserModel(final String name,
                     final String email,
                     final String password) {
        mName = name;
        mEmail = email;
        mPassword = password;
    }

    public UserModel(String name, String email,
                     String address1, String address2,
                     String town, String country, String postcode) {
        mName = name;
        mEmail = email;
        mAddress1 = address1;
        mAddress2 = address2;
        mTown = town;
        mCountry = country;
        mPostcode = postcode;
    }

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(String sessionId) {
        mSessionId = sessionId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getStripeCustomerId() {
        return mStripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        mStripeCustomerId = stripeCustomerId;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String address1) {
        mAddress1 = address1;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public void setAddress2(String address2) {
        mAddress2 = address2;
    }

    public String getTown() {
        return mTown;
    }

    public void setTown(String town) {
        mTown = town;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getPostcode() {
        return mPostcode;
    }

    public void setPostcode(String postcode) {
        mPostcode = postcode;
    }

    public String getAuthWay() {
        return mAuthWay;
    }

    public void setAuthWay(String authWay) {
        mAuthWay = authWay;
    }

    public Double getTotalPoints() {
        return mTotalPoints;
    }

    public void setTotalPoints(final Double totalPoints) {
        mTotalPoints = totalPoints;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "mSessionId='" + mSessionId + '\'' +
                ", mId=" + mId +
                ", mName='" + mName + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mPassword='" + mPassword + '\'' +
                ", mStripeCustomerId='" + mStripeCustomerId + '\'' +
                ", mAvatar='" + mAvatar + '\'' +
                ", mAddress1='" + mAddress1 + '\'' +
                ", mAddress2='" + mAddress2 + '\'' +
                ", mTown='" + mTown + '\'' +
                ", mCountry='" + mCountry + '\'' +
                ", mPostcode=" + mPostcode +
                ", mAuthWay=" + mAuthWay +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mSessionId);
        dest.writeValue(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mEmail);
        dest.writeString(this.mPhone);
        dest.writeString(this.mPassword);
        dest.writeString(this.mStripeCustomerId);
        dest.writeString(this.mAvatar);
        dest.writeString(this.mAddress1);
        dest.writeString(this.mAddress2);
        dest.writeString(this.mTown);
        dest.writeString(this.mCountry);
        dest.writeString(this.mPostcode);
        dest.writeString(this.mAuthWay);
        dest.writeValue(this.mTotalPoints);
    }

    protected UserModel(Parcel in) {
        this.mSessionId = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mName = in.readString();
        this.mEmail = in.readString();
        this.mPhone = in.readString();
        this.mPassword = in.readString();
        this.mStripeCustomerId = in.readString();
        this.mAvatar = in.readString();
        this.mAddress1 = in.readString();
        this.mAddress2 = in.readString();
        this.mTown = in.readString();
        this.mCountry = in.readString();
        this.mPostcode = in.readString();
        this.mAuthWay = in.readString();
        this.mTotalPoints = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
