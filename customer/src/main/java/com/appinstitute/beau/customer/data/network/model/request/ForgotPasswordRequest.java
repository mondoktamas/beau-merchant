package com.appinstitute.beau.customer.data.network.model.request;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class ForgotPasswordRequest {

    @JsonField(name="email") private String mEmail;

    public ForgotPasswordRequest() {
    }

    public ForgotPasswordRequest(String email) {
        mEmail = email;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }
}
