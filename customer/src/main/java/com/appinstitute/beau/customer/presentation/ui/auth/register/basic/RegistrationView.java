package com.appinstitute.beau.customer.presentation.ui.auth.register.basic;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface RegistrationView extends BaseView<RegistrationPresenter> {

    void clearValidationErrors();

    void emptyFullName();
    void incorrectEmail();
    void incorrectPassword();
    void passwordsDoNotMutch();

    void registerSuccess();

    void onSignUpFailure(String errorString);
}
