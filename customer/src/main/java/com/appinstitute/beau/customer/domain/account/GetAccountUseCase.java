package com.appinstitute.beau.customer.domain.account;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.customer.data.repository.account.AccountRepository;

import javax.inject.Inject;

import rx.Observable;

public class GetAccountUseCase extends BackgroundUseCase {

    private final AccountRepository mAccountRepository;

    @Inject
    public GetAccountUseCase(final AccountRepository accountRepository) {
        mAccountRepository = accountRepository;
    }

    @Override
    protected Observable buildObservableTask() {
        return mAccountRepository.getAccount();
    }
}
