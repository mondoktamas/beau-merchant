package com.appinstitute.beau.customer.presentation.ui.splash;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.ui.base.view.BaseActivity;
import com.appinstitute.beau.customer.presentation.ui.main.MainActivity;
import com.appinstitute.beau.customer.presentation.ui.welcome.WelcomeActivity;

public class SplashActivity extends BaseActivity<SplashPresenter> implements SplashView {

    @Override
    public int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().startSplashTask();
    }

    @Override
    public void launchNextScreen(final boolean isLoggedIn) {
        if (isLoggedIn) {
            getNavigator().startActivity(MainActivity.getLaunchIntent(this, MainActivity.TAB_SEARCH_POSITION));
        } else {
            getNavigator().startActivity(WelcomeActivity.getLaunchIntent(this));
        }

        finish();
    }
}
