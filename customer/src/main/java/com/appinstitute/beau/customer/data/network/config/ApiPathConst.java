package com.appinstitute.beau.customer.data.network.config;

public interface ApiPathConst extends BaseApiPathConst {
    String METHOD_SIGN_IN = "login";
    String METHOD_SIGN_UP = "register";
    String METHOD_ACCOUNT_UPDATE = "account/edit";
    String METHOD_FORGOT_PASSWORD = "forgot";
    String METHOD_LOGOUT = "logout";
    String METHOD_GET_ACCOUNT = "account/get";
}
