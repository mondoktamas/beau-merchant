package com.appinstitute.beau.customer.presentation.ui.backstack;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.util.Log;

import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.core.presentation.presentation.backstack.BackStackEntry;
import com.appinstitute.beau.core.presentation.presentation.backstack.BackStackManager;
import com.appinstitute.beau.customer.presentation.ui.base.view.BaseActivity;

public abstract class BackStackActivity<P extends BasePresenter> extends BaseActivity<P> {

    private static final String STATE_BACK_STACK_MANAGER = "back_stack_manager";

    protected BackStackManager backStackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        backStackManager = new BackStackManager();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        backStackManager.restoreState(savedInstanceState.getParcelable(STATE_BACK_STACK_MANAGER));
    }

    /**
     * @return false if failed to put fragment in back stack. Relates to issue:
     * java.lang.IllegalStateException: Fragment is not currently in the FragmentManager at
     * android.support.v4.app.FragmentManagerImpl.saveFragmentInstanceState(FragmentManager.java:702)
     */
    protected boolean pushFragmentToBackStack(final int hostId, @NonNull final Fragment fragment) {
        try {
            BackStackEntry entry = BackStackEntry.create(getSupportFragmentManager(), fragment);
            backStackManager.push(hostId, entry);
            return true;
        } catch (Exception e) {
            Log.e("MultiBackStack", "Failed to addItems fragment to back stack", e);
            return false;
        }
    }

    @Nullable
    protected Fragment popFragmentFromBackStack(final int hostId) {
        BackStackEntry entry = backStackManager.pop(hostId);
        return entry != null ? entry.toFragment(this) : null;
    }

    @Nullable
    protected Pair<Integer, Fragment> popFragmentFromBackStack() {
        Pair<Integer, BackStackEntry> pair = backStackManager.pop();
        return pair != null ? Pair.create(pair.first, pair.second.toFragment(this)) : null;
    }

    /**
     * @return false if back stack is missing.
     */
    protected boolean clearBackStack(final int hostId) {
        return backStackManager.clear(hostId);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_BACK_STACK_MANAGER, backStackManager.saveState());
    }

    @Override
    protected void onDestroy() {
        backStackManager = null;
        super.onDestroy();
    }
}
