package com.appinstitute.beau.customer.data.network.model.response;

import com.appinstitute.beau.core.data.network.model.response.BaseResponse;
import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class ContactResponse extends BaseResponse {

    @JsonField(name="business_name")
    private String businessName;
    @JsonField(name="address_1")
    private String address1;
    @JsonField(name="address_2")
    private String address2;
    @JsonField(name="town")
    private String town;
    @JsonField(name="country")
    private String country;
    @JsonField(name="post_code")
    private String postCode;
    @JsonField(name="email")
    private String email;
    @JsonField(name="phone")
    private String phone;
    @JsonField(name="latitude")
    private String latitude;
    @JsonField(name="longitude")
    private String longitude;
    @JsonField(name="description")
    private String description;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
