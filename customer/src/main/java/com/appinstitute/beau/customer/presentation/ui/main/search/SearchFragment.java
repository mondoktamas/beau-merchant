package com.appinstitute.beau.customer.presentation.ui.main.search;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.ui.base.view.ToolbarFragment;

public class SearchFragment extends ToolbarFragment<SearchPresenter> implements SearchView {

//    @BindView(R.id.toolbar) Toolbar mToolbar;
//
//    @BindView(R.id.background_image) ImageView mBackgroundImage;
//    @BindView(R.id.text_title) TextView mTitleText;
//
//    @BindView(R.id.text_current_date) TextView mCurrentDateText;
//    @BindView(R.id.text_new_bookings_count) TextView mNewBookingsCountText;
//    @BindView(R.id.text_open_offers_count) TextView mOpenOffersCountText;

    public static SearchFragment getNewInstance(){
        return new SearchFragment();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_search;
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }
}
