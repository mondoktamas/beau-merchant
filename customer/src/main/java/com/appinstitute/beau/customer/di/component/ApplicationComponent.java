package com.appinstitute.beau.customer.di.component;

import android.app.Application;
import android.content.Context;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ApplicationContext;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.customer.data.network.service.AccountApiService;
import com.appinstitute.beau.customer.data.network.service.AuthApiService;
import com.appinstitute.beau.customer.di.module.ApplicationModule;
import com.appinstitute.beau.customer.di.module.NetworkModule;
import com.appinstitute.beau.customer.presentation.BeauCustomerApplication;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {
    void inject(BeauCustomerApplication application);

    @ApplicationContext
    Context context();
    Application application();

    AuthApiService authApiService();

    AccountApiService accountApiService();

    AuthHelper authHelper();

    ErrorHelper errorHelper();
}
