package com.appinstitute.beau.customer.presentation.constants;

public interface Keys {
    
    interface Args {

        String ARG_TAB_POSITION = "arg_tab_position";
    }

    interface Extras {

        String EXTRA_CURRENT_TAB = "com.appinstitute.beau.customer.current_tab";
        String EXTRA_CURRENT_FRAGMENT_ARGS = "com.appinstitute.beau.customer.current_fragment_args";
    }

    interface Prefs {

    }
}
