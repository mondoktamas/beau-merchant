package com.appinstitute.beau.customer.data.repository.account;

import com.appinstitute.beau.customer.presentation.model.UserModel;

import rx.Observable;

public interface AuthRepository {

    Observable<UserModel> signIn(final String mUserId, final String mPassword);

    Observable<UserModel> registerUser(final UserModel userModel);

    Observable<Void> forgotPassword(final String email);

    Observable<Void> logout();
}
