package com.appinstitute.beau.customer.presentation.ui.auth.forgot;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.widget.Toast;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.ui.auth.login.LoginActivity;
import com.appinstitute.beau.customer.presentation.ui.base.view.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotActivity extends BaseActivity<ForgotPresenter> implements ForgotView {

    @BindView(R.id.text_back_to_login) TextView mBackToLoginTextLink;
    @BindView(R.id.layout_email) TextInputLayout mEmailLayout;
    @BindView(R.id.edit_email) TextInputEditText mEmailEdit;

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_forgot;
    }

    public static Intent getLaunchIntent(final BaseActivity baseActivity) {
        return new Intent(baseActivity, ForgotActivity.class);
    }

    @Override
    public void clearValidationErrors() {
        mEmailLayout.setError(null);
    }

    @Override
    public void emptyEmail() {
        mEmailLayout.setError(getString(R.string.forgot_error_empty_email));
    }

    @Override
    public void incorrectEmail() {
        mEmailLayout.setError(getString(R.string.forgot_error_invalid_email));
    }

    @Override
    public void requestSuccess() {
        Toast.makeText(this, R.string.forgot_message_success, Toast.LENGTH_SHORT).show();
        getNavigator().startActivity(LoginActivity.getLaunchIntent(this));
    }

    @Override
    public void requestFailure() {
        Toast.makeText(this, R.string.forgot_message_invalid_email, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessages(String errorString) {
        new AlertDialog.Builder(this, R.style.App_Theme_AlertDialogInfo)
                .setMessage(errorString)
                .create()
                .show();
    }

    @OnClick(R.id.button_send)
    public void onSendClicked() {
        getPresenter().onSendClicked(mEmailEdit.getText().toString().trim());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBackToLoginTextLink.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
