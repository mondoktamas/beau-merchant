package com.appinstitute.beau.customer.presentation.ui.splash;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface SplashView extends BaseView<SplashPresenter> {
    void launchNextScreen(final boolean isLoggedIn);
}
