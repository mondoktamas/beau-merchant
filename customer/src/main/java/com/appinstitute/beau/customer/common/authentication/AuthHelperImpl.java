package com.appinstitute.beau.customer.common.authentication;

import android.content.Context;
import android.content.SharedPreferences;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AuthHelperImpl implements AuthHelper<UserModel> {

    private static final String PREF_USER = "user_pref";
    private static final String PREF_USER_TOKEN = "session_id";
    private static final String PREF_USER_ID = "id";
    private static final String PREF_USER_NAME = "name";
    private static final String PREF_USER_EMAIL = "email";
    private static final String PREF_USER_STRIPE_ID = "stripe_customer_id";
    private static final String PREF_USER_AVATAR = "avatar";
    private static final String PREF_USER_ADDRESS_1 = "address_line_one";
    private static final String PREF_USER_ADDRESS_2 = "address_line_two";
    private static final String PREF_USER_TOWN = "town";
    private static final String PREF_USER_COUNTRY = "country";
    private static final String PREF_USER_POSTCODE = "postcode";
    private static final String PREF_USER_AUTHORIZATION_WAY = "authorization_way";


    SharedPreferences mPreferences;

    private UserModel mUserModel;

    @Inject
    public AuthHelperImpl(final Context context) {
        mPreferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
    }

    @Override
    public void setUserData(final UserModel userData) {
        mUserModel.setSessionId(userData.getSessionId());
        mUserModel.setId(userData.getId());
        mUserModel.setName(userData.getName());
        mUserModel.setEmail(userData.getEmail());
        mUserModel.setStripeCustomerId(userData.getStripeCustomerId());
        mUserModel.setAvatar(userData.getAvatar());
        mUserModel.setAddress1(userData.getAddress1());
        mUserModel.setAddress2(userData.getAddress2());
        mUserModel.setTown(userData.getTown());
        mUserModel.setCountry(userData.getCountry());
        mUserModel.setPostcode(userData.getPostcode());
        mUserModel.setAuthWay(userData.getAuthWay());

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PREF_USER_TOKEN, mUserModel.getSessionId());
        editor.putLong(PREF_USER_ID, mUserModel.getId());
        editor.putString(PREF_USER_NAME, mUserModel.getName());
        editor.putString(PREF_USER_EMAIL, mUserModel.getEmail());
        editor.putString(PREF_USER_STRIPE_ID, mUserModel.getStripeCustomerId());
        editor.putString(PREF_USER_AVATAR, mUserModel.getAvatar());
        editor.putString(PREF_USER_ADDRESS_1, mUserModel.getAddress1());
        editor.putString(PREF_USER_ADDRESS_2, mUserModel.getAddress2());
        editor.putString(PREF_USER_TOWN, mUserModel.getTown());
        editor.putString(PREF_USER_COUNTRY, mUserModel.getCountry());
        editor.putString(PREF_USER_POSTCODE, mUserModel.getPostcode());
        editor.putString(PREF_USER_AUTHORIZATION_WAY, mUserModel.getAuthWay());

        editor.apply();
    }

    @Override
    public UserModel getUserModel() {
        if (mUserModel != null) {
            return mUserModel;
        } else {
            UserModel user = new UserModel();
            user.setSessionId(mPreferences.getString(PREF_USER_TOKEN, ""));
            user.setId(mPreferences.getLong(PREF_USER_ID, 0));
            user.setName(mPreferences.getString(PREF_USER_NAME, ""));
            user.setEmail(mPreferences.getString(PREF_USER_EMAIL, ""));
            user.setStripeCustomerId(mPreferences.getString(PREF_USER_STRIPE_ID, ""));
            user.setAvatar(mPreferences.getString(PREF_USER_AVATAR, ""));
            user.setAddress1(mPreferences.getString(PREF_USER_ADDRESS_1, ""));
            user.setAddress2(mPreferences.getString(PREF_USER_ADDRESS_2, ""));
            user.setTown(mPreferences.getString(PREF_USER_TOWN, ""));
            user.setCountry(mPreferences.getString(PREF_USER_COUNTRY, ""));
            user.setPostcode(mPreferences.getString(PREF_USER_POSTCODE, ""));
            user.setAuthWay(mPreferences.getString(PREF_USER_AUTHORIZATION_WAY, ""));

            return user;
        }
    }

    @Override
    public String getToken() {
        if (mUserModel != null) {
            return mUserModel.getSessionId();
        } else {
            mUserModel = getUserModel();
            return mUserModel.getSessionId();
        }
    }

    @Override
    public boolean isLoggedIn() {
        if (getToken() != null && !getToken().equals("")) {
            return true;
        }
        return false;
    }

    @Override
    public void releaseData() {
        mUserModel = null;
        final SharedPreferences.Editor editor = mPreferences.edit();
        editor.clear().apply();
    }
}
