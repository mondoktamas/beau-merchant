package com.appinstitute.beau.customer.domain.auth;

import com.appinstitute.beau.core.domain.base.BackgroundUseCase;
import com.appinstitute.beau.customer.data.repository.account.AuthRepository;

import javax.inject.Inject;

import rx.Observable;

public class SignInUseCase extends BackgroundUseCase {

    private String mUserId;
    private String mPassword;

    private AuthRepository mAccountRepository;

    @Inject
    public SignInUseCase(final AuthRepository repository) {
        mAccountRepository = repository;
    }

    public void setData(final String userID, final String password) {
        mUserId = userID;
        mPassword = password;
    }
    @Override
    protected Observable buildObservableTask() {
        return mAccountRepository.signIn(mUserId, mPassword);
    }
}
