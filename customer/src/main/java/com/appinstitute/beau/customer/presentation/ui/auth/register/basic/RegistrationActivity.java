package com.appinstitute.beau.customer.presentation.ui.auth.register.basic;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.appinstitute.beau.customer.R;
import com.appinstitute.beau.customer.presentation.ui.auth.register.complete.RegistrationCompleteActivity;
import com.appinstitute.beau.customer.presentation.ui.base.view.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public final class RegistrationActivity extends BaseActivity<RegistrationPresenter>
        implements RegistrationView {

    @BindView(R.id.layout_full_name) TextInputLayout mLayoutFullName;
    @BindView(R.id.edit_full_name) TextInputEditText mFullName;

    @BindView(R.id.layout_email) TextInputLayout mLayoutEmail;
    @BindView(R.id.edit_email) TextInputEditText mUserId;

    @BindView(R.id.layout_password) TextInputLayout mLayoutPassword;
    @BindView(R.id.edit_password) TextInputEditText mPassword;

    @BindView(R.id.layout_password_confirm) TextInputLayout mLayoutConfirmPassword;
    @BindView(R.id.edit_password_confirm) TextInputEditText mPasswordConfirm;

    @BindView(R.id.text_terms_and_policy) TextView mTermsAndPolicy;
    @BindView(R.id.text_login) TextView mLoginText;

    public static Intent getLaunchIntent(final BaseActivity baseActivity) {
        return new Intent(baseActivity, RegistrationActivity.class);
    }

    @Override
    protected void injectToComponent() {
        getComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_registration;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTermsAndPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        mLoginText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OnClick(R.id.button_register)
    void onRegisterClicked() {
        getPresenter().registerClicked(mFullName.getText().toString().trim(),
                mUserId.getText().toString().trim(),
                mPassword.getText().toString().trim(),
                mPasswordConfirm.getText().toString().trim());
    }

    @Override
    public void clearValidationErrors() {
        mLayoutFullName.setError(null);
        mLayoutEmail.setError(null);
        mLayoutPassword.setError(null);
        mLayoutConfirmPassword.setError(null);
    }

    @Override
    public void emptyFullName() {
        mLayoutFullName.setError(getString(R.string.register_error_empty_full_name));
    }

    @Override
    public void incorrectEmail() {
        mLayoutEmail.setError(getString(R.string.register_error_wrong_email));
    }

    @Override
    public void incorrectPassword() {
        mLayoutPassword.setError(getString(R.string.register_error_wrong_password));
    }

    @Override
    public void passwordsDoNotMutch() {
        mLayoutConfirmPassword.setError(getString(R.string.register_error_password_do_not_much));
    }

    @Override
    public void registerSuccess() {
        getNavigator().startActivity(RegistrationCompleteActivity.getLaunchIntent(this));
    }

    @Override
    public void onSignUpFailure(String errorString) {
        new AlertDialog.Builder(this, R.style.App_Theme_AlertDialogInfo)
                .setTitle(R.string.register_failure_message_title)
                .setMessage(errorString)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }
}
