package com.appinstitute.beau.customer.di.module;

import android.app.Application;
import android.content.Context;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ApplicationContext;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.customer.common.authentication.AuthHelperImpl;
import com.appinstitute.beau.customer.presentation.ErrorHelperImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Singleton
    @Provides
    AuthHelper getAuthHelper(final @ApplicationContext Context context) {
        return new AuthHelperImpl(context);
    }

    @Singleton
    @Provides
    ErrorHelper getErrorHelper(final @ApplicationContext Context context) {
        return new ErrorHelperImpl(context);
    }
}
