package com.appinstitute.beau.customer.presentation.ui.auth.register.complete;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.domain.base.DefaultInternetSubscriber;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.core.presentation.base.view.form.Validator;
import com.appinstitute.beau.customer.domain.account.EditUserUseCase;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

@ConfigPersistent
public final class RegistrationCompletePresenter extends BasePresenter<RegistrationCompleteView> {

    @Inject ErrorHelper mErrorHelper;
    @Inject AuthHelper mAuthHelper;
    @Inject EditUserUseCase mEditUserUseCase;

    @Inject
    public RegistrationCompletePresenter() {
    }

    public void onSaveClicked(final String addressOne,
                              final String addressTwo,
                              final String town,
                              final String country,
                              final String postcode) {

        getView().clearValidationErrors();
        if (Validator.isEmpty(addressOne)) {
            getView().emptyAddressOne();
            return;
        }

        if (Validator.isEmpty(addressTwo)) {
            getView().emptyAddressTwo();
            return;
        }

        if (Validator.isEmpty(town)) {
            getView().emptyTown();
            return;
        }

        if (Validator.isEmpty(country)) {
            getView().emptyCountry();
            return;
        }

        if (Validator.isEmpty(postcode)) {
            getView().emptyPostcode();
            return;
        }

        UserModel currentUser = (UserModel) mAuthHelper.getUserModel();
        mEditUserUseCase.setUserModel(new UserModel(currentUser.getName(), currentUser.getEmail(),
                addressOne, addressTwo, town, country, postcode));
        mEditUserUseCase.execute(new SignUpCompleteSubscriber());

    }

    class SignUpCompleteSubscriber extends DefaultInternetSubscriber<UserModel> {

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }

        @Override
        public void onNext(UserModel userModel) {
            getView().updateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            super.onError(throwable);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void handleUnprocessableEntity(String[] errors) {
            getView().onUpdateFailure(mErrorHelper.getErrorString(errors));
        }

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }
    }
}
