package com.appinstitute.beau.customer.presentation.ui.auth.register.complete.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appinstitute.beau.customer.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    private List<CountryItem> mItems;
    private ItemListener mListener;

    public CountryAdapter(final List<CountryItem> items,
                          final ItemListener listener) {
        mItems = items;
        mListener = listener;
    }

    public void setListener(final ItemListener listener) {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.setData(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_country) TextView mCountryName;
        private CountryItem mItem;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(final CountryItem item) {
            mItem = item;
            mCountryName.setText(item.getTitle());
        }

        @OnClick(R.id.text_country)
        void onCountryClick() {
            if (mListener != null) {
                mListener.onItemClick(mItem);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(final CountryItem item);
    }
}