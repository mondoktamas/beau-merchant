package com.appinstitute.beau.customer.presentation;

import android.content.Context;

import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.customer.R;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ErrorHelperImpl implements ErrorHelper {

    private final Context mContext;

    @Inject
    public ErrorHelperImpl(final Context context) {
        mContext = context;
    }

    @Override
    public String getErrorString(String[] errors) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String error : errors) {
            stringBuilder.append(getErrorMessage(error) + "\n");
        }
        removeLastN(stringBuilder);
        return stringBuilder.toString();
    }

    private void removeLastN(StringBuilder stringBuilder) {
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    }

    private String getErrorMessage(String error) {
        switch (error) {
            case "user_not_found":
                return mContext.getString(R.string.user_not_found);

            case "user_already_exists":
                return mContext.getString(R.string.user_already_exists);

            case "session_id_not_exists":
                return mContext.getString(R.string.session_id_not_exists);

            case "sessionId":
                return mContext.getString(R.string.sessionId);

            case "email_unique":
                return mContext.getString(R.string.email_unique);

            case "user_id_unique":
                return mContext.getString(R.string.user_id_unique);

            /* ... */

            default:
                return error;
        }

    }
}
