package com.appinstitute.beau.customer.data.network.model.request;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class SignInRequest {

    @JsonField(name="email")
    String mUserId;

    @JsonField(name="password")
    String mPassword;

    @JsonField(name = "push_token")
    String mPushToken;

    public SignInRequest() {
    }

    public SignInRequest(String mUserId, String mPassword, String mPushToken) {
        this.mUserId = mUserId;
        this.mPassword = mPassword;
        this.mPushToken = mPushToken;
    }
}
