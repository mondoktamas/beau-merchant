package com.appinstitute.beau.customer.presentation.ui.account;

import com.appinstitute.beau.core.common.authentication.AuthHelper;
import com.appinstitute.beau.core.di.annotations.ConfigPersistent;
import com.appinstitute.beau.core.domain.base.DefaultInternetSubscriber;
import com.appinstitute.beau.core.presentation.base.ErrorHelper;
import com.appinstitute.beau.core.presentation.base.presenter.BasePresenter;
import com.appinstitute.beau.customer.domain.account.GetAccountUseCase;
import com.appinstitute.beau.customer.domain.auth.LogoutUseCase;
import com.appinstitute.beau.customer.presentation.model.UserModel;

import javax.inject.Inject;

@ConfigPersistent
public class MyAccountPresenter extends BasePresenter<MyAccountView> {

    @Inject ErrorHelper mErrorHelper;
    @Inject AuthHelper mAuthHelper;
    @Inject GetAccountUseCase mGetAccountUseCase;
    @Inject LogoutUseCase mLogoutUseCase;

    @Inject
    public MyAccountPresenter() {}

    void fetchAccountInfo() {
        unsubscribeOnDestroy(mGetAccountUseCase.execute(new GetAccountSubscriber()));
    }

    void logout() {
        unsubscribeOnDestroy(mLogoutUseCase.execute(new LogoutSubscriber()));
    }

    void openMyCards() {
        getView().openMyCards();
    }

    void openPersonalDetails() {
        getView().openPersonalDetails();
    }

    public class LogoutSubscriber extends DefaultInternetSubscriber {
        @Override
        public void onError(final Throwable throwable) {
            super.onError(throwable);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void handleUnprocessableEntity(final String[] errors) {
            if (!isViewAttached()) return;
            getView().onLogoutFailed(mErrorHelper.getErrorString(errors));
        }

        @Override
        public void onCompleted() {
            mAuthHelper.releaseData();
            if (!isViewAttached()) return;
            getView().showProgress(false);
            getView().onLogoutSuccess();
        }

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }
    }

    public class GetAccountSubscriber extends DefaultInternetSubscriber<UserModel> {

        @Override
        public void onCompleted() {
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void onError(final Throwable throwable) {
            super.onError(throwable);
            if (!isViewAttached()) return;
            getView().showProgress(false);
        }

        @Override
        public void onNext(final UserModel userModel) {
            if (!isViewAttached()) return;
            getView().showAccountInfo(userModel);
        }

        @Override
        public void handleUnprocessableEntity(final String[] errors) {
            if (!isViewAttached()) return;
            getView().onGetAccountFailed(mErrorHelper.getErrorString(errors));
        }

        @Override
        public void onStart() {
            if (!isViewAttached()) return;
            getView().showProgress(true);
        }
    }
}
