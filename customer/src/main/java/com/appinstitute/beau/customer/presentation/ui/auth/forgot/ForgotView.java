package com.appinstitute.beau.customer.presentation.ui.auth.forgot;

import com.appinstitute.beau.core.presentation.base.view.BaseView;

public interface ForgotView extends BaseView<ForgotPresenter> {

    void clearValidationErrors();

    void emptyEmail();

    void incorrectEmail();

    void requestSuccess();

    void requestFailure();

    void showErrorMessages(String errorString);
}
